package com.ptravels.bookTicket.presentation;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.paytm.pgsdk.PaytmConstants;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.ptravels.PaymentConstants;
import com.ptravels.R;
import com.ptravels.SharedPrefUtils;
import com.ptravels.Utils;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.domain.BlockTicketUseCase;
import com.ptravels.seatlayout.data.model.BookedSeatModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ptravels.PaymentConstants.ORDER_ID_VALUE;
import static com.ptravels.PaymentConstants.THEME_VALUE;
import static com.ptravels.PaymentConstants.TXN_AMOUNT_VALUE;
import static com.ptravels.PaymentConstants.productionChecksumGenerationURL;
import static com.ptravels.PaymentConstants.productionChecksumValidationURL;
import static com.ptravels.PaymentConstants.stagingChecksumGenerationURL;
import static com.ptravels.PaymentConstants.stagingChecksumValidationURL;

public class PaymentDetailsActivity extends AppCompatActivity implements BlockTicketView {

    private LinearLayout other_parent_layout;
    private EditText coupan_code;
    private RadioButton payWithPayTm;
    private RadioButton paynimo_btn;
    private RadioButton credit_btn;
    private RadioButton debit_btn;
    private RadioButton netbanking_btn;
    private RadioButton wallet_btn;
    private RadioButton emi_btn;
    private RadioButton imps_btn;
    private RadioButton cashcard_btn;
    private int selected_payment_type = 0;

    private TextView referralAmountTv;
    private TextView basicAmnt, serviceTaxAmnt, totalAmntText;

    private LinearLayout ticketSummaryContainer;

    private int referralAmnt = 0;
    private String totalAmntString;
    float finalTotalAmount = 0;
    String beforeAmount = "0";
    private String BOOKING_ID_FIRST = "";
    private float TOTAL_SERVICE_TAX = 0;
    private BookedSeatModel singleTripTicketData;
    private AppCompatButton payBtn;
    private BookTicketPresenterImpl bookTicketPresenter;
    private BlockTicketResponse blockTicketResponse;
    private boolean isTicketBlocked;

    private FrameLayout loaderLayout;
    private ImageView loader;

    /*Paytm*/
    private PaytmPGService paytmPGService;
    private String PAYTM_ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        if (SharedPrefUtils.getSharedPrefUtils().getSingleTripTicketData(this) != null) {
            singleTripTicketData = SharedPrefUtils.getSharedPrefUtils().getSingleTripTicketData(this);
        }

        initiateView();
    }

    public void initiateView() {
        TextView title = findViewById(R.id.title);
        title.setText("Summary");
        ticketSummaryContainer = (LinearLayout) findViewById(R.id.ticketSummaryContainer);
        credit_btn = (RadioButton) findViewById(R.id.credit_card);
        debit_btn = (RadioButton) findViewById(R.id.debit_card);
        netbanking_btn = (RadioButton) findViewById(R.id.netbanking_card);
        wallet_btn = (RadioButton) findViewById(R.id.wallet_card);
        emi_btn = (RadioButton) findViewById(R.id.emi_card);
        imps_btn = (RadioButton) findViewById(R.id.imps_card);
        cashcard_btn = (RadioButton) findViewById(R.id.cash_card);
        basicAmnt = (TextView) findViewById(R.id.fare);
        serviceTaxAmnt = (TextView) findViewById(R.id.serviceTax);
        totalAmntText = findViewById(R.id.totalAmntTv);
        payBtn = findViewById(R.id.payBtn);
        payWithPayTm = findViewById(R.id.payWithPayTm);

        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        if (singleTripTicketData != null) {
            View ticketSummaryView = (View) getLayoutInflater().inflate(R.layout.layout_ticket_summary, null);
            TextView fromTo = (TextView) ticketSummaryView.findViewById(R.id.fromToValue);
            TextView travelDate = (TextView) ticketSummaryView.findViewById(R.id.travelDate);
            TextView deptTime = (TextView) ticketSummaryView.findViewById(R.id.departureTime);

            TextView boardingPtName = (TextView) ticketSummaryView.findViewById(R.id.boardingPoint);
            CheckBox showPassengersCb = (CheckBox) ticketSummaryView.findViewById(R.id.showPassengerSummaryCb);
            final LinearLayout passengersSummaryContainer = (LinearLayout) ticketSummaryView.findViewById(R.id.passengersSummeryLayout);

            fromTo.setText(singleTripTicketData.getFromStationName() + " to " + singleTripTicketData.getToStationName());
            travelDate.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, yyyy", singleTripTicketData.getTravelDate()));
            deptTime.setText(Utils.formatDateString("yyyy-MM-dd", "hh:mm a", singleTripTicketData.getTravelDate()));
            boardingPtName.setText(singleTripTicketData.getBoardingPoint().getStationPointName());

            showPassengersCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        passengersSummaryContainer.setVisibility(View.VISIBLE);
                    } else {
                        passengersSummaryContainer.setVisibility(View.GONE);
                    }

                }
            });
            showPassengersCb.setChecked(false);
            passengersSummaryContainer.setVisibility(View.VISIBLE);

            if (singleTripTicketData.getPassengerList().size() > 0) {

                for (int i = 0; i < singleTripTicketData.getPassengerList().size(); i++) {
                    View passengerSummaryView = getLayoutInflater().inflate(R.layout.layout_passengers_summary, null);
                    TextView name = (TextView) passengerSummaryView.findViewById(R.id.passName);
                    TextView seatNo = (TextView) passengerSummaryView.findViewById(R.id.passSeatNo);
//                    TextView age = (TextView) passengerSummaryView.findViewById(R.id.passAge);
//                    TextView gender = (TextView) passengerSummaryView.findViewById(R.id.passGender);

                    name.setSelected(true);
                    name.setText(singleTripTicketData.getPassengerList().get(i).getName()
                            + "\t\t\t" + singleTripTicketData.getPassengerList().get(i).getGender()
                            + "\t\t\t" + singleTripTicketData.getPassengerList().get(i).getAge());
                    seatNo.setText("Seat#" + singleTripTicketData.getPassengerList().get(i).getSeatNbr());
//                    age.setText(singleTripTicketData.getPassengerList().get(i).getAge());
//                    gender.setText(singleTripTicketData.getPassengerList().get(i).getGender());
                    passengersSummaryContainer.addView(passengerSummaryView);
                }
            }
//            service_name.setText(singleTripTicketData.getServiceNumber());
            ticketSummaryContainer.addView(ticketSummaryView);
        }


        /*if (userDetail != null && userDetail.getResponse() != null && userDetail.getResponse().getUser_detail() != null) {
            referralAmnt = userDetail.getResponse().getUser_detail().getReferral_amount();
            if (referralAmnt > 0) {
                referralAmountTv.setVisibility(View.VISIBLE);
                referralAmountTv.setText("₹ " + referralAmnt + "" + " Reduced from referral amount! ");

                if ((Integer.valueOf(totalAmntString)) <= referralAmnt) {
                    int tonew = (Integer.valueOf(totalAmntString)) / 2;
                    referralAmnt = ((tonew));
                    totalAmntString = String.valueOf(Integer.valueOf(totalAmntString) - (referralAmnt));
                } else {
                    totalAmntString = String.valueOf(Integer.valueOf(totalAmntString) - (referralAmnt));
                }
            }
        }*/

        String tp = String.valueOf(singleTripTicketData.getTotalPrice());
//        String tp2 = "";
        totalAmntString = tp;
        String totalServiceTax = String.valueOf(singleTripTicketData.getTotalServiceTax());
        TOTAL_SERVICE_TAX = TOTAL_SERVICE_TAX + Float.valueOf(totalServiceTax);
        if (TOTAL_SERVICE_TAX > 0) {
            serviceTaxAmnt.setVisibility(View.VISIBLE);
        }

        String totalFare = String.valueOf(singleTripTicketData.getBookedSeatTotalCost());
        finalTotalAmount = Float.valueOf(totalAmntString);
//        payBtn.setText("Pay amount : \u20B9 " + /*singleTripTicketData.getBookedSeatTotalCost()*/finalTotalAmount);
        basicAmnt.setText("Basic amount : " + "\u20B9 " + Float.valueOf(totalFare));
        serviceTaxAmnt.setText("Service Tax Amount : " + "\u20B9 " + TOTAL_SERVICE_TAX);
        totalAmntText.setText("Total Amount : " + "\u20B9 " + finalTotalAmount);
        Log.d("TOTAL_SERVICE_TAX---", TOTAL_SERVICE_TAX + "");

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (blockTicketResponse == null && !isTicketBlocked)
                    blockTicket();
                else {
//                    showPaymentOption();
                    if (payWithPayTm.isChecked())
                        initPayTmTransaction(blockTicketResponse);
                }
            }
        });

    }

    private void blockTicket() {
        BlockTicketRequest request = new BlockTicketRequest();
        request.setEmailId(singleTripTicketData.getContactEmailId());
        request.setMobileNumber(singleTripTicketData.getContactNumber());
        request.setBoardingPointId(String.valueOf(singleTripTicketData.getBoardingPoint().getStationPointId()));
        request.setFromStation(singleTripTicketData.getFromStationId());
        request.setToStation(singleTripTicketData.getToStationId());
        request.setTravelDate(singleTripTicketData.getTravelDate());
        request.setScheduleId(singleTripTicketData.getScheduleId());
        request.setPassengerDetails(singleTripTicketData.getPassengerList());
        request.token = SharedPrefUtils.getSharedPrefUtils().getAccessToken(this);
        bookTicketPresenter = new BookTicketPresenterImpl(this, new BlockTicketUseCase());
        bookTicketPresenter.getBlockTicketResult(request);
    }

    @Override
    public void showBlockTicketResult(BlockTicketResponse response) {
        if (response == null)
            return;
        blockTicketResponse = response;

        if (blockTicketResponse.getSuccess().equalsIgnoreCase("true") && blockTicketResponse.getData() != null) {
            if (!blockTicketResponse.getData().getPnr().isEmpty()) {
                isTicketBlocked = true;
//                showPaymentOption();
                if (payWithPayTm.isChecked())
                    initPayTmTransaction(blockTicketResponse);
            }
        } else if (!blockTicketResponse.getSuccess().equalsIgnoreCase("false")) {
            List<String> errors = response.getError().getGlobalErrors();
            String errMsg = "";
            for (int i = 0; i < errors.size(); i++) {
                if (i == 0 || i == errors.size() - 1) {
                    errMsg = errMsg + errors.get(i);
                } else {
                    errMsg = errMsg + errors.get(i) + ",";
                }
            }

            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showError(String error) {
        if (!isFinishing())
            Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    /*public void showPaymentOption() {

        if (!isTicketBlocked || blockTicketResponse==null)
            return;

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_payment_selection, null);
        dialogBuilder.setView(dialogView);

        AppCompatButton payNow = dialogView.findViewById(R.id.payment_btn);
        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (*//*!paynimo_btn.isChecked() &&*//* !payWithPayTm.isChecked()){
                    Toast.makeText(PaymentDetailsActivity.this, "Select payment type", Toast.LENGTH_SHORT).show();
                    return;
                }

                AlertDialog alertDialog = (AlertDialog) view.getTag();
                alertDialog.dismiss();
                if (payWithPayTm.isChecked()) {
                    initPayTmTransaction(blockTicketResponse);
                } else if (paynimo_btn.isChecked()) {
//                    payNimo();
                }
            }
        });

        payWithPayTm = dialogView.findViewById(R.id.payWithPayTm);
        paynimo_btn = dialogView.findViewById(R.id.paynimo_btn);

        if (imps_btn.isChecked() || cashcard_btn.isChecked() || emi_btn.isChecked()) {
            payWithPayTm.setVisibility(View.GONE);
        }

        AppCompatButton cancelBtn = dialogView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = (AlertDialog) v.getTag();
                alertDialog.dismiss();
            }
        });

        AlertDialog dialog = dialogBuilder.create();
        cancelBtn.setTag(dialog);
        payNow.setTag(dialog);
        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }*/

    public void initPayTmTransaction(final BlockTicketResponse ticketResponse) {
        paytmPGService = null;
        String payAmnt = String.valueOf(finalTotalAmount);

//        paytmPGService = PaytmPGService.getStagingService();
        paytmPGService = PaytmPGService.getProductionService();
        Map<String, String> paramMap = new HashMap<String, String>();
        ORDER_ID_VALUE = Utils.getUtils().initOrderId();
        if (payAmnt.contains("₹")) {
            payAmnt = payAmnt.replace("₹", payAmnt).trim();
        }
        //  paramMap.put("ORDER_ID", ORDER_ID);
        paramMap.put(PaymentConstants.ORDER_ID, ticketResponse.getData().getPnr());
        paramMap.put(PaymentConstants.MID, PaymentConstants.MID_VALUE);
        paramMap.put(PaymentConstants.CUST_ID, ticketResponse.getData().getPnr());
        paramMap.put(PaymentConstants.CHANNEL_ID, PaymentConstants.CHANNEL_ID_VALUE);
        paramMap.put(PaymentConstants.INDUSTRY_TYPE_ID, PaymentConstants.INDUSTRY_TYPE_ID_VALUE);
        paramMap.put(PaymentConstants.WEBSITE, PaymentConstants.WEBSITE_VALUE);
        paramMap.put(PaymentConstants.TXN_AMOUNT, /*payAmnt.trim()*/TXN_AMOUNT_VALUE);
        paramMap.put(PaymentConstants.REQUEST_TYPE, PaymentConstants.REQUEST_TYPE_VALUE);
        paramMap.put(PaymentConstants.THEME, THEME_VALUE);
        //paramMap.put("EMAIL", "developer@brsoftech.com");
        //paramMap.put("MOBILE_NO", "7877475123");


        PaytmOrder Order = new PaytmOrder(paramMap);

//        PaytmMerchant Merchant = new PaytmMerchant(stagingChecksumGenerationURL, stagingChecksumValidationURL);
        PaytmMerchant Merchant = new PaytmMerchant(productionChecksumGenerationURL, productionChecksumValidationURL);

        paytmPGService.initialize(Order, Merchant, null);

        paytmPGService.startPaymentTransaction(this, false, false,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.e("someUIErrorOccurred", inErrorMessage);
                    }

                    @Override
                    public void onTransactionSuccess(Bundle inResponse) {
                        Log.e("LOG", "Payment Transaction is successful " + inResponse);

                        PAYTM_ID = inResponse.getString(PaytmConstants.TRANSACTION_ID);
                        Log.e("PaymentId by PayTm=", PAYTM_ID);
                        Toast.makeText(PaymentDetailsActivity.this, "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
                        ticketResponse.setPAYTM_ID(PAYTM_ID);
//                        boockedTicket(ticketResponse, 0);

                    }

                    @Override
                    public void onTransactionFailure(String inErrorMessage, Bundle inResponse) {
                        Log.e("LOG", "Payment Transaction Failed " + inErrorMessage);
                        Toast.makeText(PaymentDetailsActivity.this,
                                "Payment Transaction Failed, " + inErrorMessage,
                                Toast.LENGTH_LONG).show();
                        paytmPGService = null;
                    }

                    @Override
                    public void networkNotAvailable() { // If network is not // available, then this // method gets called.
                        Toast.makeText(PaymentDetailsActivity.this, "networkNotAvailable!!", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        Log.e("AuthenticationFailed=", inErrorMessage);
                        Toast.makeText(PaymentDetailsActivity.this, inErrorMessage, Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                        Log.e("onErrorLoadingWebPage=", inErrorMessage + ",url = " + inFailingUrl);
                        Toast.makeText(PaymentDetailsActivity.this, inErrorMessage, Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onBackPressedCancelTransaction() {
                        Toast.makeText(PaymentDetailsActivity.this, "Transaction Cancelled", Toast.LENGTH_LONG).show();
                    }

                });
    }
}
