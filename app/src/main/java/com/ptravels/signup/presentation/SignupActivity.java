package com.ptravels.signup.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.signin.presentation.SigninActivity;
import com.ptravels.global.UserData;
import com.ptravels.global.Utils;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.signup.domain.SignupUseCase;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SignupActivity extends BaseActivity implements View.OnClickListener, SignupView {
    private TextInputLayout fnameInputLayout, emailInputLayout, mobileInputLayout, pwdInputLayout;
    private EditText fnameEt, emailEt, mobileEt, pwdEt;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private AlertDialog webDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initviews();
    }

    private void initviews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        ((TextView) findViewById(R.id.title)).setText("Sign Up");

        View fnameView = findViewById(R.id.fnameInputLayout);
        View mobileView = findViewById(R.id.mobileInputLayout);
        View emailView = findViewById(R.id.emailInputLayout);
        View pwdView = findViewById(R.id.pwdInputLayout);
        fnameInputLayout = fnameView.findViewById(R.id.input_layout);
        mobileInputLayout = mobileView.findViewById(R.id.input_layout);
        emailInputLayout = emailView.findViewById(R.id.input_layout);
        pwdInputLayout = pwdView.findViewById(R.id.input_layout);
        fnameEt = fnameView.findViewById(R.id.input_et);
        mobileEt = mobileView.findViewById(R.id.input_et);
        emailEt = emailView.findViewById(R.id.input_et);
        pwdEt = pwdView.findViewById(R.id.input_et);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        mobileInputLayout.setHint("Mobile number");
        emailInputLayout.setHint("Email address");
        pwdInputLayout.setHint("Password");
        fnameInputLayout.setHint("Name");


        fnameEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        mobileEt.setInputType(InputType.TYPE_CLASS_PHONE);
        mobileEt.setKeyListener(DigitsKeyListener.getInstance(false,false));
        mobileEt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});

        emailEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
        pwdEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        ((TextView) findViewById(R.id.termConditionsTv)).setText(Html.fromHtml(getResources().getString(R.string.termsConditionsBlue)));
        setclickListeners();
        handleTextWatchers();

        SpannableString contentText = clickableStrings(getResources().getString((R.string.signin_agree)));
        TextView textView = findViewById(R.id.termConditionsTv);
        textView.setText(contentText);
        textView.setAutoLinkMask(RESULT_OK);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
        Linkify.addLinks(contentText, Linkify.WEB_URLS);
    }

    private SpannableString clickableStrings(String sampleString) {
        SpannableString spannableString = new SpannableString(sampleString);
        ClickableSpan termsConditionsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                showWebAlertDialog("file:///android_res/raw/terms_condition.html");
            }
        };


        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                showWebAlertDialog("file:///android_res/raw/privacy_policy.html");
            }
        };

        return formatSpannableString(spannableString, termsConditionsClick, privacyClick);
    }

    private void handleTextWatchers() {
        mobileEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() < 10) {
                    mobileInputLayout.setErrorEnabled(true);
                    mobileInputLayout.setError("Invalid mobile number");
                } else {
                    mobileInputLayout.setErrorEnabled(false);
                    mobileInputLayout.setError(null);
                }
            }
        });

        emailEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!Utils.getUtils().isValidEmail(editable)) {
                    emailInputLayout.setErrorEnabled(true);
                    emailInputLayout.setError("Invalid email");
                } else {
                    emailInputLayout.setErrorEnabled(false);
                    emailInputLayout.setError(null);
                }
            }
        });

        pwdEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() < 6 && editable.length() > 0) {
                    pwdInputLayout.setErrorEnabled(true);
                    pwdInputLayout.setError("Minimum 6 characters");
                } else {
                    pwdInputLayout.setErrorEnabled(false);
                    pwdInputLayout.setError(null);
                }
            }
        });
    }

    private void setclickListeners() {
        rxSingleClickListener(findViewById(R.id.backBtn));
        rxSingleClickListener(findViewById(R.id.signUpBtn));
    }

    private boolean validateFields() {
        fnameEt.clearFocus();
        mobileEt.clearFocus();
        emailEt.clearFocus();
        pwdEt.clearFocus();
        fnameInputLayout.setErrorEnabled(false);
        mobileInputLayout.setErrorEnabled(false);
        emailInputLayout.setErrorEnabled(false);
        pwdInputLayout.setErrorEnabled(false);
        fnameInputLayout.setError(null);
        mobileInputLayout.setError(null);
        emailInputLayout.setError(null);
        pwdInputLayout.setError(null);
        if (fnameEt.getText().toString().trim().isEmpty()) {
            fnameInputLayout.setErrorEnabled(true);
            fnameInputLayout.setError("Name is mandatory");
            fnameEt.setText("");
            fnameEt.requestFocus();
            return false;
        } else if (mobileEt.getText().toString().isEmpty()) {
            mobileInputLayout.setErrorEnabled(true);
            mobileInputLayout.setError("Mobile number is mandatory");
            mobileEt.requestFocus();
            return false;
        } else if (emailEt.getText().toString().isEmpty()) {
            emailInputLayout.setErrorEnabled(true);
            emailInputLayout.setError("Email is mandatory");
            emailEt.requestFocus();
            return false;
        } else if (pwdEt.getText().toString().isEmpty()) {
            pwdInputLayout.setErrorEnabled(true);
            pwdInputLayout.setError("Password is mandatory");
            pwdEt.requestFocus();
            return false;
        } else if (mobileEt.getText().toString().length() < 10) {
            mobileInputLayout.setErrorEnabled(true);
            mobileInputLayout.setError("Invalid Mobile number");
            mobileEt.requestFocus();
            return false;
        } else if (!Utils.getUtils().isValidPhoneNumber(mobileEt.getText().toString())) {
            emailInputLayout.setErrorEnabled(true);
            emailInputLayout.setError("Invalid Mobile number");
            emailEt.requestFocus();
            return false;
        } else if (!Utils.getUtils().isValidEmail(emailEt.getText().toString())) {
            emailInputLayout.setErrorEnabled(true);
            emailInputLayout.setError("Invalid Email");
            emailEt.requestFocus();
            return false;
        } else if (pwdEt.getText().toString().length() < 6) {
            pwdInputLayout.setErrorEnabled(true);
            pwdInputLayout.setError("Password should be minimum 6 characters");
            pwdEt.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View view) {
        Utils.getUtils().closeKeyboard(this, getCurrentFocus());
        switch (view.getId()) {
            case R.id.signUpBtn:

                if (validateFields()) {
                    showLoading();
                    SignupRequest request = new SignupRequest();
                    request.setFirstName(fnameEt.getText().toString());
                    request.setMobile(mobileEt.getText().toString());
                    request.setEmail(emailEt.getText().toString());
                    request.setPassword(pwdEt.getText().toString());

                    SignupPresenter presenter = new SignupPresenterImpl(new SignupUseCase(), this);
                    presenter.getSignupResponse(request);
                }
                break;
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void updateSignupResponse(SignupResponse response) {
        hideLoading();
        if (response.getUsername() != null) {
            Log.d("updateSignupResponse", "success");
            saveUserDetails(response);
        } else if (response.getError() != null) {
            String error = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
        }
    }

    private void saveUserDetails(SignupResponse response) {
        UserData data = new UserData();
        data.setFirstName(response.getFirstName());
        data.setUsername(response.getUsername());
        data.setEmail(response.getEmail());
        data.setMobile(response.getMobile());
        data.setPassword(pwdEt.getText().toString().trim());
        if (response.getEnabled().equalsIgnoreCase("true"))
            data.setEnabled(true);
        else data.setEnabled(false);

//        SharedPrefUtils.getSharedPrefUtils().saveUserDetails(this,data);
        if (response.getEnabled().equalsIgnoreCase("true")) {
            startActivity(new Intent(this, SigninActivity.class)
                    .putExtra(Constants.USER_DATA, data));
            finish();
        }
    }

    @Override
    public void showError(String error) {
        hideLoading();
        Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
    }

    private void showWebAlertDialog(String fileUrl) {
        if (webDialog!=null && webDialog.isShowing())
            webDialog.dismiss();

        WebView webView = new WebView(this);
        webView.loadUrl(fileUrl);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(webView)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (webDialog!=null && webDialog.isShowing())
                            webDialog.dismiss();
                        webDialog = null;
                    }
                });
        if (webDialog == null)
            webDialog = builder.create();
        if (!webDialog.isShowing() && !isFinishing())
            webDialog.show();
    }

    private SpannableString formatSpannableString(SpannableString spannableString,
                                                  ClickableSpan termsClicked, ClickableSpan privacyClicked) {
        spannableString.setSpan(termsClicked, 31, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(privacyClicked, 54, 68, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_btn)), 31, 51,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_btn)), 54, 68,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });

        clickViewObservable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(View view) {
                        onClick(view);
                    }
                });
    }

}
