package com.ptravels.signup.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.signup.data.model.SignupResponse;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface SignupView extends LoadingView{
    void updateSignupResponse(SignupResponse response);
    void showError(String error);
}
