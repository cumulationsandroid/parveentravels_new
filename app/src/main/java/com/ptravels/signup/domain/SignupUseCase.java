package com.ptravels.signup.domain;

import com.ptravels.global.UseCase;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.signup.data.source.SignupDataSourceImpl;

import rx.Single;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SignupUseCase extends UseCase<SignupRequest,SignupResponse> {

    private SignupDataInterface signupDataSource = new SignupDataSourceImpl();

    @Override
    public Single<SignupResponse> buildUseCase(SignupRequest request) {
        return signupDataSource.getSignupResult(request);
    }
}
