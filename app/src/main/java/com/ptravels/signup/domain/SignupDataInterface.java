package com.ptravels.signup.domain;

import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;

import rx.Single;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface SignupDataInterface {
    Single<SignupResponse> getSignupResult(SignupRequest request);
}
