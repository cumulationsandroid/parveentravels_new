package com.ptravels.signup.data.source;

import com.ptravels.REST.RestClient;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.signup.domain.SignupDataInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SignupDataSourceImpl implements SignupDataInterface {

    @Override
    public Single<SignupResponse> getSignupResult(SignupRequest request) {
        return RestClient.getApiService().getSignupResponse(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
