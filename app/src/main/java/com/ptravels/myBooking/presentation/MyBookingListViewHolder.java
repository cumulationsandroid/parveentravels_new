package com.ptravels.myBooking.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ptravels.R;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingListViewHolder extends RecyclerView.ViewHolder {


    public TextView dateTv;
    public TextView timePlaceTv;
    public TextView tripToTv;
    public TextView cancelTv;
    public TextView pnrTv;

    public MyBookingListViewHolder(View itemView) {
        super(itemView);
        tripToTv=itemView.findViewById(R.id.mybooking_item_tv_trip);
        dateTv=itemView.findViewById(R.id.mybooking_item_tv_date);
        timePlaceTv=itemView.findViewById(R.id.mybooking_item_tv_time_place);
        cancelTv=itemView.findViewById(R.id.mybooking_item_tv_cancel);
        pnrTv=itemView.findViewById(R.id.mybooking_pnr);
    }
}
