package com.ptravels.myBooking.presentation;

import com.ptravels.R;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public enum MyBookingTypes {

    UPCOMING(0, R.string.upcoming),
    COMPLETED(1, R.string.completed),
    CANCELLED(2, R.string.cancelled);


    private int pageNumber;
    private int pageNameStringID;

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageNameStringID() {
        return pageNameStringID;
    }

    MyBookingTypes(int pageNumber, int pageNameStringID) {
        this.pageNameStringID = pageNameStringID;
        this.pageNumber = pageNumber;
    }

  static public int getPageStringIdForPage(int pageNumber) {
        for (MyBookingTypes myBookingTypes : MyBookingTypes.values()) {
            if (pageNumber == myBookingTypes.pageNumber) {
                return myBookingTypes.getPageNameStringID();
            }
        }
        return 0;
    }

    static public MyBookingTypes getTypeForPosition(int pageNumber){
        for (MyBookingTypes myBookingTypes : MyBookingTypes.values()) {
            if (pageNumber == myBookingTypes.pageNumber) {
                return myBookingTypes;
            }
        }
        return null;
    }
}
