package com.ptravels.myBooking.presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.aboutUs.presentation.CallSupportFragment;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.seatlayout.data.model.BookedSeatModel;

public class FragmentHolderActivity extends AppCompatActivity {
    public static final int CALL_SUPPORT_FRAGMENT = 1001;
    public static final int TICKET_DETAILS_FRAGMENT = 1002;
    private int fragmentToDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);

        if (getIntent()!=null){
            fragmentToDisplay = getIntent().getIntExtra(Constants.LOAD_FRAGMENT,-1);
        }

        if (fragmentToDisplay == CALL_SUPPORT_FRAGMENT){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container,new CallSupportFragment())
                    .commit();
        } else if (fragmentToDisplay == TICKET_DETAILS_FRAGMENT){
            BookedSeatModel bookedSeatModel = (BookedSeatModel) getIntent().getSerializableExtra(Constants.BOOKED_SEAT_DATA);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, TicketDetailsFragment.loadTicketInfo(bookedSeatModel))
                    .commit();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        TextView toolbarTitle = findViewById(R.id.title);
        setTitleText(toolbarTitle);
        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        backPressed();
    }

    private void backPressed(){
        startActivity(new Intent(FragmentHolderActivity.this, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }

    private void setTitleText(TextView toolbarTitle){
        switch (fragmentToDisplay){
            case CALL_SUPPORT_FRAGMENT:
                toolbarTitle.setText("Call Support");
                break;

            case TICKET_DETAILS_FRAGMENT:
                toolbarTitle.setText("Ticket Details");
                break;
        }
    }
}
