package com.ptravels.myBooking.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.ParveenApp;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.myBooking.data.model.MyBookingDataModel;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.model.response.HistoryTicket;
import com.ptravels.myBooking.data.model.response.TicketDetailPassengerDetail;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.myBooking.domain.CancelTicketUseCase;
import com.ptravels.myBooking.domain.MyBookingUseCase;
import com.ptravels.myBooking.domain.TicketDetailUseCase;
import com.ptravels.seatlayout.presentation.CancellationPolicyFragment;
import com.ptravels.splash.presentation.SplashActivity;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by BHARATHY S on 12/22/17.
 */

public class MyBookingActivity extends BaseActivity implements MyBookingView, View.OnClickListener {
    private ViewPager viewPager;
    private TabLayout tablayout;
    private MyBookingPageAdapter adapter;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private HistoryTicket currentClickedTicket;
    private List<String> currentTicketSeatIds = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mybooking);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyBookingData();
    }

    private void init() {
        initViews();
        setAdapter();
//        getMyBookingData();
    }

    private void initViews() {
        viewPager = findViewById(R.id.mybooking_viewpager);
        tablayout = findViewById(R.id.mybooking_tablayout);
        findViewById(R.id.backBtn).setOnClickListener(this);
        findViewById(R.id.info_button).setOnClickListener(this);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);
    }

    private void setAdapter() {
        adapter = new MyBookingPageAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewPager/*, true*/);
        tablayout.setTabTextColors(ContextCompat.getColor(this, R.color.app_text_color),
                ContextCompat.getColor(this, R.color.light_blue));
    }

    @Override
    public void updateMyBookingData(HistoryResponse historyResponse) {
        if (historyResponse.isSuccess()) {
            adapter.updateList(parse(historyResponse));
            adapter.notifyDataSetChanged();
        } else if (historyResponse.getError() != null) {
            String error = RestClient.get200StatusErrors(historyResponse.getError());
            Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
            if (error.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void updateCanelTicketUi(CancelTicketResponse cancelTicketResponse) {
        if (cancelTicketResponse.isSuccess()) {
            if (cancelTicketResponse.getData().getPnr().equals(currentClickedTicket.getPnr())) {
                Toast.makeText(this, "Ticket cancelled successfully", Toast.LENGTH_SHORT).show();
                onRestart();
            }
//            adapter.notifyDataSetChanged();
        } else if (cancelTicketResponse.getError() != null) {
            String error = RestClient.get200StatusErrors(cancelTicketResponse.getError());
            Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
            if (error.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, "error: " + error, Toast.LENGTH_SHORT).show();
        if (error.equalsIgnoreCase("Un-authorized acess")) {
            SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
            startActivity(new Intent(this, SplashActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    @Override
    public void provideTicketDetails(TicketDetailResponse response) {
        if (!isFinishing()) {
            if (response == null)
                return;
            if (response.isSuccess()) {
                if (response.getData() != null && response.getData().getPassengerDetails() != null) {
                    List<TicketDetailPassengerDetail> passengerDetails = response.getData().getPassengerDetails();
                    for (TicketDetailPassengerDetail detail : passengerDetails) {
                        if (!detail.getTicketStatus().equalsIgnoreCase("CANCELLED"))
                            currentTicketSeatIds.add(String.valueOf(detail.getSeatId()));
                    }
                    if (currentTicketSeatIds.size() > 0)
                        cancelTicket(response.getData().getPnr(), currentTicketSeatIds);
                }
            } else if (response.getError() != null) {
                String error = RestClient.get200StatusErrors(response.getError());
                Toast.makeText(this, "" + error, Toast.LENGTH_SHORT).show();
                if (error.equalsIgnoreCase("Un-authorized acess")) {
                    SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                    startActivity(new Intent(this, SplashActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
            }
        }
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    private void getMyBookingData() {
        showLoading();
        MyBookingRequest myBookingRequest = new MyBookingRequest();
        myBookingRequest.setEmail(SharedPrefUtils.getSharedPrefUtils().getUserEmailID(this));

        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(new MyBookingUseCase(), this);
        myBookingPresenter.getMyBookingResponse(myBookingRequest);
    }

    public void cancelTicket(HistoryTicket ticket) {
        showLoading();
        CancelTicketRequest cancelTicketRequest = new CancelTicketRequest();
        cancelTicketRequest.setPnr(ticket.getPnr());
        ArrayList<String> seatIdList = new ArrayList<>();
        seatIdList.add("1");
        cancelTicketRequest.setSeatIds(seatIdList);
        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(this);
        myBookingPresenter.cancelTicket(new CancelTicketUseCase(), cancelTicketRequest);
    }

    public void cancelTicket(String pnr, List<String> seatIds) {
        CancelTicketRequest cancelTicketRequest = new CancelTicketRequest();
        cancelTicketRequest.setPnr(pnr);
        cancelTicketRequest.setSeatIds(seatIds);
        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(this);
        myBookingPresenter.cancelTicket(new CancelTicketUseCase(), cancelTicketRequest);
    }

    private MyBookingDataModel parse(HistoryResponse historyResponse) {
        MyBookingDataModel myBookingDataModel = new MyBookingDataModel();

        for (HistoryTicket ticket : historyResponse.getData().getTickets()) {
            switch (ticket.getTicketStatus()) {
                case Constants.TICKET_BOOKED:
                    MyBookingTypes type = getTicketTypeUpcomingOrCompleted(ticket);
                    if (type == MyBookingTypes.UPCOMING) {
                        myBookingDataModel.getUpcomingTripDataList().add(ticket);
                    } else if (type == MyBookingTypes.COMPLETED) {
                        myBookingDataModel.getCompletedTripDataList().add(ticket);
                    }
                    break;
                case Constants.TICKET_CANCELLED:
                    myBookingDataModel.getCancelledTripDataList().add(ticket);
                    break;
            }
        }
        return myBookingDataModel;
    }

    private MyBookingTypes getTicketTypeUpcomingOrCompleted(HistoryTicket ticket) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date ticketBoardingDate = simpleDateFormat.parse(ticket.getBoardingDate() + " " + ticket.getBoardingPointTime());
            Date currentDate = Calendar.getInstance().getTime();
            if (ticketBoardingDate.compareTo(currentDate) < 0) {
                return MyBookingTypes.COMPLETED;
            } else {
                return MyBookingTypes.UPCOMING;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //  RestClient.enableMockApi = false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;

            case R.id.info_button:
                CancellationPolicyFragment bottomSheetDialogFragment = new CancellationPolicyFragment();
                bottomSheetDialogFragment.show(getSupportFragmentManager(), CancellationPolicyFragment.class.getSimpleName());
                break;
        }
    }

    public void showCancelTicketAlert(final HistoryTicket ticket) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please confirm to cancel the ticket.");
        builder.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getTicketDetails(ticket);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void getTicketDetails(HistoryTicket ticket) {
        currentTicketSeatIds.clear();
        currentClickedTicket = ticket;
        PathParamRequest request = new PathParamRequest();
        request.setPnr(ticket.getPnr());
        request.setToken(ParveenApp.getAccessToken());
        MyBookingPresenterImpl myBookingPresenter = new MyBookingPresenterImpl(this);
        myBookingPresenter.getTicketDetails(new TicketDetailUseCase(), request);
    }
}


