package com.ptravels.myBooking.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.myBooking.data.model.TripListModel;
import com.ptravels.myBooking.data.model.response.HistoryTicket;

import java.util.ArrayList;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingFragment extends Fragment {
    private RecyclerView recyclerView;
    private TextView emptyTv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mybooking_list_fragment, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.mybooking_fragment_rv);
        emptyTv = view.findViewById(R.id.emptyTv);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        MyBookingListAdapter adapter = new MyBookingListAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);

        TripListModel tripListModel = (TripListModel) getArguments().getSerializable(Constants.MY_BOOKING_DATA);
        if (tripListModel == null || tripListModel.getTripList()== null || tripListModel.getTripList().size() == 0){
//            Toast.makeText(getActivity(), "No tickets found", Toast.LENGTH_SHORT).show();
            emptyTv.setVisibility(View.VISIBLE);
            return;
        }
        emptyTv.setVisibility(View.GONE);
        adapter.updateList((ArrayList<HistoryTicket>) tripListModel.getTripList());
        if (tripListModel.getTripListType() == MyBookingTypes.UPCOMING) {
            adapter.setCancelOptionenable(true);
        } else {
            adapter.setCancelOptionenable(false);
        }
        adapter.notifyDataSetChanged();
    }
}
