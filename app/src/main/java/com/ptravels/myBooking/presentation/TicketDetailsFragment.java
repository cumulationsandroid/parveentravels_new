package com.ptravels.myBooking.presentation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.data.model.BookedSeatModel;

/**
 * Created by Amit Tumkur on 07-01-2018.
 */

public class TicketDetailsFragment extends Fragment {
    private static TicketDetailsFragment fragment;
    private BookedSeatModel bookedSeatModel;
    public TicketDetailsFragment(){}

    public static TicketDetailsFragment loadTicketInfo(BookedSeatModel bookedSeatModel){
        if (fragment == null)
            fragment = new TicketDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BOOKED_SEAT_DATA,bookedSeatModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_ticketdetails,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View contentView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(contentView, savedInstanceState);

        TextView pnr = contentView.findViewById(R.id.pnr);
        TextView fromToNameTv = contentView.findViewById(R.id.fromToValue);
        TextView travelDateTv = contentView.findViewById(R.id.fare_detailt_tv_traveldate);
        TextView boardingPtTv = contentView.findViewById(R.id.boardingPointName);
        TextView droppingPtTv = contentView.findViewById(R.id.droppingPtName);
        TextView totalSeatPriceTv = contentView.findViewById(R.id.fare_detailt_tv_seatcost);
        TextView totalGSTcost = contentView.findViewById(R.id.fare_detailt_tv_gstcost);
        TextView totalPrice = contentView.findViewById(R.id.fare_detailt_tv_totalcost);
        TextView busTypeTv = contentView.findViewById(R.id.fare_detailt_tv_bustype);
        TextView boardingTimeTv = contentView.findViewById(R.id.boardingPointTime);
        TextView droppingTimeTv = contentView.findViewById(R.id.droppingPointTime);

        if (getArguments()!=null){
            bookedSeatModel = (BookedSeatModel) getArguments().getSerializable(Constants.BOOKED_SEAT_DATA);
        }

        pnr.setText("PNR : "+bookedSeatModel.getBlockedSeatPnr());
        fromToNameTv.setText(bookedSeatModel.getFromStationName()+" \u2794 "+bookedSeatModel.getToStationName());
        boardingPtTv.setText("Boarding Point : "+bookedSeatModel.getBoardingPoint().getStationPointName());
        droppingPtTv.setText("Dropping Point : "+bookedSeatModel.getDroppingPoint().getStationPointName());
        totalPrice.setText("\u20B9 "+String.valueOf(bookedSeatModel.getTotalPrice()));
        totalGSTcost.setText("\u20B9 "+String.valueOf(bookedSeatModel.getTotalServiceTax()));
        totalSeatPriceTv.setText("\u20B9 "+String.valueOf(bookedSeatModel.getBookedSeatTotalCost()));
        busTypeTv.setText(bookedSeatModel.getBusType());

        busTypeTv.setSelected(true);
        boardingPtTv.setSelected(true);
        droppingPtTv.setSelected(true);

        if (bookedSeatModel.getTravelDate() != null) {
            travelDateTv.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, EEE", bookedSeatModel.getTravelDate()));
        }
        if (bookedSeatModel.getBoardingPoint() != null && bookedSeatModel.getBoardingPoint().getStationPointTime() != null) {
            boardingTimeTv.setText(Utils.formatDateString("HH:mm:ss", "h:mm a", bookedSeatModel.getBoardingPoint().getStationPointTime()));
        }

        if (bookedSeatModel.getDroppingPoint() != null && bookedSeatModel.getDroppingPoint().getStationPointTime() != null) {
            droppingTimeTv.setText(Utils.formatDateString("HH:mm:ss", "h:mm a", bookedSeatModel.getDroppingPoint().getStationPointTime()));
        }

    }
}
