package com.ptravels.myBooking.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public interface MyBookingView extends LoadingView {
    void updateMyBookingData(HistoryResponse historyResponse);

    void updateCanelTicketUi(CancelTicketResponse cancelTicketResponse);

    void showError(String error);

    void provideTicketDetails(TicketDetailResponse response);
}
