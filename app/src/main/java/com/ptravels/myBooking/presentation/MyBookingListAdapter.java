package com.ptravels.myBooking.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.myBooking.data.model.response.HistoryTicket;

import java.util.ArrayList;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public class MyBookingListAdapter extends RecyclerView.Adapter<MyBookingListViewHolder> {

    Context context;
    ArrayList<HistoryTicket> tickets;
    private boolean cancelOptionenable = true;

    public boolean isCancelOptionenable() {
        return cancelOptionenable;
    }

    public void setCancelOptionenable(boolean cancelOptionenable) {
        this.cancelOptionenable = cancelOptionenable;
    }

    public MyBookingListAdapter(Context context) {
        this.context = context;
    }

    public void updateList(ArrayList<HistoryTicket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public MyBookingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mybooking_list_item, parent, false);

        MyBookingListViewHolder viewHolder = new MyBookingListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyBookingListViewHolder holder, int position) {
        if (!isCancelOptionenable()) {
            holder.cancelTv.setVisibility(View.GONE);
        } else {
            holder.cancelTv.setVisibility(View.VISIBLE);
        }
        final HistoryTicket ticket = tickets.get(position);
        holder.tripToTv.setText("Trip to " + ticket.getToStation());
        holder.pnrTv.setText("PNR : " + ticket.getPnr());
        holder.dateTv.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, EEE", ticket.getBoardingDate()));
        holder.timePlaceTv
                .setText(
                        Utils.formatDateString("HH:mm:ss", "h:mm a", ticket.getDroppingPointTime())
                                + " - " +
                                ticket.getDroppingPoint().getStationPointName()
                );

        holder.timePlaceTv.setSelected(true);

        holder.cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((MyBookingActivity) context).getTicketDetails(ticket);
                ((MyBookingActivity) context).showCancelTicketAlert(ticket);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (tickets != null) {
            return tickets.size();
        } else {
            return 0;
        }
    }
}
