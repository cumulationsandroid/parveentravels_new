package com.ptravels.myBooking.domain;

import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import rx.Single;

/**
 * Created by BHARATHY S on 12/27/17.
 */

public interface MyBookingDataInterface {
    Single<HistoryResponse> getMyBookingList(MyBookingRequest myBookingRequest);
    Single<CancelTicketResponse> cancelTicket(CancelTicketRequest cancelTicketRequest);
    Single<TicketDetailResponse> getTicketDetails(PathParamRequest request);
}
