package com.ptravels.myBooking.domain;

import com.ptravels.global.UseCase;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.source.MyBookingDataSourceImpl;

import rx.Single;

/**
 * Created by cumulations on 3/1/18.
 */

public class CancelTicketUseCase extends UseCase<CancelTicketRequest,CancelTicketResponse> {

  private MyBookingDataSourceImpl myBookingDataSource =new MyBookingDataSourceImpl();

    @Override
    public Single<CancelTicketResponse> buildUseCase(CancelTicketRequest cancelTicketRequest) {
        return myBookingDataSource.cancelTicket(cancelTicketRequest);
    }
}
