package com.ptravels.myBooking.data.model.response;

import com.ptravels.global.ResponseError;

public class CancelTicketResponse{
	private CancelTicketData data;
	private boolean success;
    private ResponseError error;

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }

	public void setData(CancelTicketData data){
		this.data = data;
	}

	public CancelTicketData getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"CancelTicketResponse{" + 
			"data = '" + data + '\'' + 
			",success = '" + success + '\'' + 
			"}";
		}
}
