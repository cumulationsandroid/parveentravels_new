package com.ptravels.myBooking.data.model.response;

public class TicketDetailsResponse{
	private HistoryData data;
	private boolean success;

	public void setData(HistoryData data){
		this.data = data;
	}

	public HistoryData getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}
}
