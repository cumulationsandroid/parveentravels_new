package com.ptravels.myBooking.data.model.response;

import com.ptravels.global.ResponseError;

public class TicketDetailResponse{
	private TicketDetailData data;
	private boolean success;
	private ResponseError error;

	public ResponseError getError() {
		return error;
	}

	public void setError(ResponseError error) {
		this.error = error;
	}

	public void setData(TicketDetailData data){
		this.data = data;
	}

	public TicketDetailData getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"TicketDetailResponse{" + 
			"data = '" + data + '\'' + 
			",success = '" + success + '\'' + 
			"}";
		}
}
