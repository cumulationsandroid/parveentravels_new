package com.ptravels.myBooking.data.model.response;

public class TicketDetailDroppingPoint {
	private String stationPointId;
	private String mobileNumber;
	private String pointName;
	private String stationPointName;
	private double latitude;
	private boolean branch;
	private String addressPinCode;
	private String addressId;
	private String contactPersonName;
	private String station;
	private String addressLine1;
	private String addressLandMark;
	private String addressLine2;
	private int operatorStationId;
	private int id;
	private double longitude;

	public void setStationPointId(String stationPointId){
		this.stationPointId = stationPointId;
	}

	public String getStationPointId(){
		return stationPointId;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setPointName(String pointName){
		this.pointName = pointName;
	}

	public String getPointName(){
		return pointName;
	}

	public void setStationPointName(String stationPointName){
		this.stationPointName = stationPointName;
	}

	public String getStationPointName(){
		return stationPointName;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setBranch(boolean branch){
		this.branch = branch;
	}

	public boolean isBranch(){
		return branch;
	}

	public void setAddressPinCode(String addressPinCode){
		this.addressPinCode = addressPinCode;
	}

	public String getAddressPinCode(){
		return addressPinCode;
	}

	public void setAddressId(String addressId){
		this.addressId = addressId;
	}

	public String getAddressId(){
		return addressId;
	}

	public void setContactPersonName(String contactPersonName){
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonName(){
		return contactPersonName;
	}

	public void setStation(String station){
		this.station = station;
	}

	public String getStation(){
		return station;
	}

	public void setAddressLine1(String addressLine1){
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine1(){
		return addressLine1;
	}

	public void setAddressLandMark(String addressLandMark){
		this.addressLandMark = addressLandMark;
	}

	public String getAddressLandMark(){
		return addressLandMark;
	}

	public void setAddressLine2(String addressLine2){
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine2(){
		return addressLine2;
	}

	public void setOperatorStationId(int operatorStationId){
		this.operatorStationId = operatorStationId;
	}

	public int getOperatorStationId(){
		return operatorStationId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"DroppingPoint{" + 
			"stationPointId = '" + stationPointId + '\'' + 
			",mobileNumber = '" + mobileNumber + '\'' + 
			",pointName = '" + pointName + '\'' + 
			",stationPointName = '" + stationPointName + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",branch = '" + branch + '\'' + 
			",addressPinCode = '" + addressPinCode + '\'' + 
			",addressId = '" + addressId + '\'' + 
			",contactPersonName = '" + contactPersonName + '\'' + 
			",station = '" + station + '\'' + 
			",addressLine1 = '" + addressLine1 + '\'' + 
			",addressLandMark = '" + addressLandMark + '\'' + 
			",addressLine2 = '" + addressLine2 + '\'' + 
			",operatorStationId = '" + operatorStationId + '\'' + 
			",id = '" + id + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}
