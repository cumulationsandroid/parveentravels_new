package com.ptravels.myBooking.data.model.response;

public class TicketDetailCancelCharge {
	private double serviceTaxAmount;
	private double cancelCharge;

	public void setServiceTaxAmount(int serviceTaxAmount){
		this.serviceTaxAmount = serviceTaxAmount;
	}

	public double getServiceTaxAmount(){
		return serviceTaxAmount;
	}

	public void setCancelCharge(double cancelCharge){
		this.cancelCharge = cancelCharge;
	}

	public double getCancelCharge(){
		return cancelCharge;
	}

	@Override
 	public String toString(){
		return 
			"CancelChargesItem{" + 
			"serviceTaxAmount = '" + serviceTaxAmount + '\'' + 
			",cancelCharge = '" + cancelCharge + '\'' + 
			"}";
		}
}
