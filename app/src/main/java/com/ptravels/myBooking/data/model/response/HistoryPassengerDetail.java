package com.ptravels.myBooking.data.model.response;

import java.io.Serializable;

public class HistoryPassengerDetail implements Serializable {
	private String seatType;
	private String gender;
	private int seatFare;
	private String ticketStatus;
	private String name;
	private int discount;
	private String seatNbr;
	private int age;

	public void setSeatType(String seatType){
		this.seatType = seatType;
	}

	public String getSeatType(){
		return seatType;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setSeatFare(int seatFare){
		this.seatFare = seatFare;
	}

	public int getSeatFare(){
		return seatFare;
	}

	public void setTicketStatus(String ticketStatus){
		this.ticketStatus = ticketStatus;
	}

	public String getTicketStatus(){
		return ticketStatus;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDiscount(int discount){
		this.discount = discount;
	}

	public int getDiscount(){
		return discount;
	}

	public void setSeatNbr(String seatNbr){
		this.seatNbr = seatNbr;
	}

	public String getSeatNbr(){
		return seatNbr;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}

	@Override
 	public String toString(){
		return 
			"PassengerDetailsItem{" + 
			"seatType = '" + seatType + '\'' + 
			",gender = '" + gender + '\'' + 
			",seatFare = '" + seatFare + '\'' + 
			",ticketStatus = '" + ticketStatus + '\'' + 
			",name = '" + name + '\'' + 
			",discount = '" + discount + '\'' + 
			",seatNbr = '" + seatNbr + '\'' + 
			",age = '" + age + '\'' + 
			"}";
		}
}
