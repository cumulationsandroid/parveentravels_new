
package com.ptravels.seatlayout.data.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusMapRequest {

    @SerializedName("fromStation")
    @Expose
    private String fromStation;
    @SerializedName("toStation")
    @Expose
    private String toStation;
    @SerializedName("travelDate")
    @Expose
    private String travelDate;
    @SerializedName("scheduleId")
    @Expose
    private String scheduleId;

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

}
