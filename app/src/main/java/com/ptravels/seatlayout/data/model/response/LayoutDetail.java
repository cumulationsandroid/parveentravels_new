
package com.ptravels.seatlayout.data.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.nio.DoubleBuffer;

public class LayoutDetail implements Serializable{

    @SerializedName("seatId")
    @Expose
    private Integer seatId;
    @SerializedName("column")
    @Expose
    private Integer column;
    @SerializedName("row")
    @Expose
    private Integer row;
    @SerializedName("seatNumber")
    @Expose
    private String seatNumber;
    @SerializedName("seatType")
    @Expose
    private String seatType;
    @SerializedName("tier")
    @Expose
    private Integer tier;
    @SerializedName("vertical")
    @Expose
    private Boolean vertical;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("availableFor")
    @Expose
    private String availableFor;
    @SerializedName("fare")
    @Expose
    private Double fare;
    @SerializedName("serviceTax")
    @Expose
    private Double serviceTax;

    public Integer getSeatId() {
        return seatId;
    }

    public void setSeatId(Integer seatId) {
        this.seatId = seatId;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public Integer getTier() {
        return tier;
    }

    public void setTier(Integer tier) {
        this.tier = tier;
    }

    public Boolean getVertical() {
        return vertical;
    }

    public void setVertical(Boolean vertical) {
        this.vertical = vertical;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAvailableFor() {
        return availableFor;
    }

    public void setAvailableFor(String availableFor) {
        this.availableFor = availableFor;
    }

    public Double getFare() {
        return fare;
    }

    public void setFare(Double fare) {
        this.fare = fare;
    }

    public Double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(Double serviceTax) {
        this.serviceTax = serviceTax;
    }


    boolean selected;
    public Boolean toggleSelection(){
         selected=!selected;
        return selected;
    }

    public Boolean isSelected() {
        return selected;
    }
}
