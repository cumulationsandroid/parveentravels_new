package com.ptravels.seatlayout.presentation;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.data.model.response.DroppingPoint;

import java.util.ArrayList;

/**
 * Created by praveenkumar on 23/12/17.
 */

public class DroppingPointsFragment extends BottomSheetDialogFragment {

    protected ArrayList<DroppingPoint> droppingPointArrayList = new ArrayList<>();
    private int selectedStationId = -1; //nothing is selected


    public void provideData(ArrayList<DroppingPoint> droppingPoints, int selectedId) {
        this.droppingPointArrayList = droppingPoints;
        this.selectedStationId = selectedId;
    }

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_board_deboard, null);
        TextView titleView = contentView.findViewById(R.id.title_dialogue);
        titleView.setText("Select a Dropping Point");

        ImageView backButton = contentView.findViewById(R.id.backBtn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        ListView placesListView = contentView.findViewById(R.id.placesList);
        final PlacesListingAdapter placesListingAdapter = new PlacesListingAdapter(droppingPointArrayList);
        placesListView.setAdapter(placesListingAdapter);
        placesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((BusMapActivity) getActivity()).setDropingPoint(droppingPointArrayList.get(i));
                selectedStationId = droppingPointArrayList.get(i).getStationPointId();
                Log.d("clicked", "help");
                placesListingAdapter.notifyDataSetChanged();
                dialog.cancel();
            }
        });
        dialog.setContentView(contentView);

    }


    private class PlacesListingAdapter extends BaseAdapter {


        PlacesListingAdapter() {
            super();
        }

        public PlacesListingAdapter(ArrayList<DroppingPoint> droppingPoints) {
            super();
        }

        @Override
        public int getCount() {
            return droppingPointArrayList.size();
        }

        @Override
        public DroppingPoint getItem(int i) {
            return droppingPointArrayList.size() == 1 ? droppingPointArrayList.get(i) : droppingPointArrayList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View inflate = getLayoutInflater().inflate(R.layout.boarding_deboardingpoints_adaptor,
                    viewGroup, false);
            TextView placeName = inflate.findViewById(R.id.placeName);
//            inflate.findViewById(R.id.placeTime).setVisibility(View.GONE);
            RadioButton radioButton = inflate.findViewById(R.id.placeButton);
            TextView placeTime = inflate.findViewById(R.id.placeTime);

            DroppingPoint droppingPoints;
            if (droppingPointArrayList.size() == 1) {
                droppingPoints = droppingPointArrayList.get(0);
            } else {
                droppingPoints = droppingPointArrayList.get(i);
            }
            Integer stationPointId = droppingPoints.getStationPointId();
            String stationPointName = droppingPoints.getStationPointName();
            String stationPointTime = Utils.formatDateString("HH:mm:ss", "h:mm a", droppingPoints.getStationPointTime());

            placeName.setText(stationPointName);
            placeTime.setText(stationPointTime);
            if (stationPointId == selectedStationId) {
                radioButton.setChecked(true);
            } else {
                radioButton.setChecked(false);
            }

            return inflate;
        }
    }
}
