package com.ptravels.seatlayout.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ptravels.R;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.seatlayout.data.model.SeatMapModel;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;

import java.util.List;

public interface BusMapView{

    void showLoading();
    void hideLoading();
    void showError(String error);
    void showSeatMap(SeatMapModel seatMapModel);


}