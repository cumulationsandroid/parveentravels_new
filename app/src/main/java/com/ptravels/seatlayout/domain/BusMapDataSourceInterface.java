package com.ptravels.seatlayout.domain;

import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BusMapResponse;

import rx.Single;

/**
 * Created by praveenkumar on 14/12/17.
 */

public interface BusMapDataSourceInterface {
    Single<BusMapResponse> getBusMap(BusMapRequest request);
}
