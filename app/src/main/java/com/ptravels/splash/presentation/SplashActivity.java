package com.ptravels.splash.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.UserData;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signin.domain.SigninUseCase;
import com.ptravels.signin.presentation.SigninPresenter;
import com.ptravels.signin.presentation.SigninPresenterImpl;
import com.ptravels.signin.presentation.SigninUpdateView;
import com.squareup.picasso.Picasso;

/**
 * Created by cumulations on 9/1/18.
 */

public class SplashActivity extends BaseActivity implements SigninUpdateView {
    private ImageView loader;
    private AlertDialog noNetDialog;
    private boolean onResumeInternet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        loader = findViewById(R.id.loader);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.isOnlineUsingCOntext())
            return;
        onResumeInternet = true;
        goToHomeScreen();
    }

    private void getGuestUserToken() {
        SigninRequest request = new SigninRequest();
        request.setEmail(Constants.GUEST_USER_EMAIL);
        request.setPassword(Constants.GUEST_USER_PASSWORD);

        SigninPresenter presenter = new SigninPresenterImpl(new SigninUseCase(), this);
        presenter.getSigninResponse(request);
    }

    @Override
    public void showLoading() {
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loader.setVisibility(View.GONE);
    }

    @Override
    public void updateSigninResponse(SigninResponse response) {
        if (isFinishing())
            return;
        hideLoading();
        if (response.isSuccess()) {
            UserData userData = new UserData();
            userData.setEmail(Constants.GUEST_USER_EMAIL);
            userData.setPassword(Constants.GUEST_USER_PASSWORD);
            userData.setAccessToken(response.getData());
            SharedPrefUtils.getSharedPrefUtils().saveUserDetails(this, userData);
            SharedPrefUtils.getSharedPrefUtils().setGuestUserLogin(this, true);
            launchHomeScreen(true);
        } else if (!response.isSuccess() && response.getError()!=null) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showError(String error) {
        if (isFinishing())
            return;
        hideLoading();
        if (error.equalsIgnoreCase(Constants.NO_INTERNET)) {
            showNOInternetDialog(Constants.NO_INTERNET);
        } else if (error.equalsIgnoreCase(Constants.NETWORK_ERROR)) {
            showNOInternetDialog(Constants.NETWORK_ERROR);
        } else {
            Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
        }
//        finish();
    }

    private void launchHomeScreen(boolean isGuestUser) {
        logEventToFabric(isGuestUser);
        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        finish();
    }

    private void logEventToFabric(boolean isGuestUser) {
        String method = isGuestUser ? "Guest" : "User";
        try{
            Answers.getInstance().logLogin(new LoginEvent()
                    .putMethod(method)
                    .putSuccess(true));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showNOInternetDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (noNetDialog!=null && noNetDialog.isShowing())
                    noNetDialog.dismiss();
                finishAffinity();
            }
        });
        if (noNetDialog == null)
            noNetDialog = builder.create();
        if (!noNetDialog.isShowing() && !isFinishing())
            noNetDialog.show();
    }

    @Override
    public void netBack(boolean isBack) {
        super.netBack(isBack);
        if (isBack && !onResumeInternet){
            goToHomeScreen();
        }
    }

    public void goToHomeScreen(){
        showLoading();
        if (!SharedPrefUtils.getSharedPrefUtils().isUserLoggedIn(this) ||
                (SharedPrefUtils.getSharedPrefUtils().isUserLoggedIn(this)
                        && SharedPrefUtils.getSharedPrefUtils().isGuestUserLogin(this))) {
            getGuestUserToken();
        } else {
            new Handler()
                    .postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            launchHomeScreen(false);
                        }
                    }, 1000);
        }
    }
}
