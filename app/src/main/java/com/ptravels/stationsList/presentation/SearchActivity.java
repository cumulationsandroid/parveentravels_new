package com.ptravels.stationsList.presentation;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.splash.presentation.SplashActivity;
import com.ptravels.stationsList.data.model.response.PathParamRequest;
import com.ptravels.stationsList.data.model.response.Station;
import com.ptravels.stationsList.data.model.response.StationsListResponse;
import com.ptravels.stationsList.domain.GetStationsUseCase;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends BaseActivity implements View.OnClickListener, StationListView {
    private SearchView fromSearchView, goingToSearchView;
    private ImageButton fromClearBtn, goingToClearBtn;
    //    private TextView popularSearchesLabel;
    private RecyclerView searchViewsRecyclerView;
    private LinearLayout goingToSearchLayout;
    private List<Station> stationList = new ArrayList<>();
    private SearchViewsRecyclerAdapter adapter;
    private final static int LEAVING_FROM_TYPE = 1;
    private final static int GOING_TO_TYPE = 2;
    private int searchViewType = 0;
    private String fromStationName, fromStationId, toStationName, toStationId;
    private RecyclerView popularPlacesGridView;
    private PlacesGridRecyclerAdapter popularPlacesAdapter;
    private View popularPlacesView;
    private TextView adapterHeaderTextView;

    private String preSelectedStation;
    private FrameLayout loaderLayout;
    private ImageView loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getDataFromThePreviousScreenIfAny();
        initViews();  // Dont move initViews after the setListners
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initAdapter();
    }

    private void initAdapter() {
        StationsListResponse response = SharedPrefUtils.getSharedPrefUtils().getBusStation(this);
        if (response == null) {
            StationsListPresenter presenter = new StationsListPresenterImpl(this, new GetStationsUseCase());
            String token = SharedPrefUtils.getSharedPrefUtils().getAccessToken(this);
            PathParamRequest request = new PathParamRequest();
            request.setToken(token);
            presenter.getStationsList(request);
        } else {
            processBusStationResponse(response);
        }
    }

    private void setListeners() {
        fromSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
//                    searchViewsRecyclerView.setVisibility(View.VISIBLE);
                    fromClearBtn.setVisibility(View.VISIBLE);
                    fromClearBtn.setImageResource(R.drawable.ic_close_icon);
                    searchViewType = LEAVING_FROM_TYPE;

                    if (popularPlacesView.getVisibility() == View.GONE) {
                        adapterHeaderTextView.setVisibility(View.VISIBLE);
                        adapterHeaderTextView.setText(R.string.select_from_city);
                    } else {
                        adapterHeaderTextView.setVisibility(View.GONE);
                    }
                }
            }
        });

        fromSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fromClearBtn.setImageResource(R.drawable.ic_leaving_from_selected);
                goingToSearchLayout.setVisibility(View.VISIBLE);
                searchViewsRecyclerView.setVisibility(View.GONE);

                if (goingToSearchView.getQuery().length()<=0)
                    popularPlacesView.setVisibility(View.VISIBLE);

                if (toStationName != null) {
                    goingToSearchView.setQuery(toStationName, false);
                    if (goingToSearchView.getQuery().length()>0) {
                        searchViewsRecyclerView.setVisibility(View.VISIBLE);
                        popularPlacesView.setVisibility(View.GONE);
                        adapter.getFilter().filter(toStationName);
                    }
                }


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                fromClearBtn.setImageResource(R.drawable.ic_close_icon);
                searchViewType = LEAVING_FROM_TYPE;
                if (newText.trim().length() > 0) {
                    popularPlacesView.setVisibility(View.GONE);
                    searchViewsRecyclerView.setVisibility(View.VISIBLE);
                    if (adapter != null)
                        adapter.getFilter().filter(newText);
                } else {
                    popularPlacesView.setVisibility(View.VISIBLE);
                    searchViewsRecyclerView.setVisibility(View.GONE);
                }
                return true;
            }
        });

        goingToSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    goingToClearBtn.setVisibility(View.VISIBLE);
                    goingToClearBtn.setImageResource(R.drawable.ic_close_icon);
                    searchViewType = GOING_TO_TYPE;

                    if (popularPlacesView.getVisibility() == View.GONE) {
                        adapterHeaderTextView.setVisibility(View.VISIBLE);
                        adapterHeaderTextView.setText(R.string.select_to_city);
                    } else {
                        adapterHeaderTextView.setVisibility(View.GONE);

                    }
                }
            }
        });

        goingToSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                goingToClearBtn.setImageResource(R.drawable.ic_going_to_selected);
                if (query.length()<=0)
                    searchViewsRecyclerView.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                goingToClearBtn.setImageResource(R.drawable.ic_close_icon);
                searchViewType = GOING_TO_TYPE;
                if (newText.trim().length() > 0) {
                    popularPlacesView.setVisibility(View.GONE);
                    searchViewsRecyclerView.setVisibility(View.VISIBLE);
                    if (adapter != null)
                        adapter.getFilter().filter(newText);
                } else {
                    searchViewsRecyclerView.setVisibility(View.GONE);
                    popularPlacesView.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });

        fromClearBtn.setOnClickListener(this);
        goingToClearBtn.setOnClickListener(this);
        findViewById(R.id.backBtn).setOnClickListener(this);
    }

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        fromSearchView = findViewById(R.id.fromSearchView);
        goingToSearchView = findViewById(R.id.goingToSearchView);
        fromClearBtn = findViewById(R.id.fromClearBtn);
        goingToClearBtn = findViewById(R.id.goingToClearBtn);
//        popularSearchesLabel = findViewById(R.id.popularLabel);
        searchViewsRecyclerView = findViewById(R.id.search_results_recycler_view);
        goingToSearchLayout = findViewById(R.id.goingToSearchLayout);

        popularPlacesView = findViewById(R.id.popularPlacesLayout);
        popularPlacesGridView = findViewById(R.id.placesGridRecyclerView);
        adapterHeaderTextView = findViewById(R.id.adapterHeader);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        goingToSearchLayout.setVisibility(View.GONE);
        searchViewsRecyclerView.setVisibility(View.GONE);

        searchViewsRecyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        searchViewsRecyclerView.setLayoutManager(layoutManager);

        fromSearchView.setEnabled(false);
        goingToSearchView.setEnabled(false);

        fromSearchView.setIconifiedByDefault(false);
        fromSearchView.setQueryHint(getString(R.string.leaving_from_string));
        if (fromStationId != null) {
            fromSearchView.setQuery(fromStationName, false);
        }

        goingToSearchView.setIconifiedByDefault(false);
        goingToSearchView.setQueryHint(getString(R.string.going_to_string));
        if (toStationId != null) {
            goingToSearchView.setQuery(toStationName, false);
        }
//        adapter = new SearchViewsRecyclerAdapter(stationList);
//        searchViewsRecyclerView.setAdapter(adapter);
    }

    private void getDataFromThePreviousScreenIfAny() {
        fromStationId = getIntent().getStringExtra(Constants.FROM_STATION_ID);
        toStationId = getIntent().getStringExtra(Constants.TO_STATION_ID);
        preSelectedStation = getIntent().getStringExtra(Constants.PRE_SELECTED);

        if (fromStationId != null) {
            fromStationName = getIntent().getStringExtra(Constants.FROM_STATION_NAME);
        }
        if (toStationId != null) {
            toStationName = getIntent().getStringExtra(Constants.TO_STATION_NAME);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fromClearBtn:
                fromSearchView.setQuery("", false);
                fromStationId = null;
                fromStationName = null;
                searchViewsRecyclerView.setVisibility(View.GONE);
                popularPlacesView.setVisibility(View.VISIBLE);
                break;

            case R.id.goingToClearBtn:
                goingToSearchView.setQuery("", false);
                toStationId = null;
                toStationName = null;
                searchViewsRecyclerView.setVisibility(View.GONE);
                popularPlacesView.setVisibility(View.VISIBLE);
                break;

            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    @Override
    public void showStationListResults(StationsListResponse response) {
        SharedPrefUtils.getSharedPrefUtils().saveBusStation(this, response);
        processBusStationResponse(response);
    }

    private void processBusStationResponse(StationsListResponse response) {
        if (response != null) {
            if (response.getSuccess().equalsIgnoreCase("true")) {
                stationList = response.getData().getStations();
                adapter = new SearchViewsRecyclerAdapter(this, stationList);
                searchViewsRecyclerView.setAdapter(adapter);
                fromSearchView.setEnabled(true);
                goingToSearchView.setEnabled(true);

                int numberOfColumns = 2;
                popularPlacesGridView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
                popularPlacesAdapter = new PlacesGridRecyclerAdapter(this, getPopularStationList(stationList));
                popularPlacesGridView.setAdapter(popularPlacesAdapter);


                DividerItemDecoration verticalDecoration = new DividerItemDecoration(
                        popularPlacesGridView.getContext(),
                        DividerItemDecoration.HORIZONTAL);
                verticalDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.vertical_divider));
                popularPlacesGridView.addItemDecoration(verticalDecoration);

                DividerItemDecoration horizontalDecoration = new DividerItemDecoration(
                        popularPlacesGridView.getContext(),
                        DividerItemDecoration.VERTICAL);
                horizontalDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.horizontal_divider));
                popularPlacesGridView.addItemDecoration(horizontalDecoration);
                popularPlacesView.setVisibility(View.VISIBLE);
                adapterHeaderTextView.setVisibility(View.GONE);

                handleThePreselecteStationsIfAny();
            } else {
                fromSearchView.setEnabled(false);
                goingToSearchView.setEnabled(false);
                popularPlacesView.setVisibility(View.GONE);
                List<String> errors = response.getError().getGlobalErrors();
                String errMsg = "";
                for (int i = 0; i < errors.size(); i++) {
                    if (i == 0 || i == errors.size() - 1) {
                        errMsg = errMsg + errors.get(i);
                    } else {
                        errMsg = errMsg + errors.get(i) + ",";
                    }
                }

                Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
                if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                    SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                    startActivity(new Intent(this, SplashActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
            }
        }
    }

    private void handleThePreselecteStationsIfAny() {
        if (preSelectedStation.equalsIgnoreCase(Constants.FROM_STATION_NAME) && fromStationId != null) {
            popularPlacesView.setVisibility(View.GONE);
            searchViewsRecyclerView.setVisibility(View.VISIBLE);
            fromSearchView.setQuery(fromStationName, false);
            adapter.getFilter().filter(fromStationName);
            adapterHeaderTextView.setVisibility(View.VISIBLE);
            adapterHeaderTextView.setText(R.string.select_from_city);
        } else if (preSelectedStation.equalsIgnoreCase(Constants.TO_STATION_NAME) && toStationId != null) {
            goingToSearchLayout.setVisibility(View.VISIBLE);
            popularPlacesView.setVisibility(View.GONE);
            searchViewsRecyclerView.setVisibility(View.VISIBLE);
            adapterHeaderTextView.setVisibility(View.VISIBLE);
            adapterHeaderTextView.setText(R.string.select_to_city);
            goingToSearchView.requestFocus();
        }
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
        if (error.equalsIgnoreCase("Un-authorized acess")) {
            SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
            startActivity(new Intent(this, SplashActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    public void updateSearchview(Station station) {
        switch (searchViewType) {
            case LEAVING_FROM_TYPE:
                fromStationName = station.getName();
                fromStationId = station.getId();
                fromSearchView.setQuery(station.getName(), true);
                fromSearchView.clearFocus();
//                fromSearchView.setEnabled(false);

                if (popularPlacesView.getVisibility() == View.GONE) {
                    adapterHeaderTextView.setVisibility(View.VISIBLE);
                    adapterHeaderTextView.setText(R.string.select_to_city);
                }
                break;

            case GOING_TO_TYPE:
                toStationName = station.getName();
                toStationId = station.getId();
                goingToSearchView.setQuery(station.getName(), true);
                goingToSearchView.clearFocus();
                adapterHeaderTextView.setVisibility(View.GONE);
//                goingToSearchView.setEnabled(false);

                if (toStationId.equalsIgnoreCase(fromStationId)) {
                    Toast.makeText(this, "From City and To City cannot be same", Toast.LENGTH_SHORT).show();
                } else if (fromStationId != null && fromStationName != null && toStationId != null && toStationName != null) {
                    startActivity(new Intent(this, HomeActivity.class)
                            .putExtra(Constants.FROM_STATION_ID, fromStationId)
                            .putExtra(Constants.TO_STATION_ID, toStationId)
                            .putExtra(Constants.FROM_STATION_NAME, fromStationName)
                            .putExtra(Constants.TO_STATION_NAME, toStationName)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (fromSearchView.hasFocus())
            fromSearchView.clearFocus();
        if (goingToSearchView.hasFocus())
            goingToSearchView.clearFocus();
        super.onBackPressed();
    }

    private List<Station> getPopularStationList(List<Station> stations) {
        List<Station> popList = new ArrayList<>();
        for (Station pop : stations) {
            if (pop.getName().contains("Bangalore")
                    || pop.getName().contains("Chennai")
                    || pop.getName().contains("Madurai")
                    || pop.getName().contains("Hyderabad")
                    || pop.getName().contains("Cochin")
                    || pop.getName().contains("Ernakulam")
                    || pop.getName().contains("Kodaikanal")
                    || pop.getName().contains("Coimbatore")) {
                popList.add(pop);
            }
        }

        return popList;
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader,null,this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader,null,this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK){
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
