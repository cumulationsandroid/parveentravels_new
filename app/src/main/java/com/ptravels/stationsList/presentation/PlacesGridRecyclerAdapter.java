package com.ptravels.stationsList.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ptravels.R;
import com.ptravels.stationsList.data.model.response.Station;

import java.util.List;

/**
 * Created by Amit Tumkur on 19-12-2017.
 */

public class PlacesGridRecyclerAdapter extends RecyclerView.Adapter<PlacesGridRecyclerAdapter.MyViewHolder> {

    private Context context;
    private List<Station> stationList;

    public PlacesGridRecyclerAdapter(Context context, List<Station> stationList) {
        this.context = context;
        this.stationList = stationList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView placeName;
        RelativeLayout placeItemLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            placeName = itemView.findViewById(R.id.placeName);
            placeItemLayout = itemView.findViewById(R.id.placeItemLayout);
        }
    }

    @Override
    public PlacesGridRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.popular_places_grid_item, parent, false);
        return new PlacesGridRecyclerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlacesGridRecyclerAdapter.MyViewHolder myViewHolder, final int position) {
        final Station station = stationList.get(position);
        myViewHolder.placeName.setText(station.getName());

        myViewHolder.placeItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SearchActivity) context).updateSearchview(station);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stationList != null ? stationList.size() : 0;
    }

}
