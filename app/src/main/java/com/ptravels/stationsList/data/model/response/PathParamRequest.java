package com.ptravels.stationsList.data.model.response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class PathParamRequest {
    private String token;
    private String pnr;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }
}
