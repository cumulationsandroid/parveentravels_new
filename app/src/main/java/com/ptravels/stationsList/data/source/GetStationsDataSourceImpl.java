package com.ptravels.stationsList.data.source;

import com.ptravels.ParveenApp;
import com.ptravels.REST.RestClient;
import com.ptravels.stationsList.data.model.response.StationsListResponse;
import com.ptravels.stationsList.domain.GetStationsDataInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class GetStationsDataSourceImpl implements GetStationsDataInterface {
    @Override
    public Single<StationsListResponse> getStations() {
        /*RestClient.enableMockApi = true;
        return RestClient.getApiService().getMockStations()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/
        return RestClient.getApiService().getStations(ParveenApp.getAccessToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
