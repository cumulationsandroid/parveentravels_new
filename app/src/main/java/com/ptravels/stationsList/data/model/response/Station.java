package com.ptravels.stationsList.data.model.response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Station {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
