package com.ptravels.stationsList.data.model.response;

import java.util.List;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Data {
    private List<Station> stations = null;

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }
}
