package com.ptravels.REST;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.myBooking.data.model.request.CancelTicketRequest;
import com.ptravels.myBooking.data.model.response.CancelTicketResponse;
import com.ptravels.myBooking.data.model.response.TicketDetailResponse;
import com.ptravels.seatlayout.data.model.request.BusMapRequest;
import com.ptravels.seatlayout.data.model.response.BusMapResponse;
import com.ptravels.myBooking.data.model.request.MyBookingRequest;
import com.ptravels.myBooking.data.model.response.HistoryResponse;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.stationsList.data.model.response.StationsListResponse;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface ApiService {
    @GET("{accessToken}/station.json")
    Single<StationsListResponse> getStations(@Path("accessToken") String token);

    @POST("{accessToken}/search.json")
    Single<BusSearchResponse> getBusSearchResults(@Path("accessToken") String token,
                                                  @Body BusSearchRequest request);

    @POST("register.json")
    Single<SignupResponse> getSignupResponse(@Body SignupRequest request);

    @POST("login.json")
    Single<SigninResponse> getSigninResponseSingle(@Body SigninRequest request);

    @POST("{accessToken}/busmap.json")
    Single<BusMapResponse> getBusMapLayout(@Path("accessToken") String token,
                                           @Body BusMapRequest request);

    @POST("{accessToken}/ticket/block.json")
    Single<BlockTicketResponse> blockTicket(@Path("accessToken") String token,@Body BlockTicketRequest request);

    @POST("{accessToken}/ticket/history.json")
    Single<HistoryResponse> getMyBookingDetails(@Path("accessToken") String token, @Body MyBookingRequest request);

    @POST("{accessToken}/ticket/confirm/cancel.json")
    Single<CancelTicketResponse> cancelTicket(@Path("accessToken") String token,@Body CancelTicketRequest cancelTicketRequest);

    @GET("{accessToken}/payment/gateway.json")
    Single<GatewayResponse> getPaymentGateways(@Path("accessToken") String token);

    @POST("{accessToken}/preTransactionRedirect.json")
    Single<PreTransactionResponse> getPreTransactionRedirect(@Path("accessToken") String token, @Body PreTransactionRequest request);

    @POST("{accessToken}/ticket/book.json")
    Single<BookTicketResponse> bookTicket(@Path("accessToken") String token, @Body BookTicketRequest request);

    @POST("{accessToken}/{pnr}/receive.json")
    Single<ReceiveResponse> receivePaytmInfo(@Path("accessToken") String token, @Path("pnr") String pnr);

    @POST("{accessToken}/ticket/{pnr}/detail.json")
    Single<TicketDetailResponse> getTicketDetails(@Path("accessToken") String token, @Path("pnr") String pnr);





    /*Mock*/
    @POST("/login")
    Single<SigninResponse> getMockLogin(@Body SigninRequest request);

    @GET("/station")
    Single<StationsListResponse> getMockStations();

    @POST("/search")
    Single<BusSearchResponse> getMockBusSearchResults();

//    @POST("/busmap")
//    Single<BusMapResponse> getBusMapResults(@Body BusMapRequest request);

    @POST("/busmap_sleeper")
    Single<BusMapResponse> getBusMapResults(@Body BusMapRequest request);

//    @POST("/busmap_sleeper2")
//    Single<BusMapResponse> getBusMapResults(@Body BusMapRequest request);

    @POST("/history")
    Single<HistoryResponse> getMockMyBookingDetails(@Body MyBookingRequest request);

    @GET("/gateway")
    Single<GatewayResponse> getMockPaymentGatewayDetails();

    /*
    * {"gatewayMode":"CC","paymentId":"paytm","pnr":"BSPT172789958"}*/
    @GET("/preTransactionRedirect")
    Single<PreTransactionResponse> getMockPreTransactionData();
}
