package com.ptravels.global;

import retrofit2.http.PUT;

/**
 * Created by Amit Tumkur on 04-01-2018.
 */

public class PaymentConstants {
    /*Staging Wallet Credentials:
    Mobile Number – 7777777777
    Password – Paytm12345
    OTP – 489871
    After every 5 minutes, the Wallet balance is topped up to 7,000 Rs.

    Staging CC/DC Credentials:
    Card Number -: Any live CC/DC number
    Expiry Date-: Any Future date from transaction date
    CVV-: Any three digit number
    OTP-: 1234 (To test successful transaction)
    Note-: To test fail transaction use any number in OTP field except 1234.*/

    public static final String ORDER_ID = "ORDER_ID";
    public static final String MID = "MID";
    public static final String CUST_ID = "CUST_ID";
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String INDUSTRY_TYPE_ID = "INDUSTRY_TYPE_ID";
    public static final String WEBSITE = "WEBSITE";
    public static final String TXN_AMOUNT = "TXN_AMOUNT";
    public static final String REQUEST_TYPE = "REQUEST_TYPE";
    public static final String THEME = "THEME";

    public static final String productionChecksumGenerationURL = "http://p.brsoftech.net/paytm/generateChecksum.php";
    public static final String productionChecksumValidationURL = "http://p.brsoftech.net/paytm/verifyChecksum.php";
    public static final String stagingChecksumGenerationURL = "http://111.93.236.131:8080/paytm/generateChecksum.php";
    public static final String stagingChecksumValidationURL = "http://111.93.236.131:8080/paytm/verifyChecksum.php";
    public static final String MID_VALUE = "PARVEE25827447456315";
    public static final String CHANNEL_ID_VALUE = "WAP";
    public static final String INDUSTRY_TYPE_ID_VALUE = "Retail92";
    public static final String WEBSITE_VALUE = "PARVEENWAP";
    public static final String REQUEST_TYPE_VALUE = "DEFAULT";
    public static final String CUST_ID_VALUE = "13";
    public static final String CALLBACK_URL = "CALLBACK_URL";
    public static final String CHECKSUMHASH = "CHECKSUMHASH";
    public static final String EMAIL = "EMAIL";
    public static final String MOBILE_NO = "MOBILE_NO";
    public static String ORDER_ID_VALUE = "68";
    public static final String TXN_AMOUNT_VALUE = "3";
    public static final String THEME_VALUE = "merchant";

    public static final String PAYTM_TEST_MOBILE_NUMBER = "7777777777";
    public static final String PAYTM_TEST_PASSWORD = "Paytm12345";
    public static final String PAYTM_TEST_OTP = "489871";
    public static final String TXN_SUCCESS= "TXN_SUCCESS";
    public static final String TXN_FAILURE= "TXN_FAILURE";
    public static final String ORDERID ="ORDERID";


    /*
    *
    * URL encoded String is
    * INDUSTRY_TYPE_ID=Retail92
    * &CHANNEL_ID=WEB
    * &CHECKSUMHASH=QQ0LWUD1Iu79DNjcrKBICjoV16qepeNx3Hu2IeGd1UWVBsiF53%2FrT%2FxpXYMuKiwYAtPoc06%2FoVosZi%2BbCzCkG2jpkxD73aAjcunSVxiuk7Y%3D
    * &MOBILE_NO=9442606831
    * &REQUEST_TYPE=DEFAULT
    * &TXN_AMOUNT=32.00
    * &MID=PARVEE25827447456315
    * &EMAIL=guest%40parveentravels.com
    * &CALLBACK_URL=https%3A%2F%2F111.93.236.131%3A8080%2Fparveen-travels%2Fpaytm%2Freceive%2F
    * &CUST_ID=BSPT581032009
    * &WEBSITE=PARVEENWEB
    * &ORDER_ID=BSPT581032009*/
}
