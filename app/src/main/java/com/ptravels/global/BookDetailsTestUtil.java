package com.ptravels.global;

import com.ptravels.seatlayout.data.model.BookedSeatModel;
import com.ptravels.seatlayout.data.model.response.BoardingPoint;
import com.ptravels.seatlayout.data.model.response.DroppingPoint;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;

import java.util.ArrayList;

/**
 * Created by Amit Tumkur on 02-01-2018.
 */

public class BookDetailsTestUtil {
    public static BookedSeatModel initBookedSeatModel(){
        BookedSeatModel model = new BookedSeatModel();

        ArrayList<LayoutDetail> list = new ArrayList<>();
        for (int i=0;i<2;i++) {
            LayoutDetail layoutDetail = new LayoutDetail();
            layoutDetail.setSeatId(5501+i);
            layoutDetail.setColumn(5+i);
            layoutDetail.setRow(i+1);
            layoutDetail.setTier(1);
            layoutDetail.setVertical(false);
            layoutDetail.setStatus("A");
            if (i%2==0) {
                layoutDetail.setAvailableFor("All");
            } else
                layoutDetail.setAvailableFor("FEMALE");
            layoutDetail.setFare((double) 821);
            layoutDetail.setServiceTax(41.05);
            layoutDetail.setSeatNumber(String.valueOf(10+(i+2)));
            list.add(layoutDetail);
        }

        model.setSelectedSeatList(list);

        BoardingPoint boardingPoint = new BoardingPoint();
        boardingPoint.setStationPointName("Madiwala");
        boardingPoint.setAddressLine1("No 06 Hosur Main Road Near Ayyappaswamy Temple");
        boardingPoint.setAddressLine2("Madiwala Banglore");
        boardingPoint.setMobileNumber("09901999118  080-42112796");
        boardingPoint.setContactPersonName("Staff");
        boardingPoint.setAddressLandMark("Opp to Bajaj Showroom");
        boardingPoint.setDay(1);
        boardingPoint.setLatitude(12.9226373);
        boardingPoint.setLongitude(77.6174442);
        boardingPoint.setAddressPinCode("560068");
        boardingPoint.setStationPointTime("22:00:00");
        boardingPoint.setStationPointId(174);

        model.setBoardingPoint(boardingPoint);

        DroppingPoint droppingPoint = new DroppingPoint();
        droppingPoint.setStationPointName("Ashok Pillar");
        droppingPoint.setAddressLine1("Ashok Pillar Junction");
        droppingPoint.setAddressLine2("Ashok Pillar,Chennai");
        droppingPoint.setMobileNumber("9952945555, 9600034799");
        droppingPoint.setContactPersonName("Staff");
        droppingPoint.setAddressLandMark("In front of Anjappar Hotel");
        droppingPoint.setDay(2);
        droppingPoint.setLatitude(13.0329064629);
        droppingPoint.setLongitude(80.2119343132);
        droppingPoint.setAddressPinCode("600083");
        droppingPoint.setStationPointTime("04:55:00");
        droppingPoint.setStationPointId(187);
        model.setDroppingPoint(droppingPoint);

        model.setBusType("2+2 Single Tier Semi Sleeper Premium Multiaxle A/C Mercedes Benz");
        model.setFromStationName("Bangalore");
        model.setToStationName("Chennai");
        model.setTotalPrice(862);
        model.setTravelDate("2018-01-03");

        return model;

    }
}
