package com.ptravels.global;

import java.util.List;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class ResponseError {
    private List<String> globalErrors;
    private List<FieldErrors> fieldErrors;

    public List<FieldErrors> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<FieldErrors> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public List<String> getGlobalErrors() {
        return globalErrors;
    }

    public void setGlobalErrors(List<String> globalErrors) {
        this.globalErrors = globalErrors;
    }

    @Override
    public String toString() {
        return "ResponseError{" +
                "globalErrors=" + globalErrors +
                ", fieldErrors=" + fieldErrors +
                '}';
    }
}
