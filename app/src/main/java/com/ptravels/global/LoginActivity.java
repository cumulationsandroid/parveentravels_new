package com.ptravels.global;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.ptravels.BaseActivity;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signin.domain.SigninUseCase;
import com.ptravels.signin.presentation.SigninActivity;
import com.ptravels.signin.presentation.SigninPresenterImpl;
import com.ptravels.signin.presentation.SigninUpdateView;
import com.ptravels.signup.data.model.SignupRequest;
import com.ptravels.signup.data.model.SignupResponse;
import com.ptravels.signup.domain.SignupUseCase;
import com.ptravels.signup.presentation.SignupActivity;
import com.ptravels.signup.presentation.SignupPresenter;
import com.ptravels.signup.presentation.SignupPresenterImpl;
import com.ptravels.signup.presentation.SignupView;
import com.ptravels.stationsList.SignInErrorTypes;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends BaseActivity implements View.OnClickListener, SigninUpdateView, SignupView {
    private TextView toolbarTitle;
    private CardView fbBtLayout;
    private CallbackManager callbackManager;
    private LoginManager fbLoginManager;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1001;
    private String TAG = LoginActivity.class.getName();
    private SigninPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        initFbCallbacks();
        initializeGoogleLogin();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitle = findViewById(R.id.title);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbarTitle.setText("Sign up / Login");

        setclickListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            mGoogleSignInClient.signOut();
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initViews() {
        fbBtLayout = findViewById(R.id.fb_login_btn_layout);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);
    }

    private void setclickListeners() {
        rxSingleClickListener(findViewById(R.id.backBtn));
        rxSingleClickListener(findViewById(R.id.signInBtn));
        rxSingleClickListener(findViewById(R.id.signUpBtn));
        rxSingleClickListener(findViewById(R.id.fb_login_btn_layout));
        rxSingleClickListener(findViewById(R.id.g_login_btn_layout));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signInBtn:
                startActivity(new Intent(LoginActivity.this, SigninActivity.class));
                break;
            case R.id.signUpBtn:
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                break;
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.fb_login_btn_layout:
                facebookLogin();
                break;
            case R.id.g_login_btn_layout:
                googleSignIn();
                break;
        }
    }

    private void initFbCallbacks() {
        fbLoginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();
        fbLoginManager.registerCallback(callbackManager, getFacecallBackInstance());
    }

    private FacebookCallback<LoginResult> getFacecallBackInstance() {
        return new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                fetchFbData(accessToken);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        };
    }

    private void fetchFbData(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, getFacbookResponseCallbackInstance());
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private GraphRequest.GraphJSONObjectCallback getFacbookResponseCallbackInstance() {
        return new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject jsonobject = response.getJSONObject();
                String fbUserId = jsonobject.optString("id");
                String fbUserName = jsonobject.optString("first_name");
                String name = jsonobject.optString("name");
                String email = jsonobject.optString("email");
                String fbLastName = jsonobject.optString("last_name");

                Log.e("fb response", fbUserId + "," + fbUserName + "," + name + "," + email + "," + fbLastName);
                requestLoginToServer(email, name);
            }

        };
    }

    private void facebookLogin() {
        ArrayList permissionList = new ArrayList();
        permissionList.add("email");
        permissionList.add("public_profile");

        fbLoginManager.logInWithReadPermissions(this, permissionList);
        /*val tcAccepted = sharedPreference?.getBoolean(Constants.TC_ACCEPTED, false)
        if (tcAccepted!!) {
            fbLoginManager?.logInWithReadPermissions(this, listOf("email", "public_profile"))
        } else {
            showTCDialog()
        }*/
    }

    private void initializeGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            Log.w(TAG, "signInResult:success" + account.getEmail());
            requestLoginToServer(account.getEmail(), account.getDisplayName());
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    @Override
    public void updateSigninResponse(SigninResponse response) {
        hideLoading();
        if (response.isSuccess()) {
            SigninRequest signinRequest = presenter.getSigninRequestBody();
            SharedPrefUtils.getSharedPrefUtils().setGuestUserLogin(LoginActivity.this, false);

            UserData userData = new UserData();
            userData.setEmail(signinRequest.getEmail());
            userData.setPassword(signinRequest.getPassword());
            userData.setAccessToken(response.getData());
            SharedPrefUtils.getSharedPrefUtils().saveUserDetails(this, userData);

            Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();

            startActivity(new Intent(this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (!response.isSuccess()) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
        }
    }

    private void requestLoginToServer(String email, String userName) {
        showLoading();
        SigninRequest request = new SigninRequest();
        request.setEmail(email);
        request.setPassword(Constants.DEFAULT_PASSWORD);
        request.setName(userName);

        presenter = new SigninPresenterImpl(new SigninUseCase(), this);
        presenter.getSigninResponse(request);
    }

    @Override
    public void updateSignupResponse(SignupResponse response) {
        hideLoading();
        if (response.getUsername()!=null){
            requestLoginToServer(response.getEmail(), response.getUsername());
        } else if (response.getError()!=null){
            String error = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showError(String error) {
        hideLoading();
        if (error.equals(SignInErrorTypes.EMAIL_ID_DOESNOT_EXSIT.getErrorType())) {
            showLoading();
            SigninRequest signinRequest = presenter.getSigninRequestBody();
            SignupRequest request = new SignupRequest();
            request.setFirstName(signinRequest.getName());
            request.setMobile(Constants.DEFAULT_MOBILE_NUMBER);
            request.setEmail(signinRequest.getEmail());
            request.setPassword(Constants.DEFAULT_PASSWORD);

            SignupPresenter presenter = new SignupPresenterImpl(new SignupUseCase(), this);
            presenter.getSignupResponse(request);

        } else if (error.equals(SignInErrorTypes.WRONG_PASSWORD.getErrorType())) {
            Intent intent = new Intent(LoginActivity.this, SigninActivity.class);
            intent.putExtra(Constants.SIGN_IN_DATA, presenter.getSigninRequestBody());
            startActivity(intent);
        } else {
            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
        }
    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });


        Subscription subscribe = clickViewObservable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(View view) {
                        onClick(view);
                    }
                });
    }
}
