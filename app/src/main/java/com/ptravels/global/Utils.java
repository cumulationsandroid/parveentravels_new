package com.ptravels.global;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;


import com.ptravels.ParveenApp;
import com.ptravels.R;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Anirudh Uppunda on 11/5/17.
 */

public class Utils {
    private static Utils utils;
    public HashMap<String,String> fiveToElevenMap = new HashMap<>();
    public HashMap<String,String> elevenToFiveMap = new HashMap<>();
    private AlertDialog enableNetworkDialog;

    private Utils() {
    }

    public static Utils getUtils() {
        if (utils == null)
            utils = new Utils();
        return utils;
    }

    public void initDeptMaps() {
        fiveToElevenMap.put("05", "AM");
        fiveToElevenMap.put("06", "AM");
        fiveToElevenMap.put("07", "AM");
        fiveToElevenMap.put("08", "AM");
        fiveToElevenMap.put("09", "AM");
        fiveToElevenMap.put("10", "AM");
        fiveToElevenMap.put("11", "AM");

        elevenToFiveMap.put("11", "PM");
        elevenToFiveMap.put("12", "PM");
        elevenToFiveMap.put("00", "AM");
        elevenToFiveMap.put("01", "PM");
        elevenToFiveMap.put("02", "PM");
        elevenToFiveMap.put("03", "PM");
        elevenToFiveMap.put("04", "PM");
        elevenToFiveMap.put("05", "PM");
    }

    public static boolean isOnline() {
        return isOnlineUsingCOntext();
    }

    public static boolean isOnlineUsingCOntext(){
        ConnectivityManager cm = (ConnectivityManager) ParveenApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();

    }

    public List<String> getNext5Days(String currentDate, int numDays) {

        List<String> next5Days = new ArrayList<>();
        if (currentDate != null) {
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            SimpleDateFormat returnDateFormat = new SimpleDateFormat("EEE, dd MMM", Locale.getDefault());
            SimpleDateFormat TodayTomFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());

            Calendar todayCalendar = Calendar.getInstance();

            String today = currentDateFormat.format(todayCalendar.getTime());
            todayCalendar.add(Calendar.DATE, 1);
            String tomorrow = currentDateFormat.format(todayCalendar.getTime());
//          String dt = "2008-12-11";  // currentDate
            for (int i = 0; i < numDays; i++) {
            /*Fri, 22 Nov*/
                Calendar c = Calendar.getInstance();


                try {
                    c.setTime(currentDateFormat.parse(currentDate));
                    c.add(Calendar.DATE, i);  // number of days to add

                    String day;

                    if (today.equalsIgnoreCase(currentDateFormat.format(c.getTime()))) {
                        day = "Today, " + TodayTomFormat.format(c.getTime());
                    } else if (tomorrow.equalsIgnoreCase(currentDateFormat.format(c.getTime()))) {
                        day = "Tom, " + TodayTomFormat.format(c.getTime());
                    } else {
                        day = returnDateFormat.format(c.getTime());

                    }

                    Log.i("getNext5Days", day);
                    next5Days.add(day);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return next5Days;
    }

    public List<String> getNext5DaysForIntent(String currentDate, int numDays) {
        List<String> next5Days = new ArrayList<>();
//          String dt = "2008-12-11";  // currentDate
        for (int i = 0; i < numDays; i++) {
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(currentDateFormat.parse(currentDate));
                c.add(Calendar.DATE, i);  // number of days to add

                String day = currentDateFormat.format(c.getTime());
//                currentDate = day;
                Log.i("getNext5DaysForIntent", day);
                next5Days.add(day);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return next5Days;
    }

    public String getCurrentDateSelected() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public int toPixels(Context context, int dp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public boolean isValidPhoneNumber(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

    public void showLoader(ImageView loadingBar, TextView loadingText, Context context) {
        loadingBar.setVisibility(View.VISIBLE);
        if (loadingText != null)
            loadingText.setVisibility(View.VISIBLE);
        Animation rotation = AnimationUtils.loadAnimation(context, R.anim.rotate);
        rotation.setFillAfter(true);
        loadingBar.startAnimation(rotation);
        AppCompatActivity appCompatActivity = (AppCompatActivity) context;
        appCompatActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void closeLoader(ImageView loadingBar, TextView loadingText, Context context) {
        loadingBar.setVisibility(View.GONE);
        if (loadingText != null)
            loadingText.setVisibility(View.GONE);
        loadingBar.clearAnimation();
        AppCompatActivity appCompatActivity = (AppCompatActivity) context;
        appCompatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    static public String formatDateString(String initialFormat, String finalFormat, String dateString) {
        try {
            SimpleDateFormat initialDateFormat = new SimpleDateFormat(initialFormat);
            Date date = initialDateFormat.parse(dateString);
            SimpleDateFormat updatedDateFormat = new SimpleDateFormat(finalFormat);
            return updatedDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long getDeptTimeInMillis(String travelDate, String travelDateFormat,
                                           String deptTime, String deptTimeFormat) {

        String toParse = travelDate + " " + deptTime; // Results in "22/06/2016 10:12 AM"
        SimpleDateFormat formatter = new SimpleDateFormat(travelDateFormat + " " + deptTimeFormat, Locale.US); // I assume d-M, you may refer to M-d for month-day instead.
        Date date = null;
        long millis = 0;
        try {
            date = formatter.parse(toParse);
            millis = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        System.out.println("Today Date Is ==" + date + "");
//        System.out.println("Today Date Is millis==" + millis + "");
        return millis;
    }

    public String initOrderId() {
        Random r = new Random(System.currentTimeMillis());
        String orderId = "ORDER" + (1 + r.nextInt(2)) * 10000
                + r.nextInt(10000) + System.currentTimeMillis() + "";
        return orderId;
    }

    public void closeKeyboard(Context context, View view) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((view == null) ? null : view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public static void printHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.ptravels",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public long getArrivalTimeMillis(String travelDate,String travelDateFormat,
                                     String arrivalTime, String arrivalFormat,
                                     String departureTime) {

        long millis = 0;
        try {
            String newTravelDate = travelDate;
            if ((arrivalTime.contains("am") || arrivalTime.contains("AM"))
                    && (!(departureTime.contains("am") || departureTime.contains("AM")))) {
                // travelDate = 2018-01-12
                String[] strings = travelDate.split("-");
                newTravelDate = strings[0] + "-" + strings[1] + "-" + (Integer.valueOf(strings[2]) + 1);
            }

            String toParse = newTravelDate + " " + arrivalTime; // Results in "2-5-2012 20:43"
            SimpleDateFormat formatter = new SimpleDateFormat(travelDateFormat + " " + arrivalFormat, Locale.getDefault()); // I assume d-M, you may refer to M-d for month-day instead.
            Date date = null;
            try {
                date = formatter.parse(toParse);
                millis = date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println("Today Date Is millis==" + millis + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return millis;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public void showEnableInternetDialog(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(context.getString(R.string.title));

        // set dialog message
        alertDialogBuilder
                .setMessage(context.getString(R.string.enableNet))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        enableNetworkDialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(intent);

                    }
                })
                /*.setNegativeButton(context.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        enableNetworkDialog.dismiss();
//                        ((AppCompatActivity) context).finish();
                    }
                })*/;

        // create alert dialog
        if (enableNetworkDialog!=null && enableNetworkDialog.isShowing())
            enableNetworkDialog.dismiss();
        enableNetworkDialog = alertDialogBuilder.create();
        if (context instanceof AppCompatActivity && !((AppCompatActivity) context).isFinishing())
            enableNetworkDialog.show();
    }

    public void dismissNetworkDialog(){
        if (enableNetworkDialog!=null && enableNetworkDialog.isShowing())
            enableNetworkDialog.dismiss();
    }

}
