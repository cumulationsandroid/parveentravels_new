package com.ptravels.signin.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.global.LoginActivity;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.UserData;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signin.domain.SigninUseCase;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SigninActivity extends BaseActivity implements View.OnClickListener,SigninUpdateView {
    private TextView toolbarTitle;
    private TextInputLayout emailMobileInputLayout,pwdInputLayout;
    private EditText emailEditText,pwdEditText;
    private View emailView,pwdView;
    private FrameLayout loaderLayout;
    private ImageView loader;
    private UserData userData;
    private AlertDialog webDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        if (SharedPrefUtils.getSharedPrefUtils().isUserLoggedIn(this)
                && !SharedPrefUtils.getSharedPrefUtils().isGuestUserLogin(this)) {
            startActivity(new Intent(this, LoginActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            Toast.makeText(this, "Already Logged In !!", Toast.LENGTH_SHORT).show();
            finish();
        }
        initViews();
        setclickListeners();
        if(getIntent()!=null && getIntent().getSerializableExtra(Constants.SIGN_IN_DATA)!=null){
            signInUIForSocialSigin();
        }
    }

    private void signInUIForSocialSigin() {
        SigninRequest signinRequest = (SigninRequest) getIntent().getSerializableExtra(Constants.SIGN_IN_DATA);
        emailEditText.setText(signinRequest.getEmail());
        emailEditText.post(new Runnable() {
            @Override
            public void run() {
                emailEditText.setSelection(emailEditText.getText().length());
                emailEditText.clearFocus();
            }
        });
        emailEditText.setEnabled(false);
    }

    private void initViews(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitle = findViewById(R.id.title);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbarTitle.setText("Login");

        emailView = findViewById(R.id.emailMobileInputLayout);
        pwdView = findViewById(R.id.pwdInputLayout);
        emailMobileInputLayout = emailView.findViewById(R.id.input_layout);
        emailEditText = emailView.findViewById(R.id.input_et);
        pwdInputLayout = pwdView.findViewById(R.id.input_layout);
        pwdEditText = pwdView.findViewById(R.id.input_et);
        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        emailMobileInputLayout.setHint("Enter your email ID");
        pwdInputLayout.setHint("Password");

        if (getIntent()!=null && getIntent().hasExtra(Constants.USER_DATA)){
            userData = (UserData) getIntent().getSerializableExtra(Constants.USER_DATA);
        }

        if (userData!=null) {
            emailEditText.setText(userData.getEmail());
            emailEditText.post(new Runnable() {
                @Override
                public void run() {
                    emailEditText.setSelection(emailEditText.getText().length());
                    emailEditText.clearFocus();
                }
            });
        }
		SpannableString contentText = clickableStrings(getResources().getString((R.string.signin_agree)));
        TextView textView= findViewById(R.id.agreeTermsText);
        textView.setText(contentText);
        textView.setAutoLinkMask(RESULT_OK);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
        Linkify.addLinks(contentText, Linkify.WEB_URLS);
    }

    private void setclickListeners() {
        rxSingleClickListener(findViewById(R.id.backBtn));
        rxSingleClickListener(findViewById(R.id.loginBtn));
    }

    @Override
    public void onClick(View view) {
        Utils.getUtils().closeKeyboard(this,getCurrentFocus());
        switch (view.getId()){
            case R.id.loginBtn:
                if (validateFields()){
                    showLoading();
                    SigninRequest request = new SigninRequest();
                    request.setEmail(emailEditText.getText().toString().trim());
                    request.setPassword(pwdEditText.getText().toString().trim());

                    SigninPresenter presenter = new SigninPresenterImpl(new SigninUseCase(),this);
                    presenter.getSigninResponse(request);
                }
                break;
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private boolean validateFields(){
        emailEditText.clearFocus();
        pwdEditText.clearFocus();
        emailMobileInputLayout.setErrorEnabled(false);
        pwdInputLayout.setErrorEnabled(false);
        emailMobileInputLayout.setError(null);
        pwdInputLayout.setError(null);
        if (emailEditText.getText().toString().isEmpty()){
            emailMobileInputLayout.setErrorEnabled(true);
            emailMobileInputLayout.setError("Email is mandatory");
            emailEditText.requestFocus();
            return false;
        } else if (pwdEditText.getText().toString().isEmpty()){
            pwdInputLayout.setErrorEnabled(true);
            pwdInputLayout.setError("Password is mandatory");
            pwdEditText.requestFocus();
            return false;
        } else if (!Utils.getUtils().isValidEmail(emailEditText.getText().toString())){
            emailMobileInputLayout.setErrorEnabled(true);
            emailMobileInputLayout.setError("Invalid Email");
            emailEditText.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader,null,this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader,null,this);
    }

    @Override
    public void updateSigninResponse(SigninResponse response) {
        hideLoading();
        if (response == null /*|| userData == null*/)
            return;

        if (userData == null)
            userData = new UserData();
        if (response.isSuccess()) {
            SharedPrefUtils.getSharedPrefUtils().setGuestUserLogin(SigninActivity.this, false);
            userData.setEmail(emailEditText.getText().toString());
            userData.setPassword(pwdEditText.getText().toString());
            userData.setAccessToken(response.getData());
            SharedPrefUtils.getSharedPrefUtils().saveUserDetails(this, userData);

            Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();

            startActivity(new Intent(this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (!response.isSuccess()){
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : "+errMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showError(String error) {
        hideLoading();
        Toast.makeText(this, "error : "+error, Toast.LENGTH_SHORT).show();
    }

    private SpannableString clickableStrings(String sampleString) {
        SpannableString spannableString = new SpannableString(sampleString);
        ClickableSpan termsConditionsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                showWebAlertDialog("file:///android_res/raw/terms_condition.html");
            }
        };


        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                showWebAlertDialog("file:///android_res/raw/privacy_policy.html");
            }
        };

        return formatSpannableString(spannableString,termsConditionsClick,privacyClick);
    }

    private SpannableString formatSpannableString(SpannableString spannableString,
                                                  ClickableSpan termsClicked, ClickableSpan privacyClicked){
        spannableString.setSpan(termsClicked,31,51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(privacyClicked,54,68, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_btn)), 31,51,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_btn)), 54,68,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void showWebAlertDialog(String fileUrl) {
        if (webDialog!=null && webDialog.isShowing())
            webDialog.dismiss();

        WebView webView = new WebView(this);
        webView.loadUrl(fileUrl);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(webView)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (webDialog!=null && webDialog.isShowing())
                            webDialog.dismiss();
                        webDialog = null;
                    }
                });
        if (webDialog == null)
            webDialog = builder.create();
        if (!webDialog.isShowing() && !isFinishing())
            webDialog.show();
    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });

        clickViewObservable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(View view) {
                        onClick(view);
                    }
                });
    }
}
