package com.ptravels.signin.presentation;

import com.ptravels.signin.data.model.SigninRequest;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface SigninPresenter {
    void getSigninResponse(SigninRequest request);
}
