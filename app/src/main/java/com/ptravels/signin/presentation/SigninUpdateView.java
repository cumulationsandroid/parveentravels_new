package com.ptravels.signin.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.signin.data.model.SigninResponse;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public interface SigninUpdateView extends LoadingView{
    void updateSigninResponse(SigninResponse response);
    void showError(String error);
}
