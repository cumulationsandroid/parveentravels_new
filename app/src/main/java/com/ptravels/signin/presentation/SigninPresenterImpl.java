package com.ptravels.signin.presentation;

import com.ptravels.global.Constants;
import com.ptravels.global.Utils;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signin.domain.SigninUseCase;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

import static com.ptravels.REST.RestClient.getErrorMessage;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SigninPresenterImpl implements SigninPresenter {
    private SigninUseCase loginUseCase;
    private SigninUpdateView loginUpdateView;
    private SigninRequest signinRequestBody;

    public SigninPresenterImpl(SigninUseCase loginUseCase, SigninUpdateView loginUpdateView) {
        this.loginUseCase = loginUseCase;
        this.loginUpdateView = loginUpdateView;
    }

    @Override
    public void getSigninResponse(SigninRequest request) {
        if (Utils.isOnline()) {
            signinRequestBody = request;
            loginUseCase.execute(request)
                    .subscribe(new SingleSubscriber<SigninResponse>() {
                        @Override
                        public void onSuccess(SigninResponse value) {
                            if (value.getData() != null) {
                                loginUpdateView.updateSigninResponse(value);
                            } else if (value.getError() != null && value.getError().getGlobalErrors() != null) {
                                loginUpdateView.showError(value.getError().getGlobalErrors().get(0));
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            try {
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    loginUpdateView.showError(getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    loginUpdateView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    loginUpdateView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                loginUpdateView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            loginUpdateView.showError(Constants.NO_INTERNET);
        }
    }

    public SigninRequest getSigninRequestBody() {
        return signinRequestBody;
    }
}
