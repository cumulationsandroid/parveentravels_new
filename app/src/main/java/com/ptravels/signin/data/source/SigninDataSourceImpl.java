package com.ptravels.signin.data.source;

import com.ptravels.REST.RestClient;
import com.ptravels.signin.data.model.SigninRequest;
import com.ptravels.signin.data.model.SigninResponse;
import com.ptravels.signin.domain.SigninDataInterface;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Amit Tumkur on 14-12-2017.
 */

public class SigninDataSourceImpl implements SigninDataInterface {

    @Override
    public Single<SigninResponse> getSigninResult(SigninRequest request) {
        /*RestClient.enableMockApi = true;

        return RestClient.getApiService().getMockLogin(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/

        return RestClient.getApiService().getSigninResponseSingle(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
