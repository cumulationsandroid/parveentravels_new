package com.ptravels;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;

import com.ptravels.global.Constants;
import com.ptravels.global.Utils;

public class BaseActivity extends AppCompatActivity {
    private IntentFilter networkChangeIntentFilter;
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                Utils utils = Utils.getUtils();
                if (!utils.isNetworkAvailable(context)) {
                    if (!BaseActivity.this.isFinishing())
                        utils.showEnableInternetDialog(BaseActivity.this);
                } else {
                    utils.dismissNetworkDialog();
                    netBack(true);
                }
            }
        }
    };
    private boolean isReceiverRegistered;

    @Override
    protected void onStart() {
        super.onStart();
        networkChangeIntentFilter = new IntentFilter();
        networkChangeIntentFilter.addAction(/*Constants.NETWORK_CHANGED*/"android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkChangeReceiver, networkChangeIntentFilter);
        isReceiverRegistered = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils utils = Utils.getUtils();
        if (!utils.isNetworkAvailable(BaseActivity.this)) {
            if (!BaseActivity.this.isFinishing())
                utils.showEnableInternetDialog(BaseActivity.this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (networkChangeReceiver != null && isReceiverRegistered)
                unregisterReceiver(networkChangeReceiver);
            isReceiverRegistered = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void netBack(boolean isBack){

    }
}
