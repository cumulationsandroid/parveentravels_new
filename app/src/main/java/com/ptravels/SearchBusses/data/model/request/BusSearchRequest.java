package com.ptravels.SearchBusses.data.model.request;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BusSearchRequest {
    private String fromStation;
    private String toStation;
    private String travelDate;

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }
}
