package com.ptravels.SearchBusses.data.model.response;

/**
 * Created by Amit Tumkur on 28-12-2017.
 */

public class BusDetails {
    private String seatType;
    private String busType;

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    @Override
    public String toString() {
        return "ClassPojo [seatType = " + seatType + ", busType = " + busType + "]";
    }
}
