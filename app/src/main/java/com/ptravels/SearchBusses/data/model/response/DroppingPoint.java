package com.ptravels.SearchBusses.data.model.response;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class DroppingPoint {
    private String stationPointName;
    private String addressLine1;
    private String addressLine2;
    private String mobileNumber;
    private String contactPersonName;
    private String addressLandMark;
    private Integer day;
    private Double latitude;
    private Double longitude;
    private String addressPinCode;
    private String stationPointTime;
    private Integer stationPointId;

    public String getStationPointName() {
        return stationPointName;
    }

    public void setStationPointName(String stationPointName) {
        this.stationPointName = stationPointName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getAddressLandMark() {
        return addressLandMark;
    }

    public void setAddressLandMark(String addressLandMark) {
        this.addressLandMark = addressLandMark;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddressPinCode() {
        return addressPinCode;
    }

    public void setAddressPinCode(String addressPinCode) {
        this.addressPinCode = addressPinCode;
    }

    public String getStationPointTime() {
        return stationPointTime;
    }

    public void setStationPointTime(String stationPointTime) {
        this.stationPointTime = stationPointTime;
    }

    public Integer getStationPointId() {
        return stationPointId;
    }

    public void setStationPointId(Integer stationPointId) {
        this.stationPointId = stationPointId;
    }
}
