package com.ptravels.SearchBusses.data.model.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class Schedule {
    private String arrivalTime;
    private String departureTime;
    private Integer availableSeats;
    private Integer totalSeats;
    private String busType;
    private String routeName;
    private String operatorDisplayName;
    private String serviceNumber;
    private List<Fare> fares = null;
    private List<BoardingPoint> boardingPoints = null;
    private List<DroppingPoint> droppingPoints = null;
    private String scheduleId;
    private List<String> amenityList;
    private List<BusDetails> busDetails;

    private long arrivalTimeMillis;
    private long departureTimeMillis;
    private String duration;

    public long getArrivalTimeMillis() {
        return arrivalTimeMillis;
    }

    public void setArrivalTimeMillis(long arrivalTimeMillis) {
        this.arrivalTimeMillis = arrivalTimeMillis;
    }

    public long getDepartureTimeMillis() {
        return departureTimeMillis;
    }

    public void setDepartureTimeMillis(long departureTimeMillis) {
        this.departureTimeMillis = departureTimeMillis;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }

    public Integer getTotalSeats() {
        return totalSeats;
    }

    public void setTotalSeats(Integer totalSeats) {
        this.totalSeats = totalSeats;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getOperatorDisplayName() {
        return operatorDisplayName;
    }

    public void setOperatorDisplayName(String operatorDisplayName) {
        this.operatorDisplayName = operatorDisplayName;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(List<Fare> fares) {
        this.fares = fares;
    }

    public List<BoardingPoint> getBoardingPoints() {
        return boardingPoints;
    }

    public void setBoardingPoints(List<BoardingPoint> boardingPoints) {
        this.boardingPoints = boardingPoints;
    }

    public List<DroppingPoint> getDroppingPoints() {
        return droppingPoints;
    }

    public void setDroppingPoints(List<DroppingPoint> droppingPoints) {
        this.droppingPoints = droppingPoints;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public List<String> getAmenityList() {
        return amenityList;
    }

    public void setAmenityList(List<String> amenityList) {
        this.amenityList = amenityList;
    }

    public List<BusDetails> getBusDetails() {
        return busDetails;
    }

    public void setBusDetails(List<BusDetails> busDetails) {
        this.busDetails = busDetails;
    }
}
