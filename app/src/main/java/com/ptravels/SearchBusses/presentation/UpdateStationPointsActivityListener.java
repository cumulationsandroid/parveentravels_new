package com.ptravels.SearchBusses.presentation;

import com.ptravels.SearchBusses.data.model.response.BoardingPoint;
import com.ptravels.SearchBusses.data.model.response.DroppingPoint;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Amit Tumkur on 08-01-2018.
 */

public interface UpdateStationPointsActivityListener {
    /*void updateFilterStationPoints(HashMap<Integer,BoardingPoint> boardingPointHashMap,
                                   HashMap<Integer,DroppingPoint> droppingPointHashMap);*/
    void updateFilterStationPoints(List<String> brPts,List<String> drPts);
}
