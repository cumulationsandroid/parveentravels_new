package com.ptravels.SearchBusses.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.ptravels.R;

import java.util.List;

/**
 * Created by praveenkumar on 09/01/18.
 *
 * This is the adapter you can extend to show the shimmer loader before the actual data
 *  It works by looking at the dataList array list size, if the size is 0 it will add shimmer placeholder playout
 *
 * */

class ShimmerLoaderAdapter<T> extends RecyclerView.Adapter<ShimmerLoaderAdapter.ShimmerViewHolder>{

    Boolean isShimmerDataActive =true;
    List<T> dataList;


    /** This function is called when you first initilaze the adapter without calling the refresh/notify datachanged
     **/
    void setTheDataList(List<T> dataList)
    {
        this.dataList=dataList;
        if(dataList.size()==0){
            isShimmerDataActive =true;
        }else
            isShimmerDataActive =false;

    }

    /** This function is called when you refresh the adapter after you fetch the data
     **/
    void setTheDataListWithoutShimmer(List<T> dataList, Boolean isDataEmpty)
    {
        isShimmerDataActive =false;
        this.dataList=dataList;

    }

    List<T> getTheDataElement(){
        return this.dataList;

    }


    @Override
    public ShimmerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ShimmerViewHolder holder, int position) {
            if(isShimmerDataActive)
            {
                ShimmerFrameLayout mShimmerViewContainer = (ShimmerFrameLayout) holder.placeholder.findViewById(R.id.placeholderItem);
                holder.placeholder.setVisibility(View.VISIBLE);
                mShimmerViewContainer.setDuration(800);
                mShimmerViewContainer.startShimmerAnimation();


            }
            else {
                ShimmerFrameLayout mShimmerViewContainer = (ShimmerFrameLayout) holder.placeholder.findViewById(R.id.placeholderItem);
                holder.placeholder.setVisibility(View.GONE);
                mShimmerViewContainer.stopShimmerAnimation();

            }
    }

    @Override
   final public int getItemCount() {

        if (isShimmerDataActive){
            return 8;
        }else
        {
            return dataList.size();
        }
    }

     static class ShimmerViewHolder extends RecyclerView.ViewHolder{
        public  View placeholder;
        public ShimmerViewHolder(View itemView) {
            super(itemView);
            placeholder=itemView.findViewById(R.id.placeholder);
        }

    }
}
