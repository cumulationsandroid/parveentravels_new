package com.ptravels.SearchBusses.presentation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.R;
import com.ptravels.global.Constants;
import com.ptravels.REST.RestClient;
import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BoardingPoint;
import com.ptravels.SearchBusses.data.model.response.BusDetails;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.SearchBusses.data.model.response.Fare;
import com.ptravels.SearchBusses.data.model.response.Schedule;
import com.ptravels.SearchBusses.domain.BusSearchUseCase;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.presentation.BusMapActivity;
import com.ptravels.splash.presentation.SplashActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class SearchResultsFragment extends Fragment implements BusSearchView, FragmentApplyFiltersListener {
    private RecyclerView mRecyclerView;
    private SearchResultsAdapter mAdapter;
    private String fromId, toId, travelDate, fromStationName, toStationName;
    private BusSearchPresenter mBusSearchPresenter;
    private List<String> filters;
    private List<Schedule> searchResultsList = new ArrayList<>();

    private UpdateStationPointsActivityListener mCallback;
    private TextView noBusesView;

    public SearchResultsFragment() {
    }

    public static SearchResultsFragment initParamsInstance(String fromStationId, String toStationId,
                                                           String travelDate/*, List<String> filtersSelectedList*/, String fromStationName, String toStattionName) {
        SearchResultsFragment fragment = new SearchResultsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.FROM_STATION_ID, fromStationId);
        args.putString(Constants.TO_STATION_ID, toStationId);
        args.putString(Constants.TRAVEL_DATE, travelDate);
        args.putString(Constants.FROM_STATION_NAME, fromStationName);
        args.putString(Constants.TO_STATION_NAME, toStattionName);
//        args.putStringArrayList(Constants.FILTERS_LIST, (ArrayList<String>) filtersSelectedList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity) {
            // This makes sure that the container activity has implemented
            // the callback interface. If not, it throws an exception
            try {
                mCallback = (UpdateStationPointsActivityListener) context;
            } catch (ClassCastException e) {
                throw new ClassCastException(((AppCompatActivity) context).getClass().getSimpleName() +
                        " must implement UpdateStationPointsActivityListener");
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.FROM_STATION_ID)) {
                fromId = getArguments().getString(Constants.FROM_STATION_ID);
            }

            if (getArguments().containsKey(Constants.TO_STATION_ID)) {
                toId = getArguments().getString(Constants.TO_STATION_ID);
            }

            if (getArguments().containsKey(Constants.TRAVEL_DATE)) {
                travelDate = getArguments().getString(Constants.TRAVEL_DATE);
            }
            if (getArguments().containsKey(Constants.FROM_STATION_NAME)) {
                fromStationName = getArguments().getString(Constants.FROM_STATION_NAME);
            }
            if (getArguments().containsKey(Constants.TO_STATION_NAME)) {
                toStationName = getArguments().getString(Constants.TO_STATION_NAME);
            }

            /*if (getArguments().containsKey(Constants.FILTERS_LIST)){
                filters = getArguments().getStringArrayList(Constants.FILTERS_LIST);
            }*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_results, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        noBusesView = view.findViewById(R.id.no_buses_tv);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.cumulations_recycler_divider));
        mRecyclerView.addItemDecoration(divider);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new SearchResultsAdapter(getActivity(), new ArrayList<Schedule>(), this);
        mRecyclerView.setAdapter(mAdapter);

        showLoading();
        mBusSearchPresenter = new BusSearchPresenterImpl(this, new BusSearchUseCase());
        BusSearchRequest busSearchRequest = new BusSearchRequest();
        busSearchRequest.setFromStation(fromId);
        busSearchRequest.setToStation(toId);
        busSearchRequest.setTravelDate(travelDate);
        mBusSearchPresenter.getBusSearchResults(busSearchRequest);
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void showBusSearchResults(BusSearchResponse response, List<String> brPts, List<String> drPts) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            hideLoading();
            if (response == null)
                return;
            if (response.getSuccess() && getActivity() != null) {
                searchResultsList = response.getData().getSchedules();
                searchResultsList = showOnlyCurrentTimeResults(searchResultsList);
                filters = ((SearchResultsActivity) getActivity()).filtersSelectedList;
                if (filters != null && filters.size() > 0) {
                    filterResults(searchResultsList, filters);
                } else {
                    refreshAdapter(searchResultsList);
                }
                mCallback.updateFilterStationPoints(brPts, drPts);
            } else if (response.getError()!=null){
                String errMsg = RestClient.get200StatusErrors(response.getError());
                Toast.makeText(getActivity(), "error : " + errMsg, Toast.LENGTH_SHORT).show();
                refreshAdapter(new ArrayList<Schedule>());

                if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                    SharedPrefUtils.getSharedPrefUtils().clearUserData(getActivity());
                    startActivity(new Intent(getActivity(), SplashActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    getActivity().finish();
                }
            }
        }
    }


    List<Schedule> applyTimeFilterIfAny(List<Schedule> schedules, List<String> filters) {

        boolean isThisFIlterRequired = false;

        for (int f = 0; f < filters.size(); f++) {
            if (filters.get(f).contains("5am") || filters.get(f).contains("11am")) {
                isThisFIlterRequired = true;
                break;
            }
            if (filters.get(f).contains("5pm") || filters.get(f).contains("11pm")) {
                isThisFIlterRequired = true;
                break;
            }

        }

        if (!isThisFIlterRequired)
            return schedules;


        if (filters.size() == 0)
            return schedules;

        List<Schedule> filteredSchedules = new ArrayList<>();

        for (int f = 0; f < filters.size(); f++) {
            if (filters.get(f).contains("am") || filters.get(f).contains("pm")) {
                Utils utils = Utils.getUtils();
                utils.initDeptMaps();
                for (Schedule schedule : schedules) {
                    String depTime = schedule.getDepartureTime();
                    String[] depParams = depTime.split(":");
                    String hr = depParams[0];
                    String minAMPM = depParams[1];
                    String[] minAPMPMsplit = minAMPM.split(" ");
                    int min = Integer.parseInt(minAPMPMsplit[0]);

                    if (filters.get(f).equalsIgnoreCase("5am-11am")) {
                        if (depTime.contains("AM") && utils.fiveToElevenMap.containsKey(hr)) {
                            if (!filteredSchedules.contains(schedule) && !(hr.equals("11") && min > 0)) {
                                filteredSchedules.add(schedule);
                            }
                        }
                    } else if (filters.get(f).equalsIgnoreCase("5pm-11pm")) {
                        if (depTime.contains("PM") && utils.fiveToElevenMap.containsKey(hr)) {
                            if (!filteredSchedules.contains(schedule) && !(hr.equals("11") && min > 0)) {
                                filteredSchedules.add(schedule);
                            }
                        }
                    } else if (filters.get(f).equalsIgnoreCase("11am-5pm")) {

                        if (utils.elevenToFiveMap.containsKey(hr)) {
                            if (depTime.contains("AM")
                                    && (hr.equals("11") || hr.equals("12"))
                                    && !filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                            else if (depTime.contains("PM")
                                    && (hr.equals("00") || hr.equals("01") || hr.equals("02") || hr.equals("03") || hr.equals("04") || hr.equals("05"))
                                    && !(hr.equals("05") && min > 0)
                                    && !filteredSchedules.contains(schedule)) {
                                filteredSchedules.add(schedule);
                            }
                        }
                    } else if (filters.get(f).equalsIgnoreCase("11pm-5am")) {
                        if (utils.elevenToFiveMap.containsKey(hr)) {
                            if (depTime.contains("PM")
                                    && (hr.equals("11") || hr.equals("12"))
                                    && !filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                            else if (depTime.contains("AM")
                                    && (hr.equals("00") || hr.equals("01") || hr.equals("02") || hr.equals("03") || hr.equals("04") || hr.equals("05"))
                                    && !(hr.equals("05") && min > 0)
                                    && !filteredSchedules.contains(schedule)) {
                                filteredSchedules.add(schedule);
                            }
                        }
                    }
                }
            }


        }
        return filteredSchedules;
    }

    List<Schedule> applyACNonACFilterIfAny(List<Schedule> schedules, List<String> filters) {

        Boolean isThisFIlterRequired = false;
        for (int f = 0; f < filters.size(); f++) {

            if (filters.get(f).equalsIgnoreCase("AC")
                    || filters.get(f).equalsIgnoreCase("Non AC")
                    ) {
                isThisFIlterRequired = true;
                break;
            }
        }

        if (!isThisFIlterRequired)
            return schedules;

        List<Schedule> filteredSchedules = new ArrayList<>();

        for (int f = 0; f < filters.size(); f++) {

            if (filters.get(f).equalsIgnoreCase("AC")
                    || filters.get(f).equalsIgnoreCase("Non AC")
                    ) {
                for (Schedule schedule : schedules) {
                    List<BusDetails> busDetails = schedule.getBusDetails();
                    if (busDetails == null) {
                        Toast.makeText(getActivity(), "Filters for AC,Non Ac,Seater,Sleeper not working", Toast.LENGTH_SHORT).show();
                        return filteredSchedules;
                    }
                    for (BusDetails details : busDetails) {
                        if (details.getBusType().equals("AC") && filters.get(f).equalsIgnoreCase("AC")) {
                            if (!filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                            break;
                        }

                        if (details.getBusType().equals("NON AC") && filters.get(f).equalsIgnoreCase("Non AC")) {
                            if (!filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                            break;
                        }


                    }
                }
                break;
            }
        }
        return filteredSchedules;
    }

    List<Schedule> applySeaterSleeperIfAny(List<Schedule> schedules, List<String> filters) {

        Boolean isThisFIlterRequired = false;
        for (int f = 0; f < filters.size(); f++) {

            if (filters.get(f).equalsIgnoreCase("Seater")
                    || filters.get(f).equalsIgnoreCase("Sleeper")) {
                isThisFIlterRequired = true;
                break;
            }
        }

        if (!isThisFIlterRequired)
            return schedules;

        List<Schedule> filteredSchedules = new ArrayList<>();

        for (int f = 0; f < filters.size(); f++) {

            if (filters.get(f).equalsIgnoreCase("Seater")
                    || filters.get(f).equalsIgnoreCase("Sleeper")) {
                for (Schedule schedule : schedules) {
                    List<BusDetails> busDetails = schedule.getBusDetails();
                    if (busDetails == null) {
                        Toast.makeText(getActivity(), "Filters for Seater,Sleeper not working", Toast.LENGTH_SHORT).show();
                        return filteredSchedules;
                    }
                    for (BusDetails details : busDetails) {

                        if ((details.getSeatType().equals("Semi Sleeper") || details.getSeatType().equalsIgnoreCase("Seater"))
                                && filters.get(f).equalsIgnoreCase("Seater")) {
                            if (!filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                            break;
                        }

                        if (details.getSeatType().equals("Sleeper") && filters.get(f).equalsIgnoreCase("Sleeper")) {
                            if (!filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                            break;
                        }
                    }
                }
                break;
            }
        }
        return filteredSchedules;
    }

    List<Schedule> applyBoardingPointFilterIfAny(List<Schedule> schedules, List<String> filters) {

        if (filters.size() == 0)
            return schedules;


        if (checkIfBoardingPointFilterRequired(filters)) {
            List<Schedule> filteredSchedules = new ArrayList<>();
            for (int i = 0; i < filters.size(); i++) {

                for (Schedule schedule : schedules) {
                    List<BoardingPoint> boardingPoints = schedule.getBoardingPoints();
                    for (BoardingPoint point : boardingPoints) {
                        if (point.getStationPointName().equals(filters.get(i))) {
                            if (!filteredSchedules.contains(schedule))
                                filteredSchedules.add(schedule);
                        }
                    }
                }
            }
            return filteredSchedules;
        } else
            return schedules;

    }

    private boolean checkIfBoardingPointFilterRequired(List<String> filters) {

        if(filters.size()==0)
            return false;

        else {

              HashMap<String,String> map=new HashMap<>();

            for(int i=0;i<filters.size();i++) {
                map.put(filters.get(i),filters.get(i));
            }

              for(int i=0;i<filters.size();i++){

                    if (filters.get(i).contains("5am") || filters.get(i).contains("5pm")) {
                        map.remove(filters.get(i));
                        continue;
                    }
                    if (filters.get(i).contains("11am") || filters.get(i).contains("11pm")) {
                        map.remove(filters.get(i));
                        continue;
                    }

                    if (filters.get(i).equalsIgnoreCase("AC") || filters.get(i).contains("Non AC")) {
                        map.remove(filters.get(i));
                         continue;
                    }
                    if (filters.get(i).equalsIgnoreCase("Seater") || filters.get(i).contains("Sleeper")){
                        map.remove(filters.get(i));
                        continue;
                    }

                    if (filters.get(i).equalsIgnoreCase("Low to high") || filters.get(i).contains("High to low")){
                      map.remove(filters.get(i));
                      continue;
                    }

              }
                if (map.size()>0)
                return true;
                else
                return false;


        }
    }



    List<Schedule> applySortingFilterIfAny(List<Schedule> schedules,List<String> filters) {
        Boolean isThisFIlterRequired = false;

        for (int f = 0; f < filters.size(); f++) {
            if (filters.get(f).contains("Low to high") || filters.get(f).contains("High to low")) {
                isThisFIlterRequired = true;
                break;
            }
        }

        if (!isThisFIlterRequired)
            return schedules;

        for (int f = 0; f < filters.size(); f++) {

            if (filters.get(f).contains("Low to high")) {
                Collections.sort(schedules, new LowToHIghFareSorter());
                return schedules;

            } else if (filters.get(f).contains("High to low")) {
                Collections.sort(schedules, new HIghToLowFareSorter());
                return schedules;

            }

        }
        return schedules;
    }

    List<Schedule> sortTheSchedulesBasedOnDepartureTime(List<Schedule> schedules){
        if(schedules.size()<1)
            return schedules;

        Collections.sort(schedules, new departureBasedTimeComaprator());

        return schedules;
    }

    private void filterResults(List<Schedule> schedules, List<String> filters) {

        if (filters.size() == 0) {
            /*by default sort according to departure time*/
            searchResultsList = showOnlyCurrentTimeResults(searchResultsList);
            searchResultsList = sortTheSchedulesBasedOnDepartureTime(searchResultsList);  //applied before the filter sorting
            refreshAdapter(searchResultsList);
            return;
        }

        List<Schedule> filteredSchedules = schedules;
        filteredSchedules = applyTimeFilterIfAny(filteredSchedules, filters);
        filteredSchedules = applyACNonACFilterIfAny(filteredSchedules, filters);
        filteredSchedules = applySeaterSleeperIfAny(filteredSchedules, filters);
        filteredSchedules = applyBoardingPointFilterIfAny(filteredSchedules, filters);
        filteredSchedules = sortTheSchedulesBasedOnDepartureTime(filteredSchedules);  //applied before the filter sorting
        filteredSchedules = applySortingFilterIfAny(filteredSchedules, filters);
        refreshAdapter(filteredSchedules);

    }

    @Override
    public void showError(String error) {
        hideLoading();
        if (getActivity()==null || getActivity().isFinishing())
            return;
        if (error.equalsIgnoreCase("Un-authorized acess")) {
            Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
            SharedPrefUtils.getSharedPrefUtils().clearUserData(getActivity());
            startActivity(new Intent(getActivity(), SplashActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            getActivity().finish();
            return;
        }
        if (!error.equalsIgnoreCase("Sorry No Buses Available")) {
            Toast.makeText(getActivity(), "" + error, Toast.LENGTH_SHORT).show();
        }
        refreshAdapter(new ArrayList<Schedule>());
    }

    private void refreshAdapter(List<Schedule> buses) {


        if (buses.size() > 0) {
            noBusesView.setVisibility(View.GONE);
        } else {
            noBusesView.setVisibility(View.VISIBLE);
        }
        mAdapter.refreshAdapter(buses);
    }

    @Override
    public void applyFilters(List<String> filtersList) {
        if (searchResultsList.size() > 0 && mAdapter != null) {
            filterResults(searchResultsList, filtersList);
        }
    }

    public class LowToHIghFareSorter implements Comparator<Schedule> {

        @Override
        public int compare(Schedule LHSschedule, Schedule RHSSchedule) {
            Fare lFare = LHSschedule.getFares().get(0);
            Fare rFare = RHSSchedule.getFares().get(0);
            float rFareInt = Float.valueOf(String.valueOf(rFare.getFare()));
            float lFareInt = Float.valueOf(String.valueOf(lFare.getFare()));
            return (int) (lFareInt - rFareInt);
        }
    }

    public class departureBasedTimeComaprator implements Comparator<Schedule> {

        @Override
        public int compare(Schedule LHSschedule, Schedule RHSSchedule) {
            return (int) (LHSschedule.getDepartureTimeMillis() - RHSSchedule.getDepartureTimeMillis()) / 1000;
        }
    }

    public class HIghToLowFareSorter implements Comparator<Schedule> {

        @Override
        public int compare(Schedule LHSschedule, Schedule RHSSchedule) {
            Fare lFare = LHSschedule.getFares().get(0);
            Fare rFare = RHSSchedule.getFares().get(0);
            float rFareInt = Float.valueOf(String.valueOf(rFare.getFare()));
            float lFareInt = Float.valueOf(String.valueOf(lFare.getFare()));
            return (int) (rFareInt - lFareInt);
        }
    }



    public void openSeatLayout(String scheduleId, String busType) {
        startActivity(new Intent(getActivity(), BusMapActivity.class)
                .putExtra(Constants.BUS_TYPE, busType)
                .putExtra(Constants.FROM_STATION_ID, fromId)
                .putExtra(Constants.TO_STATION_ID, toId)
                .putExtra(Constants.TRAVEL_DATE, travelDate)
                .putExtra(Constants.SCHEDULE_ID, scheduleId)
                .putExtra(Constants.FROM_STATION_NAME, fromStationName)
                .putExtra(Constants.TO_STATION_NAME, toStationName)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private List<Schedule> showOnlyCurrentTimeResults(List<Schedule> searchResultsList){
        List<Schedule> temp = new ArrayList<>();
        for (Schedule schedule:searchResultsList){
            if (schedule.getDepartureTimeMillis()>System.currentTimeMillis()){
                temp.add(schedule);
            }
        }

        searchResultsList.clear();
        searchResultsList.addAll(temp);
        return searchResultsList;
    }


    @Override
    public void onResume() {
        Log.d("test_date",""+travelDate);
        super.onResume();
    }
}