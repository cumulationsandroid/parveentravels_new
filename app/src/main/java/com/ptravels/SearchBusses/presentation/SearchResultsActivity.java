package com.ptravels.SearchBusses.presentation;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptravels.BaseActivity;
import com.ptravels.SearchBusses.data.model.response.SearchFragmentData;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.global.Utils;
import com.ptravels.home.presentation.CheckableLinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchResultsActivity extends BaseActivity implements View.OnClickListener, UpdateStationPointsActivityListener {

    private List<String> datesList = new ArrayList<>();
    private SlidingTabsViewPagerAdapter mSlidingTabsViewPagerAdapter;

    private ViewPager mViewPager;
    private HashMap<String, String> dateToSendMap = new HashMap<>();
    private String fromId;
    private String toId;
    private String travelDate;
    private DrawerLayout drawerLayout;
    private CheckableLinearLayout acLayout, nonAcLayout, seaterLayout, sleeperLayout,
            sunriseLayout, noonLayout, sunsetLayout, nightLayout,
            slowLayout, fastLayout,
            lowLayout, highLayout;
    private TextView acTextView, nonAcTextView, seaterTextView, sleeperTextView,
            sunriseTextView, noonTextView, sunsetTextView, nightTextView,
            slowTextView, fastTextView,
            lowTextView, highText;
    private ImageView acIcon, nonAcicon, seaterIcon, sleeperIcon,
            sunriseIcon, noonIcon, sunsetIcon, nightIcon,
            slowIcon, fastIcon,
            lowIcon, highIcon;
    public List<String> filtersSelectedList;
    private HashMap<Integer, SearchFragmentData> mFragmentsRefMap = new HashMap<>();
    private String frmStationName, toStationName;

    private LinearLayout boardingPtsLayout, droppingPtsLayout;
    private AppCompatCheckBox brPtsCb;
    private String TAG = SearchResultsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerLayout = findViewById(R.id.drawer_layout);

        final TextView title = findViewById(R.id.title);
        if (getIntent() != null) {
            fromId = getIntent().getStringExtra(Constants.FROM_STATION_ID);
            toId = getIntent().getStringExtra(Constants.TO_STATION_ID);
            travelDate = getIntent().getStringExtra(Constants.TRAVEL_DATE);
            frmStationName = getIntent().getStringExtra(Constants.FROM_STATION_NAME);
            toStationName = getIntent().getStringExtra(Constants.TO_STATION_NAME);
            Utils utils = Utils.getUtils();
            datesList = utils.getNext5Days(travelDate, 5);
            List<String> next5DaysListForIntent = Utils.getUtils().getNext5DaysForIntent(travelDate, 5);
            populateHashMap(datesList, next5DaysListForIntent);
            mFragmentsRefMap=initializeFragmentsMap(5);
            title.setText(frmStationName + " \u2794 " + toStationName);
        }

        title.postDelayed(new Runnable() {
            @Override
            public void run() {
                title.setSelected(true);
            }
        }, 2000);

        mSlidingTabsViewPagerAdapter = new SlidingTabsViewPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSlidingTabsViewPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.white_with_opacity),
                ContextCompat.getColor(this, android.R.color.white));
        changeTabsFont(tabLayout);

        initDrawerViews();
        filtersSelectedList = new ArrayList<>();

        drawerLayout.addDrawerListener(new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                applyFilter();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        });

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.filterBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        findViewById(R.id.filtersCloseIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                }
            }
        });

        findViewById(R.id.applyFiltersBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyFilter();
            }
        });
    }

    private HashMap initializeFragmentsMap(int size) {

        for (int position = 0; position < size; position++)
            {
                travelDate = dateToSendMap.get(datesList.get(position));
                SearchResultsFragment fragment = SearchResultsFragment.initParamsInstance(fromId, toId, travelDate, frmStationName, toStationName);
                SearchFragmentData data=new SearchFragmentData(fromId, toId, travelDate, frmStationName, toStationName);
                mFragmentsRefMap.put(position, data);
            }
            return mFragmentsRefMap;
    }

    private void applyFilter() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }
        SlidingTabsViewPagerAdapter pagerAdapter = (SlidingTabsViewPagerAdapter) mViewPager.getAdapter();
      //  FragmentApplyFiltersListener listener = pagerAdapter.getFragment(mViewPager.getCurrentItem());

        SearchResultsFragment listener = (SearchResultsFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + mViewPager.getCurrentItem());
        if (listener != null) {
            listener.applyFilters(filtersSelectedList);
        }
    }

    private void initDrawerViews() {
        acLayout = findViewById(R.id.acLayout);
        nonAcLayout = findViewById(R.id.nonAcLayout);
        seaterLayout = findViewById(R.id.seaterLayout);
        sleeperLayout = findViewById(R.id.sleeperLayout);
        sunriseLayout = findViewById(R.id.sunriseLayout);
        noonLayout = findViewById(R.id.noonLayout);
        sunsetLayout = findViewById(R.id.sunsetLayout);
        nightLayout = findViewById(R.id.nightLayout);
        slowLayout = findViewById(R.id.slowLayout);
        fastLayout = findViewById(R.id.fastLayout);
        lowLayout = findViewById(R.id.lowLayout);
        highLayout = findViewById(R.id.highLayout);

        acTextView = findViewById(R.id.acText);
        nonAcTextView = findViewById(R.id.nonAcText);
        seaterTextView = findViewById(R.id.seaterText);
        sleeperTextView = findViewById(R.id.sleeperText);
        sunriseTextView = findViewById(R.id.sunriseText);
        noonTextView = findViewById(R.id.noonText);
        sunsetTextView = findViewById(R.id.sunsetText);
        nightTextView = findViewById(R.id.nightText);
        slowTextView = findViewById(R.id.slowText);
        fastTextView = findViewById(R.id.fastText);
        lowTextView = findViewById(R.id.lowToHighText);
        highText = findViewById(R.id.highToLowText);

        acIcon = findViewById(R.id.acImage);
        nonAcicon = findViewById(R.id.nonAcImage);
        seaterIcon = findViewById(R.id.seaterImage);
        sleeperIcon = findViewById(R.id.sleeperImage);
        sunriseIcon = findViewById(R.id.sunriseImage);
        noonIcon = findViewById(R.id.noonImage);
        sunsetIcon = findViewById(R.id.sunsetImage);
        nightIcon = findViewById(R.id.nightImage);
        slowIcon = findViewById(R.id.slowImage);
        fastIcon = findViewById(R.id.fastImage);
        lowIcon = findViewById(R.id.lowToHighImage);
        highIcon = findViewById(R.id.highToLowImage);
        brPtsCb = findViewById(R.id.boardingPtDropdown);
        boardingPtsLayout = findViewById(R.id.boardingPtsList);

        acLayout.setOnClickListener(this);
        nonAcLayout.setOnClickListener(this);
        seaterLayout.setOnClickListener(this);
        sleeperLayout.setOnClickListener(this);
        sunriseLayout.setOnClickListener(this);
        noonLayout.setOnClickListener(this);
        sunsetLayout.setOnClickListener(this);
        nightLayout.setOnClickListener(this);
        slowLayout.setOnClickListener(this);
        fastLayout.setOnClickListener(this);
        lowLayout.setOnClickListener(this);
        highLayout.setOnClickListener(this);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                SearchResultsFragment page = (SearchResultsFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + position);

                if (page != null)
                    page.applyFilters(filtersSelectedList);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        brPtsCb.setChecked(false);
        boardingPtsLayout.setVisibility(View.GONE);

        brPtsCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    boardingPtsLayout.setVisibility(View.VISIBLE);
                } else boardingPtsLayout.setVisibility(View.GONE);
            }
        });


    }

    @Override
    public void onClick(View view) {
        CheckableLinearLayout checkableLinearLayout = findViewById(view.getId());
        if (checkableLinearLayout.isChecked()) {
            checkableLinearLayout.setChecked(false);
        } else {
            checkableLinearLayout.setChecked(true);
            disableOtherFilters(view.getId());
        }

        toggleFilterViews(view.getId(), checkableLinearLayout.isChecked());
    }

    private void toggleFilterViews(int checkedLayoutId, boolean isChecked) {
        switch (checkedLayoutId) {
            case R.id.acLayout:
                if (isChecked) {
                    acTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    acIcon.setImageResource(R.drawable.ic_ac_selected);
                    if (!filtersSelectedList.contains(acTextView.getText().toString())) {
                        filtersSelectedList.add(acTextView.getText().toString());
                    }
                } else {
                    acTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    acIcon.setImageResource(R.drawable.ic_ac_not_selected);
                    if (filtersSelectedList.contains(acTextView.getText().toString())) {
                        filtersSelectedList.remove(acTextView.getText().toString());
                    }
                }
                break;

            case R.id.nonAcLayout:
                if (isChecked) {
                    nonAcTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    nonAcicon.setImageResource(R.drawable.ic_non_ac_selected);
                    if (!filtersSelectedList.contains(nonAcTextView.getText().toString())) {
                        filtersSelectedList.add(nonAcTextView.getText().toString());
                    }
                } else {
                    nonAcTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    nonAcicon.setImageResource(R.drawable.ic_non_ac_not_selected);
                    if (filtersSelectedList.contains(nonAcTextView.getText().toString())) {
                        filtersSelectedList.remove(nonAcTextView.getText().toString());
                    }
                }
                break;

            case R.id.seaterLayout:
                if (isChecked) {
                    seaterTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    seaterIcon.setImageResource(R.drawable.ic_seater_selected);
                    if (!filtersSelectedList.contains(seaterTextView.getText().toString())) {
                        filtersSelectedList.add(seaterTextView.getText().toString());
                    }
                } else {
                    seaterTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    seaterIcon.setImageResource(R.drawable.ic_seater_not_selected);
                    if (filtersSelectedList.contains(seaterTextView.getText().toString())) {
                        filtersSelectedList.remove(seaterTextView.getText().toString());
                    }
                }
                break;

            case R.id.sleeperLayout:
                if (isChecked) {
                    sleeperTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    sleeperIcon.setImageResource(R.drawable.ic_sleeper_selected);
                    if (!filtersSelectedList.contains(sleeperTextView.getText().toString())) {
                        filtersSelectedList.add(sleeperTextView.getText().toString());
                    }
                } else {
                    sleeperTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    sleeperIcon.setImageResource(R.drawable.ic_sleeper_not_selected);
                    if (filtersSelectedList.contains(sleeperTextView.getText().toString())) {
                        filtersSelectedList.remove(sleeperTextView.getText().toString());
                    }
                }
                break;

            case R.id.sunriseLayout:
                if (isChecked) {
                    sunriseTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    sunriseIcon.setImageResource(R.drawable.ic_sunrise_selected);
                    if (!filtersSelectedList.contains(sunriseTextView.getText().toString())) {
                        filtersSelectedList.add(sunriseTextView.getText().toString());
                    }
                } else {
                    sunriseTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    sunriseIcon.setImageResource(R.drawable.ic_sunrise_not_selected);
                    if (filtersSelectedList.contains(sunriseTextView.getText().toString())) {
                        filtersSelectedList.remove(sunriseTextView.getText().toString());
                    }
                }
                break;

            case R.id.noonLayout:
                if (isChecked) {
                    noonTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    noonIcon.setImageResource(R.drawable.ic_noon_selected);
                    if (!filtersSelectedList.contains(noonTextView.getText().toString())) {
                        filtersSelectedList.add(noonTextView.getText().toString());
                    }
                } else {
                    noonTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    noonIcon.setImageResource(R.drawable.ic_noon_not_selected);
                    if (filtersSelectedList.contains(noonTextView.getText().toString())) {
                        filtersSelectedList.remove(noonTextView.getText().toString());
                    }
                }
                break;

            case R.id.sunsetLayout:
                if (isChecked) {
                    sunsetTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    sunsetIcon.setImageResource(R.drawable.ic_sunset_selected);
                    if (!filtersSelectedList.contains(sunsetTextView.getText().toString())) {
                        filtersSelectedList.add(sunsetTextView.getText().toString());
                    }
                } else {
                    sunsetTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    sunsetIcon.setImageResource(R.drawable.ic_sunset_not_selected);
                    if (filtersSelectedList.contains(sunsetTextView.getText().toString())) {
                        filtersSelectedList.remove(sunsetTextView.getText().toString());
                    }
                }
                break;

            case R.id.nightLayout:
                if (isChecked) {
                    nightTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    nightIcon.setImageResource(R.drawable.ic_night_selected);
                    if (!filtersSelectedList.contains(nightTextView.getText().toString())) {
                        filtersSelectedList.add(nightTextView.getText().toString());
                    }
                } else {
                    nightTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    nightIcon.setImageResource(R.drawable.ic_night_not_selected);
                    if (filtersSelectedList.contains(nightTextView.getText().toString())) {
                        filtersSelectedList.remove(nightTextView.getText().toString());
                    }
                }
                break;

            case R.id.slowLayout:
                if (isChecked) {
                    slowTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    slowIcon.setImageResource(R.drawable.ic_slow_selected);
                    if (!filtersSelectedList.contains(slowTextView.getText().toString())) {
                        filtersSelectedList.add(slowTextView.getText().toString());
                    }
                } else {
                    slowTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    slowIcon.setImageResource(R.drawable.ic_slow_not_selected);
                    if (filtersSelectedList.contains(slowTextView.getText().toString())) {
                        filtersSelectedList.remove(slowTextView.getText().toString());
                    }
                }
                break;

            case R.id.fastLayout:
                if (isChecked) {
                    fastTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    fastIcon.setImageResource(R.drawable.ic_fast_selected);
                    if (!filtersSelectedList.contains(fastTextView.getText().toString())) {
                        filtersSelectedList.add(fastTextView.getText().toString());
                    }
                } else {
                    fastTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    fastIcon.setImageResource(R.drawable.ic_fast_not_selected);
                    if (filtersSelectedList.contains(fastTextView.getText().toString())) {
                        filtersSelectedList.remove(fastTextView.getText().toString());
                    }
                }
                break;

            case R.id.lowLayout:
                if (isChecked) {
                    lowTextView.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    lowIcon.setImageResource(R.drawable.ic_lowtohigh_selected);
                    if (!filtersSelectedList.contains(lowTextView.getText().toString())) {
                        filtersSelectedList.add(lowTextView.getText().toString());
                    }
                } else {
                    lowTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    lowIcon.setImageResource(R.drawable.ic_lowtohigh_not_selected);
                    if (filtersSelectedList.contains(lowTextView.getText().toString())) {
                        filtersSelectedList.remove(lowTextView.getText().toString());
                    }
                }
                break;

            case R.id.highLayout:
                if (isChecked) {
                    highText.setTextColor(ContextCompat.getColor(this, R.color.filters_blue_selected));
                    highIcon.setImageResource(R.drawable.ic_lowtohigh_selected);
                    highIcon.setRotation(180);
                    if (!filtersSelectedList.contains(highText.getText().toString())) {
                        filtersSelectedList.add(highText.getText().toString());
                    }
                } else {
                    highText.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                    highIcon.setImageResource(R.drawable.ic_lowtohigh_not_selected);
                    highIcon.setRotation(180);
                    if (filtersSelectedList.contains(highText.getText().toString())) {
                        filtersSelectedList.remove(highText.getText().toString());
                    }
                }
                break;
        }
    }

    @Override
    public void updateFilterStationPoints(final List<String> brPts, List<String> drPts) {

        boardingPtsLayout.removeAllViews();
        for (final String brPt : brPts) {
            View brPtView = getLayoutInflater().inflate(R.layout.br_pt_cb_layout, null);
            final AppCompatCheckBox brPtCb = brPtView.findViewById(R.id.boardingPointCb);

            brPtCb.setText(brPt);
            if (filtersSelectedList.contains(brPt)) {
                brPtCb.setChecked(true);
            }

            brPtCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        if (!filtersSelectedList.contains(brPt))
                            filtersSelectedList.add(brPt);
                    } else {
                        if (filtersSelectedList.contains(brPt))
                            filtersSelectedList.remove(brPt);
                    }
                }
            });

            boardingPtsLayout.addView(brPtView);
        }
    }



    public class SlidingTabsViewPagerAdapter extends FragmentPagerAdapter {

        public SlidingTabsViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
         public Fragment getItem(int position) {

            SearchFragmentData data= mFragmentsRefMap.get(position);
            return SearchResultsFragment.initParamsInstance(data.getFromStationId(),data.getToStationId(),data.getTravelDate(),data.getFromStationName(),data.getToStattionName());


        }

        @Override
        public int getCount() {
            return mFragmentsRefMap.size();
        }

        @Override
        public int getItemPosition(Object object) {
            /*Doesn't get called*/
            SearchResultsFragment fragment = (SearchResultsFragment) object;
            if (fragment != null)
                fragment.applyFilters(filtersSelectedList);
            return super.getItemPosition(object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
         //   mFragmentsRefMap.remove(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return datesList.get(position);
        }


    }

    private void populateHashMap(List<String> next5DaysList, List<String> next5DaysListForIntent) {
        for (int i = 0; i < next5DaysList.size(); i++) {
            dateToSendMap.put(next5DaysList.get(i), next5DaysListForIntent.get(i));
        }
    }

    private void changeTabsFont(TabLayout tabLayout) {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        Typeface montserratLightTypeface = ResourcesCompat.getFont(this, R.font.montserrat_light_family);

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(montserratLightTypeface);
                    ((TextView) tabViewChild).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                }
            }
        }
    }

    private void disableOtherFilters(int id) {
        switch (id) {
            case R.id.acLayout:
                nonAcTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                nonAcicon.setImageResource(R.drawable.ic_non_ac_not_selected);
                if (filtersSelectedList.contains(nonAcTextView.getText().toString())) {
                    filtersSelectedList.remove(nonAcTextView.getText().toString());
                }
                nonAcLayout.setChecked(false);
                break;

            case R.id.nonAcLayout:
                acTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                acIcon.setImageResource(R.drawable.ic_ac_not_selected);
                if (filtersSelectedList.contains(acTextView.getText().toString())) {
                    filtersSelectedList.remove(acTextView.getText().toString());
                }
                acLayout.setChecked(false);
                break;

            case R.id.seaterLayout:
                sleeperTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                sleeperIcon.setImageResource(R.drawable.ic_sleeper_not_selected);
                if (filtersSelectedList.contains(sleeperTextView.getText().toString())) {
                    filtersSelectedList.remove(sleeperTextView.getText().toString());
                }
                sleeperLayout.setChecked(false);
                break;

            case R.id.sleeperLayout:
                seaterTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                seaterIcon.setImageResource(R.drawable.ic_seater_not_selected);
                if (filtersSelectedList.contains(seaterTextView.getText().toString())) {
                    filtersSelectedList.remove(seaterTextView.getText().toString());
                }
                seaterLayout.setChecked(false);
                break;

            case R.id.lowLayout:
                highText.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                highIcon.setImageResource(R.drawable.ic_lowtohigh_not_selected);
                highIcon.setRotation(180);
                if (filtersSelectedList.contains(highText.getText().toString())) {
                    filtersSelectedList.remove(highText.getText().toString());
                }
                highLayout.setChecked(false);
                break;

            case R.id.highLayout:
                lowTextView.setTextColor(ContextCompat.getColor(this, R.color.app_text_color));
                lowIcon.setImageResource(R.drawable.ic_lowtohigh_not_selected);
                if (filtersSelectedList.contains(lowTextView.getText().toString())) {
                    filtersSelectedList.remove(lowTextView.getText().toString());
                }
                lowLayout.setChecked(false);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
            return;
        }
        super.onBackPressed();
        Log.e(TAG, "onBackpressed");
    }
}
