package com.ptravels.SearchBusses.domain;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.SearchBusses.data.model.response.BusSearchResponse;
import com.ptravels.SearchBusses.data.source.BusSearchDataImpl;
import com.ptravels.global.UseCase;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BusSearchUseCase extends UseCase<BusSearchRequest,BusSearchResponse> {

    private BusSearchDataInterface busSearchData = new BusSearchDataImpl();

    @Override
    public Single<BusSearchResponse> buildUseCase(BusSearchRequest busSearchRequest) {
        return busSearchData.getBusSearchResults(busSearchRequest);
    }
}
