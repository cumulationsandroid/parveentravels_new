package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class UserCommisions{
	private String fromDate;
	private int commissionId;
	private String toDate;
	private boolean byDefault;
	private int id;
	private int userId;

	public void setFromDate(String fromDate){
		this.fromDate = fromDate;
	}

	public String getFromDate(){
		return fromDate;
	}

	public void setCommissionId(int commissionId){
		this.commissionId = commissionId;
	}

	public int getCommissionId(){
		return commissionId;
	}

	public void setToDate(String toDate){
		this.toDate = toDate;
	}

	public String getToDate(){
		return toDate;
	}

	public void setByDefault(boolean byDefault){
		this.byDefault = byDefault;
	}

	public boolean isByDefault(){
		return byDefault;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}
}
