package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class PaymentRequestData{
	private int amount;
	private String merchantCode;
	private String partnerCode;
	private int gatewayModeDetailId;
	private int gatewayModeId;
	private String appCode;
	private UserDetails userDetails;
	private String transactionId;
	private String bigDecimalString;
	private String gatewayModeCode;
	private boolean agentRecharge;
	private String returnURL;
	private int operatorId;
	private String gatewayModeDetailCode;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setMerchantCode(String merchantCode){
		this.merchantCode = merchantCode;
	}

	public String getMerchantCode(){
		return merchantCode;
	}

	public void setPartnerCode(String partnerCode){
		this.partnerCode = partnerCode;
	}

	public String getPartnerCode(){
		return partnerCode;
	}

	public void setGatewayModeDetailId(int gatewayModeDetailId){
		this.gatewayModeDetailId = gatewayModeDetailId;
	}

	public int getGatewayModeDetailId(){
		return gatewayModeDetailId;
	}

	public void setGatewayModeId(int gatewayModeId){
		this.gatewayModeId = gatewayModeId;
	}

	public int getGatewayModeId(){
		return gatewayModeId;
	}

	public void setAppCode(String appCode){
		this.appCode = appCode;
	}

	public String getAppCode(){
		return appCode;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setBigDecimalString(String bigDecimalString){
		this.bigDecimalString = bigDecimalString;
	}

	public String getBigDecimalString(){
		return bigDecimalString;
	}

	public void setGatewayModeCode(String gatewayModeCode){
		this.gatewayModeCode = gatewayModeCode;
	}

	public String getGatewayModeCode(){
		return gatewayModeCode;
	}

	public void setAgentRecharge(boolean agentRecharge){
		this.agentRecharge = agentRecharge;
	}

	public boolean isAgentRecharge(){
		return agentRecharge;
	}

	public void setReturnURL(String returnURL){
		this.returnURL = returnURL;
	}

	public String getReturnURL(){
		return returnURL;
	}

	public void setOperatorId(int operatorId){
		this.operatorId = operatorId;
	}

	public int getOperatorId(){
		return operatorId;
	}

	public void setGatewayModeDetailCode(String gatewayModeDetailCode){
		this.gatewayModeDetailCode = gatewayModeDetailCode;
	}

	public String getGatewayModeDetailCode(){
		return gatewayModeDetailCode;
	}
}
