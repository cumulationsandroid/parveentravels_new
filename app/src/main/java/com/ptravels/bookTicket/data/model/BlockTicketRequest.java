package com.ptravels.bookTicket.data.model;

import com.ptravels.seatlayout.data.model.Passenger;

import java.util.List;

/**
 * Created by Amit Tumkur on 03-01-2018.
 */

public class BlockTicketRequest {
    private String emailId;
    private String scheduleId;
    private List<Passenger> passengerDetails;
    private String toStation;
    private String boardingPointId;
    private String travelDate;
    private String mobileNumber;
    private String fromStation;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public List<Passenger> getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(List<Passenger> passengerDetails) {
        this.passengerDetails = passengerDetails;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getBoardingPointId() {
        return boardingPointId;
    }

    public void setBoardingPointId(String boardingPointId) {
        this.boardingPointId = boardingPointId;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }
}
