package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class Data{
	private int amount;
	private String gatewayUrl;
	private String gatewayCode;
	private String method;
	private GatewayProviderCredentialsDTO gatewayProviderCredentialsDTO;
	private String returnUrl;
	private UserDetails userDetails;
	private String transactionId;
	private GatewayFormParam gatewayFormParam;
	private String gatewayModeDetailCode;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setGatewayUrl(String gatewayUrl){
		this.gatewayUrl = gatewayUrl;
	}

	public String getGatewayUrl(){
		return gatewayUrl;
	}

	public void setGatewayCode(String gatewayCode){
		this.gatewayCode = gatewayCode;
	}

	public String getGatewayCode(){
		return gatewayCode;
	}

	public void setMethod(String method){
		this.method = method;
	}

	public String getMethod(){
		return method;
	}

	public void setGatewayProviderCredentialsDTO(GatewayProviderCredentialsDTO gatewayProviderCredentialsDTO){
		this.gatewayProviderCredentialsDTO = gatewayProviderCredentialsDTO;
	}

	public GatewayProviderCredentialsDTO getGatewayProviderCredentialsDTO(){
		return gatewayProviderCredentialsDTO;
	}

	public void setReturnUrl(String returnUrl){
		this.returnUrl = returnUrl;
	}

	public String getReturnUrl(){
		return returnUrl;
	}

	public void setUserDetails(UserDetails userDetails){
		this.userDetails = userDetails;
	}

	public UserDetails getUserDetails(){
		return userDetails;
	}

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setGatewayFormParam(GatewayFormParam gatewayFormParam){
		this.gatewayFormParam = gatewayFormParam;
	}

	public GatewayFormParam getGatewayFormParam(){
		return gatewayFormParam;
	}

	public void setGatewayModeDetailCode(String gatewayModeDetailCode){
		this.gatewayModeDetailCode = gatewayModeDetailCode;
	}

	public String getGatewayModeDetailCode(){
		return gatewayModeDetailCode;
	}
}
