package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class Customer{
	private int id;
	private String registerType;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setRegisterType(String registerType){
		this.registerType = registerType;
	}

	public String getRegisterType(){
		return registerType;
	}
}
