package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class PreTransactionResponse{
	private Data data;
	private boolean success;
	private PaymentRequestData paymentRequestData;
	private boolean error;

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setPaymentRequestData(PaymentRequestData paymentRequestData){
		this.paymentRequestData = paymentRequestData;
	}

	public PaymentRequestData getPaymentRequestData(){
		return paymentRequestData;
	}

	public void setError(boolean error){
		this.error = error;
	}

	public boolean isError(){
		return error;
	}
}
