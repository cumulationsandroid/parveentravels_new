package com.ptravels.bookTicket.data.model;

public class BookTicketRequest{
	private String pnr;

	public void setPnr(String pnr){
		this.pnr = pnr;
	}

	public String getPnr(){
		return pnr;
	}

	@Override
 	public String toString(){
		return 
			"BookTicketRequest{" + 
			"pnr = '" + pnr + '\'' + 
			"}";
		}
}
