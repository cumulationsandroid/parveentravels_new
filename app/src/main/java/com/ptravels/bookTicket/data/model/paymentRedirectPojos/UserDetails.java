package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

import java.util.List;

public class UserDetails{
	private String lastName;
	private Address address;
	private Role role;
	private List<UserCommisions> userCommissions;
	private String gender;
	private int roleId;
	private boolean credentialsNonExpired;
	private String mobile;
	private boolean active;
	private String type;
	private boolean socialLogin;
	private boolean enabled;
	private String firstName;
	private List<Object> userPrivileges;
	private String password;
	private String roleName;
	private boolean accountNonExpired;
	private int id;
	private String email;
	private boolean accountNonLocked;
	private String username;

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setAddress(Address address){
		this.address = address;
	}

	public Address getAddress(){
		return address;
	}

	public void setRole(Role role){
		this.role = role;
	}

	public Role getRole(){
		return role;
	}

	public void setUserCommissions(List<UserCommisions> userCommissions){
		this.userCommissions = userCommissions;
	}

	public List<UserCommisions> getUserCommissions(){
		return userCommissions;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setRoleId(int roleId){
		this.roleId = roleId;
	}

	public int getRoleId(){
		return roleId;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired){
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public boolean isCredentialsNonExpired(){
		return credentialsNonExpired;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setActive(boolean active){
		this.active = active;
	}

	public boolean isActive(){
		return active;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setSocialLogin(boolean socialLogin){
		this.socialLogin = socialLogin;
	}

	public boolean isSocialLogin(){
		return socialLogin;
	}

	public void setEnabled(boolean enabled){
		this.enabled = enabled;
	}

	public boolean isEnabled(){
		return enabled;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setUserPrivileges(List<Object> userPrivileges){
		this.userPrivileges = userPrivileges;
	}

	public List<Object> getUserPrivileges(){
		return userPrivileges;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setRoleName(String roleName){
		this.roleName = roleName;
	}

	public String getRoleName(){
		return roleName;
	}

	public void setAccountNonExpired(boolean accountNonExpired){
		this.accountNonExpired = accountNonExpired;
	}

	public boolean isAccountNonExpired(){
		return accountNonExpired;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setAccountNonLocked(boolean accountNonLocked){
		this.accountNonLocked = accountNonLocked;
	}

	public boolean isAccountNonLocked(){
		return accountNonLocked;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}
}