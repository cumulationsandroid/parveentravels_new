package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class Role{
	private String code;
	private int level;
	private String authority;
	private String name;
	private String description;
	private int id;
	private boolean employeeRole;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setLevel(int level){
		this.level = level;
	}

	public int getLevel(){
		return level;
	}

	public void setAuthority(String authority){
		this.authority = authority;
	}

	public String getAuthority(){
		return authority;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmployeeRole(boolean employeeRole){
		this.employeeRole = employeeRole;
	}

	public boolean isEmployeeRole(){
		return employeeRole;
	}
}
