package com.ptravels.bookTicket.data.model;

import com.ptravels.global.ResponseError;

import java.util.List;

/**
 * Created by Amit Tumkur on 05-01-2018.
 */

public class GatewayResponse {
    private List<GatewayData> data;
    private String success;
    private ResponseError error;

    public List<GatewayData> getData() {
        return data;
    }

    public void setData(List<GatewayData> data) {
        this.data = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public ResponseError getError() {
        return error;
    }

    public void setError(ResponseError error) {
        this.error = error;
    }
}
