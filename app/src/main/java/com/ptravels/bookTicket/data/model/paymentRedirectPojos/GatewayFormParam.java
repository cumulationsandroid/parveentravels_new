package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class GatewayFormParam{
	private String INDUSTRY_TYPE_ID;
	private String ORDER_ID;
	private String MOBILE_NO;
	private String CUST_ID;
	private String CHANNEL_ID;
	private String EMAIL;
	private String REQUEST_TYPE;
	private String MID;
	private String CALLBACK_URL;
	private String WEBSITE;
	private String TXN_AMOUNT;
	private String CHECKSUMHASH;

	public String getINDUSTRY_TYPE_ID() {
		return INDUSTRY_TYPE_ID;
	}

	public void setINDUSTRY_TYPE_ID(String INDUSTRY_TYPE_ID) {
		this.INDUSTRY_TYPE_ID = INDUSTRY_TYPE_ID;
	}

	public String getORDER_ID() {
		return ORDER_ID;
	}

	public void setORDER_ID(String ORDER_ID) {
		this.ORDER_ID = ORDER_ID;
	}

	public String getMOBILE_NO() {
		return MOBILE_NO;
	}

	public void setMOBILE_NO(String MOBILE_NO) {
		this.MOBILE_NO = MOBILE_NO;
	}

	public String getCUST_ID() {
		return CUST_ID;
	}

	public void setCUST_ID(String CUST_ID) {
		this.CUST_ID = CUST_ID;
	}

	public String getCHANNEL_ID() {
		return CHANNEL_ID;
	}

	public void setCHANNEL_ID(String CHANNEL_ID) {
		this.CHANNEL_ID = CHANNEL_ID;
	}

	public String getEMAIL() {
		return EMAIL;
	}

	public void setEMAIL(String EMAIL) {
		this.EMAIL = EMAIL;
	}

	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}

	public void setREQUEST_TYPE(String REQUEST_TYPE) {
		this.REQUEST_TYPE = REQUEST_TYPE;
	}

	public String getMID() {
		return MID;
	}

	public void setMID(String MID) {
		this.MID = MID;
	}

	public String getCALLBACK_URL() {
		return CALLBACK_URL;
	}

	public void setCALLBACK_URL(String CALLBACK_URL) {
		this.CALLBACK_URL = CALLBACK_URL;
	}

	public String getWEBSITE() {
		return WEBSITE;
	}

	public void setWEBSITE(String WEBSITE) {
		this.WEBSITE = WEBSITE;
	}

	public String getTXN_AMOUNT() {
		return TXN_AMOUNT;
	}

	public void setTXN_AMOUNT(String TXN_AMOUNT) {
		this.TXN_AMOUNT = TXN_AMOUNT;
	}

	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}

	public void setCHECKSUMHASH(String CHECKSUMHASH) {
		this.CHECKSUMHASH = CHECKSUMHASH;
	}

	@Override
	public String toString() {
		return "GatewayFormParam{" +
				"INDUSTRY_TYPE_ID='" + INDUSTRY_TYPE_ID + '\'' +
				", ORDER_ID='" + ORDER_ID + '\'' +
				", MOBILE_NO='" + MOBILE_NO + '\'' +
				", CUST_ID='" + CUST_ID + '\'' +
				", CHANNEL_ID='" + CHANNEL_ID + '\'' +
				", EMAIL='" + EMAIL + '\'' +
				", REQUEST_TYPE='" + REQUEST_TYPE + '\'' +
				", MID='" + MID + '\'' +
				", CALLBACK_URL='" + CALLBACK_URL + '\'' +
				", WEBSITE='" + WEBSITE + '\'' +
				", TXN_AMOUNT='" + TXN_AMOUNT + '\'' +
				", CHECKSUMHASH='" + CHECKSUMHASH + '\'' +
				'}';
	}
}
