package com.ptravels.bookTicket.data.model;

import com.ptravels.global.ResponseError;

public class BookTicketResponse{
	private Data data;
	private boolean success;
	private ResponseError error;

	public ResponseError getError() {
		return error;
	}

	public void setError(ResponseError error) {
		this.error = error;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}
}
