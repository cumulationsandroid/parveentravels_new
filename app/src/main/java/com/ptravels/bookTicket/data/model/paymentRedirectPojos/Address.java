package com.ptravels.bookTicket.data.model.paymentRedirectPojos;

public class Address{
	private String phoneNumber;
	private String pinCode;
	private int id;
	private String line2;
	private String landMark;
	private String line1;
	private int stationId;

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setPinCode(String pinCode){
		this.pinCode = pinCode;
	}

	public String getPinCode(){
		return pinCode;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLine2(String line2){
		this.line2 = line2;
	}

	public String getLine2(){
		return line2;
	}

	public void setLandMark(String landMark){
		this.landMark = landMark;
	}

	public String getLandMark(){
		return landMark;
	}

	public void setLine1(String line1){
		this.line1 = line1;
	}

	public String getLine1(){
		return line1;
	}

	public void setStationId(int stationId){
		this.stationId = stationId;
	}

	public int getStationId(){
		return stationId;
	}
}
