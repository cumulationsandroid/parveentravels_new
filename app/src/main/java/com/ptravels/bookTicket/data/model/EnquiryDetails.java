package com.ptravels.bookTicket.data.model;

public class EnquiryDetails{
	private String transactionType;
	private String merchantReturnUrl;
	private String bankCode;
	private double amount;
	private String gatewayTransactionId;
	private boolean isSessionInvalid;
	private String responseRecevied;
	private GatewayCredentials gatewayCredentials;
	private GatewayResponse gatewayResponse;
	private String vendorTransactionId;
	private String transactionId;
	private String status;

	public void setTransactionType(String transactionType){
		this.transactionType = transactionType;
	}

	public String getTransactionType(){
		return transactionType;
	}

	public void setMerchantReturnUrl(String merchantReturnUrl){
		this.merchantReturnUrl = merchantReturnUrl;
	}

	public String getMerchantReturnUrl(){
		return merchantReturnUrl;
	}

	public void setBankCode(String bankCode){
		this.bankCode = bankCode;
	}

	public String getBankCode(){
		return bankCode;
	}

	public void setAmount(double amount){
		this.amount = amount;
	}

	public double getAmount(){
		return amount;
	}

	public void setGatewayTransactionId(String gatewayTransactionId){
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public String getGatewayTransactionId(){
		return gatewayTransactionId;
	}

	public void setIsSessionInvalid(boolean isSessionInvalid){
		this.isSessionInvalid = isSessionInvalid;
	}

	public boolean isIsSessionInvalid(){
		return isSessionInvalid;
	}

	public void setResponseRecevied(String responseRecevied){
		this.responseRecevied = responseRecevied;
	}

	public String getResponseRecevied(){
		return responseRecevied;
	}

	public void setGatewayCredentials(GatewayCredentials gatewayCredentials){
		this.gatewayCredentials = gatewayCredentials;
	}

	public GatewayCredentials getGatewayCredentials(){
		return gatewayCredentials;
	}

	public void setGatewayResponse(GatewayResponse gatewayResponse){
		this.gatewayResponse = gatewayResponse;
	}

	public GatewayResponse getGatewayResponse(){
		return gatewayResponse;
	}

	public void setVendorTransactionId(String vendorTransactionId){
		this.vendorTransactionId = vendorTransactionId;
	}

	public String getVendorTransactionId(){
		return vendorTransactionId;
	}

	public void setTransactionId(String transactionId){
		this.transactionId = transactionId;
	}

	public String getTransactionId(){
		return transactionId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}
