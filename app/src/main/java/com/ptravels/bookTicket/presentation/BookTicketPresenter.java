package com.ptravels.bookTicket.presentation;

import com.ptravels.SearchBusses.data.model.request.BusSearchRequest;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.domain.BookTicketUseCase;
import com.ptravels.bookTicket.domain.GatewayUseCase;
import com.ptravels.bookTicket.domain.PreTransactionUseCase;
import com.ptravels.bookTicket.domain.ReceiveUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.nio.file.Path;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BookTicketPresenter {
    void getBlockTicketResult(BlockTicketRequest request);
    void getGatewayData(GatewayUseCase gatewayUseCase,PathParamRequest request);
    void getPreTransactionData(PreTransactionUseCase preTransactionUseCase, PreTransactionRequest request);
    void getBookTicketResult(BookTicketUseCase useCase,BookTicketRequest request);
    void receivePaytmInfo(ReceiveUseCase useCase, ReceiveRequest request);
}
