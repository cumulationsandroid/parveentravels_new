package com.ptravels.bookTicket.presentation;

import com.ptravels.R;
import com.ptravels.global.Constants;
import com.ptravels.REST.RestClient;
import com.ptravels.global.Utils;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.domain.BlockTicketUseCase;
import com.ptravels.bookTicket.domain.BookTicketUseCase;
import com.ptravels.bookTicket.domain.GatewayUseCase;
import com.ptravels.bookTicket.domain.PreTransactionUseCase;
import com.ptravels.bookTicket.domain.ReceiveUseCase;
import com.ptravels.stationsList.data.model.response.PathParamRequest;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import retrofit2.adapter.rxjava.HttpException;
import rx.SingleSubscriber;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public class BookTicketPresenterImpl implements BookTicketPresenter {

    private BlockTicketUseCase blockTicketUseCase;
    private BookTicketView bookTicketView;

    public BookTicketPresenterImpl(BookTicketView bookTicketView) {
        this.bookTicketView = bookTicketView;
    }

    public BookTicketPresenterImpl(BookTicketView bookTicketView, BlockTicketUseCase blockTicketUseCase) {
        this.bookTicketView = bookTicketView;
        this.blockTicketUseCase = blockTicketUseCase;
    }

    @Override
    public void getBlockTicketResult(BlockTicketRequest request) {
        if (Utils.isOnline()) {
            bookTicketView.showLoading();
            blockTicketUseCase.execute(request)
                    .subscribe(new SingleSubscriber<BlockTicketResponse>() {
                        @Override
                        public void onSuccess(BlockTicketResponse value) {
                            bookTicketView.hideLoading();
                            bookTicketView.showBlockTicketResult(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            bookTicketView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    bookTicketView.showError(RestClient.getErrorMessage(responseBody));
                                    return;
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    bookTicketView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    bookTicketView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                bookTicketView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            bookTicketView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getGatewayData(GatewayUseCase gatewayUseCase, PathParamRequest request) {
        if (Utils.isOnline()) {
            bookTicketView.showLoading();
            gatewayUseCase.execute(request)
                    .subscribe(new SingleSubscriber<GatewayResponse>() {
                        @Override
                        public void onSuccess(GatewayResponse value) {
                            bookTicketView.hideLoading();
                            bookTicketView.provideGatewayData(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            bookTicketView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    bookTicketView.showError(RestClient.getErrorMessage(responseBody));
                                    return;
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    bookTicketView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    bookTicketView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                bookTicketView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            bookTicketView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getPreTransactionData(PreTransactionUseCase preTransactionUseCase, PreTransactionRequest request) {
        if (Utils.isOnline()) {
            bookTicketView.showLoading();
            preTransactionUseCase.execute(request)
                    .subscribe(new SingleSubscriber<PreTransactionResponse>() {
                        @Override
                        public void onSuccess(PreTransactionResponse value) {
                            bookTicketView.hideLoading();
                            bookTicketView.providePreTransactionData(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            bookTicketView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    bookTicketView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    bookTicketView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    bookTicketView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                bookTicketView.showError(Constants.UNKNOWN_ERROR);
                            }
                        }
                    });
        } else {
            bookTicketView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void getBookTicketResult(final BookTicketUseCase bookTicketUseCase, BookTicketRequest request) {
        if (Utils.isOnline()) {
            bookTicketView.showLoading();
            bookTicketUseCase.execute(request)
                    .subscribe(new SingleSubscriber<BookTicketResponse>() {
                        @Override
                        public void onSuccess(BookTicketResponse value) {
                            bookTicketView.hideLoading();
                            bookTicketView.showBookTicketResult(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            bookTicketView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    bookTicketView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    bookTicketView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    bookTicketView.showError(error.getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                bookTicketView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            bookTicketView.showError(Constants.NO_INTERNET);
        }
    }

    @Override
    public void receivePaytmInfo(ReceiveUseCase useCase, ReceiveRequest request) {
        if (Utils.isOnline()) {
            bookTicketView.showLoading();
            useCase.execute(request)
                    .subscribe(new SingleSubscriber<ReceiveResponse>() {
                        @Override
                        public void onSuccess(ReceiveResponse value) {
                            bookTicketView.hideLoading();
                            bookTicketView.receivePaytmInfo(value);
                        }

                        @Override
                        public void onError(Throwable error) {
                            bookTicketView.hideLoading();
                            try {
                                error.printStackTrace();
                                if (error instanceof HttpException) {
                                    ResponseBody responseBody = ((HttpException) error).response().errorBody();
                                    bookTicketView.showError(RestClient.getErrorMessage(responseBody));
                                } else if (error instanceof ConnectException || error instanceof SocketTimeoutException
                                        || error instanceof UnknownHostException) {
                                    bookTicketView.showError(Constants.NETWORK_ERROR);
                                } else {
                                    bookTicketView.showError(error.getMessage());
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                                bookTicketView.showError(Constants.UNKNOWN_ERROR);
                            }

                        }
                    });
        } else {
            bookTicketView.showError(Constants.NO_INTERNET);
        }
    }
}
