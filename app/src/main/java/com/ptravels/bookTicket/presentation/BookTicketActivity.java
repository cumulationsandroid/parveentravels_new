package com.ptravels.bookTicket.presentation;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ptravels.BaseActivity;
import com.ptravels.global.Constants;
import com.ptravels.R;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.seatlayout.data.model.BookedSeatModel;
import com.ptravels.seatlayout.data.model.Passenger;
import com.ptravels.seatlayout.data.model.response.LayoutDetail;
import com.ptravels.seatlayout.presentation.BusMapSeatAdapter;
import com.ptravels.seatlayout.presentation.CancellationPolicyFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BookTicketActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout passengerDetails;
    private TextInputLayout emailInputLayout, mobileNoInputLayout;
    private TextInputEditText emailIdEt, phoneNumberEt;
    private CheckBox termsAndConditionsCb;
    private TextView fromToLabel, travelDateLabel;
    private BookedSeatModel bookedSeatModel;
    private AlertDialog webviewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_ticket);
        initViews();
    }

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        fromToLabel = findViewById(R.id.fromToLabel);
        travelDateLabel = findViewById(R.id.travelDateLabel);
        passengerDetails = findViewById(R.id.passengerDetails);
        emailInputLayout = findViewById(R.id.email_input_layout);
        mobileNoInputLayout = findViewById(R.id.phone_input_layout);
        emailIdEt = findViewById(R.id.emailInputEt);
        phoneNumberEt = findViewById(R.id.mobileNumberInputEt);
        termsAndConditionsCb = findViewById(R.id.termConditionsCb);


        setClickListeners();

        if (getIntent() != null && getIntent().getSerializableExtra(Constants.BOOKED_SEAT_DATA) != null) {
            bookedSeatModel = (BookedSeatModel) getIntent().getSerializableExtra(Constants.BOOKED_SEAT_DATA);
        }
        initDetails();
    }

    private void initDetails() {

        if (bookedSeatModel == null)
            return;
        fromToLabel.setText(bookedSeatModel.getFromStationName() + " - " + bookedSeatModel.getToStationName());
        travelDateLabel.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, EEE", bookedSeatModel.getTravelDate()));

        if (!SharedPrefUtils.getSharedPrefUtils().isGuestUserLogin(this)) {
            String email = SharedPrefUtils.getSharedPrefUtils().getUserEmailID(this);
            if (!email.isEmpty()) {
                emailIdEt.setText(email);
            /*to place cursor at end of text*/
                emailIdEt.post(new Runnable() {
                    @Override
                    public void run() {
                        emailIdEt.setSelection(emailIdEt.getText().length());
                    }
                });
            }

            String mobile = SharedPrefUtils.getSharedPrefUtils().getMobileNumber(this);
            if (!mobile.isEmpty()) {
                phoneNumberEt.setText(mobile);
            /*to place cursor at end of text*/
                phoneNumberEt.post(new Runnable() {
                    @Override
                    public void run() {
                        phoneNumberEt.setSelection(phoneNumberEt.getText().length());
                    }
                });
            }
        }

        ArrayList<LayoutDetail> selectedSeats = bookedSeatModel.getSelectedSeatList();
        int size = selectedSeats.size();

        ((TextView) findViewById(R.id.passCount)).setText("Passenger details for " + size + " seats selected");
        for (int s = 0; s < size; s++) {
            View view = getLayoutInflater().inflate(R.layout.layout_passenger_row, null);
            TextView seatNo = view.findViewById(R.id.seatNoTv);
            TextView passengerInfo = view.findViewById(R.id.passengerInfo);
            TextInputLayout nameIpL = view.findViewById(R.id.name_input_layout);
            TextInputLayout ageIpL = view.findViewById(R.id.age_input_layout);
            TextInputEditText name = view.findViewById(R.id.nameInputEt);
            TextInputEditText age = view.findViewById(R.id.ageInputEt);
            RadioButton maleRb = view.findViewById(R.id.maleRb);
            RadioButton femaleRb = view.findViewById(R.id.femaleRb);
            TextView seatOnlyInfo = view.findViewById(R.id.seatOnlyInfoTv);

            name.setTag(1);
            age.setTag(1);
            maleRb.setTag(1);
            femaleRb.setTag(1);

//            name.setFilters(new InputFilter[]{filtertxt});
//            age.setFilters(new InputFilter[]{filtertxt2});

            int j = s + 1;
            if (s == 0) {
                passengerInfo.setText("Primary Passenger");
            } else
                passengerInfo.setText("Co-Passenger");
            //passenger_text.setText(getActivity().getResources().getString(R.string.traveller_on_seat_a1) + " " + busMapFragment.getSelectedCommanFloorSeatArrayList().get(i).getSeatNbr() + "");
            name.setTag(R.id.nameInputEt, selectedSeats.get(s).getServiceTax() + "");
            seatNo.setText(selectedSeats.get(s).getSeatNumber());
            if (selectedSeats.get(s).getAvailableFor().equalsIgnoreCase(BusMapSeatAdapter.FEMALE_SEAT)) {
                femaleRb.setChecked(true);
                maleRb.setEnabled(false);
                seatOnlyInfo.setVisibility(View.VISIBLE);
                seatOnlyInfo.setText("Seat available only for ladies");
            } else if (selectedSeats.get(s).getAvailableFor().equalsIgnoreCase("MALE")) {
                maleRb.setChecked(true);
                femaleRb.setEnabled(false);
                seatOnlyInfo.setVisibility(View.VISIBLE);
                seatOnlyInfo.setText("Seat available only for gents");
            } else {
                seatOnlyInfo.setVisibility(View.GONE);
            }

            passengerDetails.addView(view);
        }

        SpannableString contentText = clickableStrings(getResources().getString((R.string.payment_agree)));
        TextView textView = findViewById(R.id.agreeTermsText);
        textView.setText(contentText);
        textView.setAutoLinkMask(RESULT_OK);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.TRANSPARENT);
        Linkify.addLinks(contentText, Linkify.WEB_URLS);

    }

    private void setClickListeners() {
        findViewById(R.id.backBtn).setOnClickListener(this);
        findViewById(R.id.infoBtn).setOnClickListener(this);
//        findViewById(R.id.paymentProceedBtn).setOnClickListener(this);

        rxSingleClickListener(findViewById(R.id.paymentProceedBtn));
    }

    @Override
    public void onClick(View view) {
        Utils.getUtils().closeKeyboard(this, getCurrentFocus());
        switch (view.getId()) {
            case R.id.paymentProceedBtn:
                if (validateFields()) {
                    validateAndSavePassengerDetails();
                }
                break;

            case R.id.backBtn:
                onBackPressed();
                break;

            case R.id.infoBtn:
                CancellationPolicyFragment bottomSheetDialogFragment = new CancellationPolicyFragment();
                bottomSheetDialogFragment.show(getSupportFragmentManager(), CancellationPolicyFragment.class.getSimpleName());
                break;
        }

    }

    private boolean validateFields() {
        emailIdEt.clearFocus();
        phoneNumberEt.clearFocus();
        emailInputLayout.setErrorEnabled(false);
        mobileNoInputLayout.setErrorEnabled(false);
        emailInputLayout.setError(null);
        mobileNoInputLayout.setError(null);
        if (emailIdEt.getText().toString().isEmpty()) {
            emailInputLayout.setErrorEnabled(true);
            emailInputLayout.setError("Email ID is mandatory");
            emailIdEt.requestFocus();
            return false;
        } else if (!Utils.getUtils().isValidEmail(emailIdEt.getText().toString())) {
            emailInputLayout.setErrorEnabled(true);
            emailInputLayout.setError("Invalid Email");
            emailIdEt.requestFocus();
            return false;
        } else if (phoneNumberEt.getText().toString().isEmpty()) {
            mobileNoInputLayout.setErrorEnabled(true);
            mobileNoInputLayout.setError("Mobile number is mandatory");
            phoneNumberEt.requestFocus();
            return false;
        } else if (phoneNumberEt.getText().toString().length() < 10) {
            mobileNoInputLayout.setErrorEnabled(true);
            mobileNoInputLayout.setError("Invalid Mobile number");
            phoneNumberEt.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private void validateAndSavePassengerDetails() {
        bookedSeatModel.setContactEmailId(emailIdEt.getText().toString().trim());
        bookedSeatModel.setContactNumber(phoneNumberEt.getText().toString().trim());

        List<Passenger> passengerList = new ArrayList<>();
        for (int i = 0; i < passengerDetails.getChildCount(); i++) {
            View viewPassenger = passengerDetails.getChildAt(i);
            TextInputLayout nameIpL = viewPassenger.findViewById(R.id.name_input_layout);
            TextInputLayout ageIpL = viewPassenger.findViewById(R.id.age_input_layout);
            TextInputEditText name = viewPassenger.findViewById(R.id.nameInputEt);
            TextInputEditText age = viewPassenger.findViewById(R.id.ageInputEt);
            RadioButton maleRb = viewPassenger.findViewById(R.id.maleRb);
            RadioButton femaleRb = viewPassenger.findViewById(R.id.femaleRb);
            String fullName = name.getText().toString().trim();
            String ageValue = age.getText().toString().trim();


            boolean b = maleRb.isChecked() || femaleRb.isChecked();
            nameIpL.setErrorEnabled(false);
            nameIpL.setError(null);
            name.clearFocus();
            ageIpL.setErrorEnabled(false);
            ageIpL.setError(null);
            age.clearFocus();
            maleRb.clearFocus();
            if (fullName.isEmpty()) {
                nameIpL.setErrorEnabled(true);
                nameIpL.setError("Name is mandatory");
                name.requestFocus();
                break;
            } else if (ageValue.isEmpty()) {
                ageIpL.setErrorEnabled(true);
                ageIpL.setError("Age is mandatory");
                age.requestFocus();
                break;
            } else if (!b) {
                Toast.makeText(this, "Select gender for " + fullName, Toast.LENGTH_SHORT).show();
                maleRb.requestFocus();
                break;
            }

            Passenger passenger = new Passenger();
            if ((Integer) name.getTag() == 1) {
                passenger.setAge(ageValue);
                passenger.setName(fullName);
                String gender = maleRb.isChecked() ? "M" : "F";
                passenger.setGender(gender);
                passenger.setSeatNbr(bookedSeatModel.getSelectedSeatList().get(i).getSeatNumber());
                passenger.setSeatId(String.valueOf(bookedSeatModel.getSelectedSeatList().get(i).getSeatId()));
                passenger.setSeatPrice(String.valueOf(bookedSeatModel.getSelectedSeatList().get(i).getFare()));

                String serTax = String.valueOf(bookedSeatModel.getSelectedSeatList().get(i).getServiceTax());
                passenger.setServiceTax(Float.valueOf(serTax));// add service text from selected seat model
                passenger.setAvailableFor(bookedSeatModel.getSelectedSeatList().get(i).getAvailableFor());// For Check Male And Female Condition
                passengerList.add(passenger);

            }/*else{
                passenger.setAge(ageValue);
                passenger.setName(fullName);
                passenger.setGender(steGender);
                passenger.setSeatNbr(seatIdNumberRound[i-(size)].trim());
                passenger.setSeatId(seatIdArrayRound[i-(size)].trim());
                int firstTripArraySize=blockRequestFirst.getSelectedCommanFloorSeatArrayList().size();
                passenger.setSeat_price(blockRequestRound.getSelectedCommanFloorSeatArrayList().get(i-firstTripArraySize).getFare()+"");
                passenger.setService_tex(blockRequestRound.getSelectedCommanFloorSeatArrayList().get(i-firstTripArraySize).getServiceTax() );
                passenger.setAvailable_for(blockRequestRound.getSelectedCommanFloorSeatArrayList().get(i-firstTripArraySize).getAvailableFor() );
                passengerDetailses2.add(passenger);
            }*/


            if (i == passengerDetails.getChildCount() - 1) {
//                if (termsAndConditionsCb.isEnabled() && !termsAndConditionsCb.isChecked()) {
//                    Toast.makeText(this, getResources().getString(R.string.term_condition_choose), Toast.LENGTH_SHORT).show();
//                } else {
                bookedSeatModel.setPassengerList(passengerList);
                if (Utils.isOnline()) {
                    SharedPrefUtils.getSharedPrefUtils().saveSingleTripTicketData(this, bookedSeatModel);
                    startActivity(new Intent(this, PaymentDetailsActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
//                }
            }

        }
    }

    private void showWebAlertDialog(String fileurl) {
        if (webviewDialog!=null && webviewDialog.isShowing()) {
            webviewDialog.dismiss();
            webviewDialog = null;
        }
        WebView webView = new WebView(this);
        webView.loadUrl(fileurl);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(webView)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        webviewDialog.dismiss();
                        webviewDialog = null;
                    }
                });
        if (webviewDialog == null)
            webviewDialog = builder.create();
        if (!webviewDialog.isShowing() && !isFinishing())
            webviewDialog.show();
    }

    private SpannableString clickableStrings(String sampleString) {
        final SpannableString spannableString = new SpannableString(sampleString);
        ClickableSpan termsConditionsClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                showWebAlertDialog("file:///android_res/raw/terms_condition.html");
            }
        };


        ClickableSpan privacyClick = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                textView.invalidate();
                showWebAlertDialog("file:///android_res/raw/privacy_policy.html");
            }
        };

        try {
            return formatSpannableString(spannableString, termsConditionsClick, privacyClick);
        } catch (Exception e) {
            e.printStackTrace();
            return spannableString;
        }
    }

    private SpannableString formatSpannableString(SpannableString spannableString,
                                                  ClickableSpan termsClicked, ClickableSpan privacyClicked) throws Exception {
        spannableString.setSpan(termsClicked, 56, 76, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(privacyClicked, 79, 93, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_btn)), 56, 76,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_btn)), 79, 93,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });

        clickViewObservable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(View view) {
                        onClick(view);
                    }
                });
    }
}
