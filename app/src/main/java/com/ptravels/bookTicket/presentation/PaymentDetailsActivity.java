package com.ptravels.bookTicket.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.PurchaseEvent;
import com.paytm.pgsdk.PaytmConstants;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.ptravels.BaseActivity;
import com.ptravels.BuildConfig;
import com.ptravels.global.Constants;
import com.ptravels.ParveenApp;
import com.ptravels.global.PaymentConstants;
import com.ptravels.R;
import com.ptravels.REST.RestClient;
import com.ptravels.global.SharedPrefUtils;
import com.ptravels.global.Utils;
import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayData;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.GatewayFormParam;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.domain.BlockTicketUseCase;
import com.ptravels.bookTicket.domain.BookTicketUseCase;
import com.ptravels.bookTicket.domain.GatewayUseCase;
import com.ptravels.bookTicket.domain.PreTransactionUseCase;
import com.ptravels.bookTicket.domain.ReceiveUseCase;
import com.ptravels.home.presentation.HomeActivity;
import com.ptravels.myBooking.presentation.FragmentHolderActivity;
import com.ptravels.seatlayout.data.model.BookedSeatModel;
import com.ptravels.splash.presentation.SplashActivity;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PaymentDetailsActivity extends BaseActivity implements BookTicketView {

    private RadioButton payWithPayTm;
    private RadioButton paynimo_btn;
    private RadioButton credit_btn;
    private RadioButton debit_btn;
    private RadioButton netbanking_btn;
    private RadioButton wallet_btn;
    private RadioButton emi_btn;
    private RadioButton imps_btn;
    private RadioButton cashcard_btn;
    private int selected_payment_type = 0;

    private TextView referralAmountTv;
    private TextView basicAmnt, serviceTaxAmnt, totalAmntText;

    private LinearLayout ticketSummaryContainer;

    private int referralAmnt = 0;
    private String totalAmntString;
    float finalTotalAmount = 0;
    String beforeAmount = "0";
    private String BOOKING_ID_FIRST = "";
    private float TOTAL_SERVICE_TAX = 0;
    private BookedSeatModel singleTripTicketData;
    private AppCompatButton payBtn;
    private BookTicketPresenterImpl bookTicketPresenter;
    private BlockTicketResponse blockTicketResponse;
    private boolean isTicketBlocked;

    private FrameLayout loaderLayout;
    private ImageView loader;

    /*Paytm*/
    private PaytmPGService paytmPGService;
    private String PAYTM_ID = "";
    private GatewayFormParam param;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        if (SharedPrefUtils.getSharedPrefUtils().getSingleTripTicketData(this) != null) {
            singleTripTicketData = SharedPrefUtils.getSharedPrefUtils().getSingleTripTicketData(this);
        }

        initiateView();
    }

    public void initiateView() {
        TextView title = findViewById(R.id.title);
        title.setText("Summary");
        ticketSummaryContainer = (LinearLayout) findViewById(R.id.ticketSummaryContainer);
        credit_btn = (RadioButton) findViewById(R.id.credit_card);
        debit_btn = (RadioButton) findViewById(R.id.debit_card);
        netbanking_btn = (RadioButton) findViewById(R.id.netbanking_card);
        wallet_btn = (RadioButton) findViewById(R.id.wallet_card);
        emi_btn = (RadioButton) findViewById(R.id.emi_card);
        imps_btn = (RadioButton) findViewById(R.id.imps_card);
        cashcard_btn = (RadioButton) findViewById(R.id.cash_card);
        basicAmnt = (TextView) findViewById(R.id.fare);
        serviceTaxAmnt = (TextView) findViewById(R.id.serviceTax);
        totalAmntText = findViewById(R.id.totalAmntTv);
        payBtn = findViewById(R.id.payBtn);
        payWithPayTm = findViewById(R.id.payWithPayTm);

        loaderLayout = findViewById(R.id.full_screen_loader_layout);
        loader = findViewById(R.id.loader);

        if (singleTripTicketData != null) {
            View ticketSummaryView = (View) getLayoutInflater().inflate(R.layout.layout_ticket_summary, null);
            TextView fromTo = (TextView) ticketSummaryView.findViewById(R.id.fromToValue);
            TextView travelDate = (TextView) ticketSummaryView.findViewById(R.id.travelDate);
            TextView deptTime = (TextView) ticketSummaryView.findViewById(R.id.departureTime);
            TextView emailTv = ticketSummaryView.findViewById(R.id.summary_email);
            TextView mobileNumber = ticketSummaryView.findViewById(R.id.summary_mobile_no);

            TextView boardingPtName = (TextView) ticketSummaryView.findViewById(R.id.boardingPoint);
            CheckBox showPassengersCb = (CheckBox) ticketSummaryView.findViewById(R.id.showPassengerSummaryCb);
            final LinearLayout passengersSummaryContainer = (LinearLayout) ticketSummaryView.findViewById(R.id.passengersSummeryLayout);

            fromTo.setText(singleTripTicketData.getFromStationName() + " to " + singleTripTicketData.getToStationName());
            travelDate.setText(Utils.formatDateString("yyyy-MM-dd", "dd MMM, yyyy", singleTripTicketData.getTravelDate()));
            deptTime.setText(Utils.formatDateString("HH:mm:ss", "h:mm a", singleTripTicketData.getBoardingPoint().getStationPointTime()));
            boardingPtName.setText(singleTripTicketData.getBoardingPoint().getStationPointName());
            boardingPtName.setSelected(true);

            emailTv.setText(singleTripTicketData.getContactEmailId());
            emailTv.setSelected(true);
            mobileNumber.setText(singleTripTicketData.getContactNumber());
            mobileNumber.setSelected(true);

            showPassengersCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked) {
                        passengersSummaryContainer.setVisibility(View.VISIBLE);
                    } else {
                        passengersSummaryContainer.setVisibility(View.GONE);
                    }

                }
            });
            showPassengersCb.setChecked(false);
            passengersSummaryContainer.setVisibility(View.VISIBLE);

            if (singleTripTicketData.getPassengerList().size() > 0) {

                for (int i = 0; i < singleTripTicketData.getPassengerList().size(); i++) {
                    View passengerSummaryView = getLayoutInflater().inflate(R.layout.layout_passengers_summary, null);
                    TextView name = (TextView) passengerSummaryView.findViewById(R.id.passName);
                    TextView seatNo = (TextView) passengerSummaryView.findViewById(R.id.passSeatNo);
//                    TextView age = (TextView) passengerSummaryView.findViewById(R.id.passAge);
//                    TextView gender = (TextView) passengerSummaryView.findViewById(R.id.passGender);

                    name.setSelected(true);
                    name.setText(singleTripTicketData.getPassengerList().get(i).getName()
                            + "\t\t\t" + singleTripTicketData.getPassengerList().get(i).getGender()
                            + "\t\t\t" + singleTripTicketData.getPassengerList().get(i).getAge());
                    seatNo.setText("Seat " + singleTripTicketData.getPassengerList().get(i).getSeatNbr());
//                    age.setText(singleTripTicketData.getPassengerList().get(i).getAge());
//                    gender.setText(singleTripTicketData.getPassengerList().get(i).getGender());
                    passengersSummaryContainer.addView(passengerSummaryView);
                }
            }
//            service_name.setText(singleTripTicketData.getServiceNumber());
            ticketSummaryContainer.addView(ticketSummaryView);
        }


        /*if (userDetail != null && userDetail.getResponse() != null && userDetail.getResponse().getUser_detail() != null) {
            referralAmnt = userDetail.getResponse().getUser_detail().getReferral_amount();
            if (referralAmnt > 0) {
                referralAmountTv.setVisibility(View.VISIBLE);
                referralAmountTv.setText("₹ " + referralAmnt + "" + " Reduced from referral amount! ");

                if ((Integer.valueOf(totalAmntString)) <= referralAmnt) {
                    int tonew = (Integer.valueOf(totalAmntString)) / 2;
                    referralAmnt = ((tonew));
                    totalAmntString = String.valueOf(Integer.valueOf(totalAmntString) - (referralAmnt));
                } else {
                    totalAmntString = String.valueOf(Integer.valueOf(totalAmntString) - (referralAmnt));
                }
            }
        }*/

        String tp = String.valueOf(singleTripTicketData.getTotalPrice());
//        String tp2 = "";
        totalAmntString = tp;
        String totalServiceTax = String.valueOf(singleTripTicketData.getTotalServiceTax());
        TOTAL_SERVICE_TAX = TOTAL_SERVICE_TAX + Float.valueOf(totalServiceTax);
        if (TOTAL_SERVICE_TAX > 0) {
            serviceTaxAmnt.setVisibility(View.VISIBLE);
        }

        String totalFare = String.valueOf(singleTripTicketData.getBookedSeatTotalCost());
        finalTotalAmount = Float.valueOf(totalAmntString);
//        payBtn.setText("Pay amount : \u20B9 " + /*singleTripTicketData.getBookedSeatTotalCost()*/finalTotalAmount);
        basicAmnt.setText("Basic Amount : " + "\u20B9 " + Float.valueOf(totalFare));
        serviceTaxAmnt.setText("GST   Amount : " + "\u20B9 " + TOTAL_SERVICE_TAX);
        totalAmntText.setText("\u20B9 " + finalTotalAmount);
        Log.d("TOTAL_SERVICE_TAX---", TOTAL_SERVICE_TAX + "");

        /*payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((blockTicketResponse == null || blockTicketResponse.getData() == null || blockTicketResponse.getData().getPnr() == null)
                        && !isTicketBlocked) {
                    blockTicket();
                } else {
                    if (payWithPayTm.isChecked()) {
                        if (param == null) {
                            initPreTransaction(blockTicketResponse);
                        } else {
                            try {
                                initPayTmTransaction();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });*/
        rxSingleClickListener(payBtn);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        getPaymentGateways();

    }

    private void getPaymentGateways() {
        bookTicketPresenter = new BookTicketPresenterImpl(this);
        bookTicketPresenter.getGatewayData(new GatewayUseCase(), null);
    }

    private void blockTicket() {
        BlockTicketRequest request = new BlockTicketRequest();
        request.setEmailId(singleTripTicketData.getContactEmailId());
        request.setMobileNumber(singleTripTicketData.getContactNumber());
        request.setBoardingPointId(String.valueOf(singleTripTicketData.getBoardingPoint().getStationPointId()));
        request.setFromStation(singleTripTicketData.getFromStationId());
        request.setToStation(singleTripTicketData.getToStationId());
        request.setTravelDate(singleTripTicketData.getTravelDate());
        request.setScheduleId(singleTripTicketData.getScheduleId());
        request.setPassengerDetails(singleTripTicketData.getPassengerList());
        bookTicketPresenter = new BookTicketPresenterImpl(this, new BlockTicketUseCase());
        bookTicketPresenter.getBlockTicketResult(request);
    }

    @Override
    public void showBlockTicketResult(BlockTicketResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        blockTicketResponse = response;

        if (blockTicketResponse.getSuccess().equalsIgnoreCase("true") && blockTicketResponse.getData() != null) {
            if (!blockTicketResponse.getData().getPnr().isEmpty()) {
                isTicketBlocked = true;
                if (payWithPayTm.isChecked()) {
                    initPreTransaction(blockTicketResponse);
                }
            }
        } else if (!blockTicketResponse.getSuccess().equalsIgnoreCase("false")) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(PaymentDetailsActivity.this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
            if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }

            if (errMsg.equalsIgnoreCase("null")) {
                Toast.makeText(this, "Something went wrong, please try again later", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    private void initPreTransaction(BlockTicketResponse blockTicketResponse) {
        PreTransactionRequest request = new PreTransactionRequest();
        request.setPnr(blockTicketResponse.getData().getPnr());
        request.setGatewayMode("CC");
        request.setPaymentId("paytm");
        bookTicketPresenter.getPreTransactionData(new PreTransactionUseCase(), request);
    }

    @Override
    public void provideGatewayData(GatewayResponse response) {
        if (response == null)
            return;
        if (response.getSuccess().equalsIgnoreCase("true")) {
            if (response.getData() != null) {
                List<GatewayData> dataList = response.getData();
                for (GatewayData data : dataList) {
                    if (data.getPaymentGatewayProviderCode().equalsIgnoreCase("paytm")) {
                        payWithPayTm.setVisibility(View.VISIBLE);
                        payWithPayTm.setChecked(true);
                    }
                }
            }
        } else if (!response.getSuccess().equalsIgnoreCase("false")) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
            if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void showError(String error) {
        if (!isFinishing()) {
            Toast.makeText(this, "error : " + error, Toast.LENGTH_SHORT).show();
            if (error.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    @Override
    public void providePreTransactionData(PreTransactionResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        if (response.isSuccess()) {
            if (response.getData() != null && response.getData().getTransactionId() != null) {
                if (blockTicketResponse.getData().getPnr().equals(response.getData().getTransactionId())) {
                    if (response.getData().getGatewayCode().equals("paytm")
                            && response.getData().getGatewayModeDetailCode().equals("CC")) {
                        param = response.getData().getGatewayFormParam();
                        if (param == null)
                            return;
                        try {
                            initPayTmTransaction();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (response.isError()) {
            Log.d("PreTransactionData", "error");
            Toast.makeText(this, "Transaction failure, try again later", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    @Override
    public void showBookTicketResult(BookTicketResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        if (response.isSuccess()) {
            if (blockTicketResponse.getData().getPnr().equals(response.getData().getPnr())) {
                Toast.makeText(this, "Ticket Booked successfully", Toast.LENGTH_SHORT).show();
                singleTripTicketData.setBlockedSeatPnr(response.getData().getPnr());
                logPurchaseEvent(singleTripTicketData);
                startActivity(new Intent(this, FragmentHolderActivity.class)
                        .putExtra(Constants.LOAD_FRAGMENT, FragmentHolderActivity.TICKET_DETAILS_FRAGMENT)
                        .putExtra(Constants.BOOKED_SEAT_DATA, singleTripTicketData));
                finish();
            }
        } else if (!response.isSuccess()) {
            String errMsg = RestClient.get200StatusErrors(response.getError());
            Toast.makeText(this, "error : " + errMsg, Toast.LENGTH_SHORT).show();
            if (errMsg.equalsIgnoreCase("Un-authorized acess")) {
                SharedPrefUtils.getSharedPrefUtils().clearUserData(this);
                startActivity(new Intent(this, SplashActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        }
    }

    private void logPurchaseEvent(BookedSeatModel singleTripTicketData) {
        try {
            BigDecimal itemPrice = new BigDecimal(singleTripTicketData.getTotalPrice(), MathContext.DECIMAL64);
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemId(singleTripTicketData.getBlockedSeatPnr())
                    .putCurrency(Currency.getInstance("INR"))
                    .putItemName(singleTripTicketData.getFromStationName() + " - " + singleTripTicketData.getToStationName())
                    .putItemPrice(itemPrice)
                    .putCustomAttribute("Seats", singleTripTicketData.getSelectedSeatList().size()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receivePaytmInfo(ReceiveResponse response) {
        if (isFinishing())
            return;
        if (response == null)
            return;
        if (response.getMessage().equalsIgnoreCase("Sucess") && !response.isError() && response.isStatus()) {
            Toast.makeText(PaymentDetailsActivity.this, "Payment Transaction is Successful", Toast.LENGTH_LONG).show();
            initBookTicket();
        } else if (response.isError()) {
            Toast.makeText(this, "error : " + response.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void initPayTmTransaction() {
        paytmPGService = null;
//        paytmPGService = PaytmPGService.getStagingService();
        paytmPGService = PaytmPGService.getProductionService();
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put(PaymentConstants.ORDER_ID, /*pnr*/param.getORDER_ID());
        paramMap.put(PaymentConstants.MID, param.getMID());
        paramMap.put(PaymentConstants.CUST_ID, param.getCUST_ID());
        paramMap.put(PaymentConstants.CHANNEL_ID, param.getCHANNEL_ID());
        paramMap.put(PaymentConstants.INDUSTRY_TYPE_ID, param.getINDUSTRY_TYPE_ID());
        paramMap.put(PaymentConstants.WEBSITE, param.getWEBSITE());
        paramMap.put(PaymentConstants.TXN_AMOUNT, param.getTXN_AMOUNT()/*"3"*/);
        paramMap.put(PaymentConstants.REQUEST_TYPE, param.getREQUEST_TYPE());
        paramMap.put(PaymentConstants.EMAIL, param.getEMAIL());
        paramMap.put(PaymentConstants.MOBILE_NO, param.getMOBILE_NO());

//        paramMap.put(PaymentConstants.CALLBACK_URL, Constants.paytm_callback_url);
        paramMap.put(PaymentConstants.CALLBACK_URL, param.getCALLBACK_URL());

        paramMap.put(PaymentConstants.CHECKSUMHASH, param.getCHECKSUMHASH());

        PaytmOrder Order = new PaytmOrder(paramMap);
        paytmPGService.initialize(Order, null);


        paytmPGService.startPaymentTransaction(this, false, false,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        Log.e("someUIErrorOccurred", inErrorMessage);
                    }

                    @Override
                    public void onTransactionResponse(Bundle bundle) {
//                        Log.e("LOG", "Payment Transaction is successful " + bundle);

                        if (bundle != null) {
                            if (BuildConfig.DEBUG) {
                                Log.d("onTransactionResponse", "bundle = " + bundle.toString());
                            }
                            if (isValidPayment(bundle, param)) {
                                PAYTM_ID = bundle.getString(PaytmConstants.TRANSACTION_ID);
                                Log.e("PaymentId by PayTm=", PAYTM_ID);
                                requestReceiveJson();
                            } else {
                                String str = bundle.getString(PaytmConstants.RESPONSE_MSG);
                                Toast.makeText(PaymentDetailsActivity.this, "" + str, Toast.LENGTH_LONG).show();

                            }
                        }
                    }

                    @Override
                    public void networkNotAvailable() { // If network is not // available, then this // method gets called.
                        Toast.makeText(PaymentDetailsActivity.this, "networkNotAvailable!!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        Log.e("AuthenticationFailed=", inErrorMessage);
                        Toast.makeText(PaymentDetailsActivity.this, inErrorMessage, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                        Log.e("onErrorLoadingWebPage=", inErrorMessage + ",url = " + inFailingUrl);
                        Toast.makeText(PaymentDetailsActivity.this, inErrorMessage, Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onBackPressedCancelTransaction() {
                        Toast.makeText(PaymentDetailsActivity.this, "Transaction Cancelled", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }

                    @Override
                    public void onTransactionCancel(String s, Bundle bundle) {
                        Log.e("LOG", "Payment Transaction Cancelled " + s);
                        Toast.makeText(PaymentDetailsActivity.this, "Payment Transaction Cancelled, " + s, Toast.LENGTH_LONG).show();
                        paytmPGService = null;
                        startActivity(new Intent(PaymentDetailsActivity.this, HomeActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }

                });
    }

    private void requestReceiveJson() {
        if (blockTicketResponse == null || blockTicketResponse.getData() == null || blockTicketResponse.getData().getPnr() == null) {
            Toast.makeText(this, "Booking failed, Please try again later", Toast.LENGTH_SHORT).show();
            Log.d("requestReceiveJson", "pnr null");
            return;
        }

        ReceiveRequest request = new ReceiveRequest();
        request.setPnr(blockTicketResponse.getData().getPnr());
        request.setToken(ParveenApp.getAccessToken());
        bookTicketPresenter.receivePaytmInfo(new ReceiveUseCase(), request);
    }

    private void initBookTicket() {
        BookTicketRequest request = new BookTicketRequest();
        request.setPnr(blockTicketResponse.getData().getPnr());
        bookTicketPresenter.getBookTicketResult(new BookTicketUseCase(), request);
    }

    @Override
    public void showLoading() {
        loaderLayout.setVisibility(View.VISIBLE);
        Utils.getUtils().showLoader(loader, null, this);
    }

    @Override
    public void hideLoading() {
        loaderLayout.setVisibility(View.GONE);
        Utils.getUtils().closeLoader(loader, null, this);
    }

    private boolean isValidPayment(Bundle bundle, GatewayFormParam param) {
        return (bundle.getString(PaytmConstants.STATUS).equals(PaymentConstants.TXN_SUCCESS));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("paymentdetail screen", "onDestroy");
    }

    private void rxSingleClickListener(final View clickView) {

        Observable<View> clickViewObservable = Observable.create(new Observable.OnSubscribe<View>() {
            @Override
            public void call(final Subscriber<? super View> subscriber) {
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (subscriber.isUnsubscribed())
                            return;
                        subscriber.onNext(v);
                    }
                });
            }
        });

        clickViewObservable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<View>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(View view) {
                        switch (view.getId()){
                            case R.id.payBtn:
                                if ((blockTicketResponse == null || blockTicketResponse.getData() == null || blockTicketResponse.getData().getPnr() == null)
                                        && !isTicketBlocked) {
                                    blockTicket();
                                } else {
                                    if (payWithPayTm.isChecked()) {
                                        if (param == null) {
                                            initPreTransaction(blockTicketResponse);
                                        } else {
                                            try {
                                                initPayTmTransaction();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                });
    }
}
