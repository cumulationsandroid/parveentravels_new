package com.ptravels.bookTicket.presentation;

import com.ptravels.global.LoadingView;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BookTicketView extends LoadingView {
    void showBlockTicketResult(BlockTicketResponse response);
    void provideGatewayData(GatewayResponse response);
    void showError(String error);
    void providePreTransactionData(PreTransactionResponse response);
    void showBookTicketResult(BookTicketResponse response);
    void receivePaytmInfo(ReceiveResponse response);
}
