package com.ptravels.bookTicket.domain;

import com.ptravels.bookTicket.data.model.BlockTicketRequest;
import com.ptravels.bookTicket.data.model.BlockTicketResponse;
import com.ptravels.bookTicket.data.model.BookTicketRequest;
import com.ptravels.bookTicket.data.model.BookTicketResponse;
import com.ptravels.bookTicket.data.model.GatewayResponse;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionResponse;
import com.ptravels.bookTicket.data.model.paymentRedirectPojos.PreTransactionRequest;

import rx.Single;

/**
 * Created by Anirudh Uppunda on 9/12/17.
 */

public interface BookTicketDataInterface {
    Single<BlockTicketResponse> blockTicket(BlockTicketRequest request);
    Single<GatewayResponse> getGateway();
    Single<PreTransactionResponse> getPreRedirectResponse(PreTransactionRequest request);
    Single<BookTicketResponse> bookTicket(BookTicketRequest request);
    Single<ReceiveResponse> receivePaytmTransactInfo(ReceiveRequest request);
}
