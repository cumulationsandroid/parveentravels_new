package com.ptravels.bookTicket.domain;

import com.ptravels.global.UseCase;
import com.ptravels.bookTicket.data.model.ReceiveRequest;
import com.ptravels.bookTicket.data.model.ReceiveResponse;
import com.ptravels.bookTicket.data.source.BookTicketDataImpl;

import rx.Single;

/**
 * Created by Amit Tumkur on 17-01-2018.
 */

public class ReceiveUseCase extends UseCase<ReceiveRequest,ReceiveResponse> {
    private BookTicketDataInterface bookTicketDataInterface = new BookTicketDataImpl();

    @Override
    public Single<ReceiveResponse> buildUseCase(ReceiveRequest request) {
        return bookTicketDataInterface.receivePaytmTransactInfo(request);
    }
}
